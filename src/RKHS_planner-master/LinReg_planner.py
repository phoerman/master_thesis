

import sys
import os
import csv
import pickle
import time
import numpy as np
from scipy import interpolate

import matplotlib
#matplotlib.use("Agg")# set if no images are required to appear during runtime - figures are created but not shown

import matplotlib.pyplot as pl
from AbstractFunctionalPlanner import AbstractFunctionalPlanner

from Path.LinearMeanFunction import LinearMeanFunction
from Path.LinReg import LinReg_path

from Samplers.AbstractSampler import AbstractSampler
from Samplers.weighted_sampler import WeightedSampler
from Samplers.random_sampler import RandomSampler
from Samplers.determinstic_max_sampler import DetermisticSampler

from Utils import learning_rate
from Utils.Utils import normal_vect

#Define global variables
if sys.platform == "linux" or sys.platform == "linux2":
    TempFolder = "/home/gfra8070/tmp"+os.sep

elif sys.platform == "win32":
    TempFolder = "c:" + os.sep + "temp"+os.sep
else:
    raise ValueError("Unsupported os")


class LinearReg_Planner(AbstractFunctionalPlanner):
    kerenl_approx_method = {
        "Nystroem": "Nystroem",
        "RFF": "RFF"
    }

    def __init__(self, kerenl_approx_method, D):
        '''

        :param D: Dimensionality of planner
        '''
        super(LinearReg_Planner, self).__init__(D)
        self.lengthscale = 0.1  # Just for reference
        self.gamma = 1./self.lengthscale
        self.feature_func = None

        #self.boundary_feature = []
        self.boundary_conditions_inv_K = np.array
        #self.boundary_K_list = []

        self.sampled_points = []
        self.all_samples = []

        self.learning_rate = None
        self.batch_size = 1
        self.sampler = AbstractSampler()
        self.grad_cap = 0.3
        self.cost_multiplier = 0. #0. sampler updated only with valid samples, 1.=use the obstacle cost for invalid samples

        #New idea
        self._use_normal_update = False

        #Movie
        self.save_movie= False
        self.map2plot = None
        self.movie_fig_num = 666
        self.frame_counter = 0
        self.movie_filename = 'Results' + os.sep + 'LinReg_'


        assert(kerenl_approx_method in self.kerenl_approx_method)
        self.method = kerenl_approx_method

    def initialize_solution(self,
                            gamma,
                            number_of_features,
                            start_point,
                            end_point,
                            grad_boundary_conditions={},
                            include_end_point=True
                            ):

        if self.D != start_point.size:
            raise ValueError('LinReg_path:initialize_straight_line - Invalid input size')

        self.psi_t_0 = start_point
        self.psi_t_1 = end_point

        self.path = LinReg_path()
        self.path.path_mean = LinearMeanFunction(x0=self.psi_t_0,
                                                 x1=self.psi_t_1)
        self.boundary_conditions[0.] = self.psi_t_0 # - self.path.curve_at_t(0.)  # after reduction of mean
        if include_end_point:
            self.boundary_conditions[1.] = self.psi_t_1 # - self.path.curve_at_t(1.)  # after reduction of mean

        self.gamma = gamma # Just for record keeping - gamma only matters in features
        self.lengthscale = 1./self.gamma
        self.path.initialize_features(method=self.method,
                                 n_components=number_of_features,
                                 gamma=gamma,
                                 D=self.D
                                 )

        #  Prepare boundary condition for a simple online update
        t_b = np.array([])
        if self.boundary_conditions:
            t_b = np.concatenate((t_b, np.fromiter(iter(self.boundary_conditions.keys()), dtype=float)), axis=0)

        if grad_boundary_conditions:
            assert isinstance(grad_boundary_conditions, dict)
            self.grad_boundary_conditions = grad_boundary_conditions

            #Boundary conditon are defined at specific time, but boundary feature need to be unique
            modifier = self.gamma/100
            for t_gb in self.grad_boundary_conditions.keys():

                while t_gb in t_b:
                    t_gb += modifier

                t_b = np.append(t_b, t_gb)


        boundary_gamme = 10.*self.gamma # sho
        self.path.initialize_boundary_features(t_b,
                                               gamma=boundary_gamme,
                                               D=self.D
                                               )

        K_b = self.path.boundary_features.evaluate(t_b)
        K_g = self.path.boundary_features.gradient(t_b)

        K = np.concatenate((K_b[:len(self.boundary_conditions),:],
                     K_g[:len(self.grad_boundary_conditions), :]),
                     axis=0
                     )

        self.boundary_conditions_inv_K =  np.linalg.pinv(K)

        #Fixing boundary condition and pulling toward initial guess
        self._update_boundary_conditions()

        if end_point is not None:
            K = self.path.features.evaluate(1.)
            grad = end_point - self.path.mean(1.)

            eta = 1. #learning_rate()
            g = np.asarray(grad)  # + (eta**3) * np.random.randn(len(grad), self.D)

            self.path.weights -= eta * K.T.dot(g)

            self._update_boundary_conditions()

        if True:
            pl.figure(2)
            pl.clf()
            self.path.plot_curve(fig_num=2,plotstr='b-')
            pl.scatter(*[x for x in self.psi_t_0[:,np.newaxis]], marker='*')
            pl.scatter(*[x for x in self.psi_t_1[:,np.newaxis]], marker='p')
            pl.show(block=False)

    def initialize_solution_step(self, gamma, number_of_features, start_point,
                                 end_point, dx_t0=None):

        #A wrapper to initiliaze that sets boundary conditions

        if end_point is not None and dx_t0 is None:
            dx_t0 = end_point - start_point

        grad_boundary_condition = {0.: dx_t0}

        self.initialize_solution(gamma,
                                 number_of_features,
                                 start_point,
                                 end_point,
                                 grad_boundary_conditions=grad_boundary_condition,
                                 include_end_point=False
                                 )  # from LinReg planner


    def plan(self):


        ##fixed points
        '''
        fixed_support, fixed_support_cost = self.find_support(t = np.asarray(list(self.path.features.keys())))

        grad, J = self.compute_cost_and_gradient(fixed_support)

        self.update_trajectory_weights(fixed_support, J, grad)
        '''
        #stochastic selection
        stochastic_change = []

        samples = self.sampler.draw_samples(n=self.batch_size)

        stochastic_support, stochastic_cost = self.find_support(t = samples)  # calculate U_obs, supprt(t_i)

        if stochastic_support:
            x1 = self.path.mean(stochastic_support)

        grad, J = self.compute_cost_and_gradient(stochastic_support)

        #self.update_trajectory_weights(stochastic_support, J, grad)
        self.fixed_update_trajectory_weights(stochastic_support, J, grad)

        #self.support = np.concatenate((fixed_support, stochastic_support))
        self.support = stochastic_support

        self.sampled_points.extend(stochastic_support)
        self.all_samples.extend(samples)

        #  update sampler
        if stochastic_support:
            x2 = self.path.mean(stochastic_support)
            stochastic_change = np.linalg.norm(x2 - x1, axis=1)
        sampler_updated_cost = stochastic_cost.copy()*self.cost_multiplier

        for i, sample in enumerate(samples):
            try:  # only updates valid cost woth the actaul change - inavlid samples will still return full cost
                idx = stochastic_support.index(sample)
                sampler_updated_cost[i] = stochastic_change[idx]
            except:
                pass
        self.sampler.update(sampler_updated_cost)

        #return self.reduce_obs_cost(np.concatenate((fixed_support_cost, stochastic_cost)))
        return self.reduce_obs_cost(stochastic_cost)

    def compute_cost_and_gradient(self, support):
        #cost = []
        grad = []
        J = []
        #  print("t, xy, obs cost, regulrasied control cost")
        if self.save_movie:
            # occ = self.map_plot(fig_num=1)
            # self._create_movie_frame_single_image(occ, x, obs_grad, control_grad, MI_grad)
            x2plot = []
            obs_grad2plot = []
            control_grad2plot = []

        for t_i in support:

        #    cost.append(self.obs_cost(t_i) +
         #               self.regulariser_beta * self.control_cost(t_i)
         #
        #                )
            grad_obs = self.obs_cost_gradient(t_i)

            control_grad =  self.regulariser_beta * self.control_cost_grad_at_time(t_i)
            if self._use_normal_update:
                #testing an idea

                path_grad = self.path.gradient(t_i)
                obs_grad_normal = normal_vect(grad_obs)
                angle_x_grad_u = path_grad.dot(obs_grad_normal) / (np.linalg.norm(obs_grad_normal) * np.linalg.norm(path_grad))  # cos angle
                angle_x_grad_u = np.arccos(angle_x_grad_u)
                if angle_x_grad_u < np.pi / 2:
                    obs_grad_normal *= -1. # change vector direction by 180 - TODO: check which direction is better
                obs_grad_normal *= np.linalg.norm(grad_obs) / 10.

                grad_obs += obs_grad_normal

            grad.append(grad_obs + control_grad)


            #  print("{0}, {1}, {2}, {3}".format(t_i, np.squeeze(self.path.curve_at_t(t_i)), np.squeeze(self.obs_cost_gradient(t_i)), self.regulariser_beta*np.squeeze(self.control_cost_grad_at_time(t_i))))

            # Assumes a simple Jacobian (for now)
            J.append(np.eye(self.D))

        #return cost,grad,J
        return grad, J

    def update_trajectory_weights(self, support, J, grad):
        '''

        Only update a single feature at a time - the closest to the support point
        :param support:
        :param J:
        :param grad:
        :return:
        '''
        debug = True

        if debug:
            debug_fig = 999
            pl.figure(debug_fig)
            pl.clf()
            self.path.plot_curve(fig_num=debug_fig,
                                    plotstr='r-',
                                    linewidth=1.5
                                    )
            pl.show(block=False)

        if support:  # not empty

            for t_i, grad_i, J_i in zip(support, grad, J):

                if t_i in self.path.features:
                    closest_feature_key = t_i
                else:
                    #  using the fact the path.features are SortedDict, find closest index in dicr
                    feature_index = self.path.features.bisect_right(t_i)
                    closest_feature_key = self.path.features.iloc[feature_index]
                    if (closest_feature_key - t_i  > t_i - self.path.features.iloc[feature_index - 1]):
                        closest_feature_key = self.path.features.iloc[feature_index - 1]

                if debug:
                    t_i_xy = self.path.mean(t_i)
                    pl.scatter(t_i_xy[:, 0], t_i_xy[:, 1], marker='8', color='m')
                    basis_xy = self.path.mean(closest_feature_key)
                    pl.scatter(basis_xy[:, 0], basis_xy[:, 1], marker='x', color='b')
                    pl.show(block=False)

                K_b = np.asscalar(self.path.features[closest_feature_key].evaluate(t_i))

                g = self.learning_rate() * (J_i.dot(grad_i))*K_b

                '''
                for boundary_key in self.boundary_conditions.keys():
                    K_b = np.asscalar(self.path.boundary_features[boundary_key].evaluate(closest_feature_key))
                    g -= self.learning_rate * K_b * K_b * grad_i
                '''

                self.path.weights[closest_feature_key] -= g

                if debug:
                    self.path.plot_curve(fig_num=debug_fig,
                                         plotstr='c-',
                                         linewidth=1.
                                         )

                    t_i_xy = self.path.mean(t_i)
                    pl.scatter(t_i_xy[:, 0], t_i_xy[:, 1], marker='8', color='m')
                    basis_xy = self.path.mean(closest_feature_key)
                    pl.scatter(basis_xy[:, 0], basis_xy[:, 1], marker='x', color='b')
                    pl.show(block=False)


            if self.boundary_conditions:  # fix boundary conditions
                boundary_offset = (self.path.mean(np.asarray(list(self.boundary_conditions.keys()))) -
                                   np.squeeze(np.asarray(list(self.boundary_conditions.values())))
                                  )

                boundary_offset = self.boundary_conditions_inv_K.dot(boundary_offset.T).T

                for key, offset in zip(self.boundary_conditions.keys(), boundary_offset):
                    self.path.boundary_weights[key] -= offset

        if debug:
            self.path.plot_curve(fig_num=debug_fig,
                                 plotstr='b-',
                                 linewidth=2.
                                 )

            pl.show(block=False)

        if self.debug:
            self.path.plot_curve(fig_num=1,
                                 plotstr='r-',
                                 linewidth=1.
                                )
            pl.show(block=False)

    def fixed_update_trajectory_weights(self, support, J, grad):

        '''
        This method is different to "update_trajectory_weights". All weights are updated by each support.
        update of w_(i+1) = w_(i) - learning_rate*grad*feature_(n)[support]

        :param support:
        :param J:
        :param grad:
        :return:
        '''

        if support:  # not empty

            K = self.path.features.evaluate(support)
            for i, grad_i in enumerate(grad):
                #capping gradient
                grad_i[grad_i > self.grad_cap] = self.grad_cap
                grad_i[grad_i < -self.grad_cap] = -self.grad_cap

                grad[i] = grad_i.dot(J[i])

            eta = self.learning_rate()
            g = np.asarray(grad) #+ (eta**3) * np.random.randn(len(grad), self.D)

            self.path.weights -= eta * K.T.dot(g)/len(support) #len(support) for averaging with the number of support

            self._update_boundary_conditions()

        if True: #self.debug:
            self.path.plot_curve(fig_num=1,
                                 plotstr='r-',
                                 linewidth=1.
                                 )
            pl.show(block=False)

    def _update_boundary_conditions(self):

        boundary_conditions = np.array([])

        if self.boundary_conditions:  # fix boundary conditions
            bound_delta = self.path.mean(np.fromiter(iter(self.boundary_conditions.keys()), dtype=float)) - \
                          np.squeeze(np.asarray(list(self.boundary_conditions.values())))
            boundary_conditions = np.vstack([boundary_conditions, bound_delta]) if boundary_conditions.size \
                else bound_delta

        if self.grad_boundary_conditions:
            grad_delta = self.path.gradient(np.fromiter(iter(self.grad_boundary_conditions.keys()), dtype=float)) - \
                         np.squeeze(np.asarray(list(self.grad_boundary_conditions.values())))

            boundary_conditions = np.vstack([boundary_conditions, grad_delta]) if boundary_conditions.size \
                else grad_delta

        self.path.boundary_weights -= self.boundary_conditions_inv_K.dot(boundary_conditions)

    def plot_path_gradient_debug(self):
        pl.figure(111)
        pl.clf()
        t = np.atleast_2d(np.linspace(0., 1., 1000, endpoint=True)).T
        path = self.path.mean(t)

        pl.plot(t, path[:, 0], 'b-')
        pl.plot(t, path[:, 1], 'b.')
        pl.show(block=False)

        g = self.path.gradient(t)
        pl.plot(t, g[:, 0], 'r-')
        pl.plot(t, g[:, 1], 'r.')
        pl.show(block=False)

        g2 = self.path.gradient2(t)
        pl.plot(t, g2[:, 0], 'g-')
        pl.plot(t, g2[:, 1], 'g.')
        pl.show(block=False)

    def create_frame2(self, rkhs_iter, map, fig_number, filename):

        fig = pl.figure(fig_number)
        pl.clf()

        t_plot = np.linspace(start=0.,
                             stop=1.,
                             num=len(self.cost_results),
                             endpoint=True
                             )
        pl.title(s='Iteration:{0} '.format(rkhs_iter))
        pl.tick_params(axis='both', which='both', labelsize=14)
        #  Plotting costs
        if self._extent is None:
            extent = (np.min(self.grid_x), np.max(self.grid_x), np.max(self.grid_y), np.min(self.grid_y))  # Note order of Y
        else:
            extent = self._extent


        pl.imshow(map, extent=extent, interpolation='bilinear', cmap='hot')
        self.plot_obstacles(fig_number)
        t = np.atleast_2d(np.linspace(0., 1., 1000, endpoint=True)).T
        path = self.path.mean(t)
        pl.plot(path[:, 0], path[:, 1], 'c-', linewidth=1.5)
        cbar = pl.colorbar()
        cbar.ax.tick_params(labelsize=14)
        pl.axis(extent)
        pl.tick_params(axis='both', which='both', labelsize=14)
        pl.show(block=False)
       # pl.tight_layout(pad=1., w_pad=2.0, h_pad=2.0)

        pl.savefig(filename+'_map')

        pl.figure()
        if len(self.all_samples) > 0 and len(self.sampled_points) > 0:
            n, bins, patches = pl.hist(self.all_samples, 100, normed=False, facecolor='red', alpha=0.75, label='Invalid')
            n, bins, patches = pl.hist(self.sampled_points, 100, normed=False, facecolor='green', alpha=0.75, label='Valid')
        pl.xlabel('t',fontsize=24)
        pl.ylabel('Accumulated samples',fontsize=24)
        pl.gca().legend(fontsize=24,loc=2)
        pl.tick_params(axis='both', which='both', labelsize=20)
        pl.xlim(0., 1.)
        pl.ylim(0, 100)
        pl.tight_layout()
        pl.show(block=False)
        pl.savefig(filename + '_samples')
        pl.close()

        pl.figure()
        h1 = pl.plot(t_plot, self.cost_results, 'b-', linewidth=3., label='Obstacle Cost')
        control_cost = [self.regulariser_beta * self.control_cost(t) for t in t_plot]
        h2 = pl.plot(t_plot, control_cost, 'k-', linewidth=3., label='Control Cost')
        h3 = pl.plot(t_plot, np.asarray(self.cost_results) + np.asarray(np.squeeze(control_cost)), 'r-', linewidth=3.,
                     label='Overall Cost')
        #  h2 = pl.plot(t_plot, control_cost_results, 'r-', linewidth=2., label='Control Cost')
        pl.gca().legend(loc='upper left', fontsize=24)
        pl.tick_params(axis='both', which='both', labelsize=20)
        pl.xlabel('t',fontsize=24)
        pl.ylabel('Cost',fontsize=24)


        pl.xlim(0., 1.)
        pl.ylim(0., 1.5)
        pl.tight_layout()

        ## pl.legend([h1,h2],['Obs Cost','Control Cost'])
        pl.show(block=False)
        pl.savefig(filename + '_objective')
        pl.close()

        pl.figure()

        try:
            pvals = [weight() / self.sampler._normaliser for weight in self.sampler._weights]
            x = np.linspace(start=0.,
                            stop=1.,
                            num=len(pvals) + 1,
                            endpoint=True)
            pvals.append(pvals[-1])
            pl.step(x, pvals, linewidth=3.)
            pl.xlim(0., 1.)
            pl.ylim(0, 0.2)
            pl.tick_params(axis='both', which='both', labelsize=20)
            pl.xlabel('t',fontsize=24)
            pl.ylabel('P',fontsize=24)
            pl.tight_layout()

            pl.savefig(filename + '_Q')


        except:
            pass
        pl.show(block=False)
        # pl.tight_layout(pad=1., w_pad=2.0, h_pad=2.0)
        pl.close()


def plan(hmap, start_point, end_point, dx_bound= None, number_of_features = 10,
         kernel_gamma=7., batch_size=10, grad_cap=0.2, cost_multiplier = 1.,
         reduce_obs_cost=np.max, regulariser_beta=0.0008, occ=None):




    planner = LinearReg_Planner(kerenl_approx_method="Nystroem",#kerenl_approx_method="Nystroem", #kerenl_approx_method="RFF",
                                D=start_point.size)

    planner.initialize_solution_step(number_of_features=number_of_features,
                                     gamma=kernel_gamma,
                                     start_point=start_point,
                                     end_point=end_point,
                                     dx_t0=dx_bound
                                     )

    planner.init_planner_with_map_model(hmap)

    if os.path.isfile("." + os.sep + "occ_map.p"):
        occ = pickle.load(open("." + os.sep + "occ_map.p", "rb"))
    else:
        occ = planner.plot_HS_map(1)
        pickle.dump(occ, open("." + os.sep + "occ_map.p", "wb"))
    planner.map2plot = occ
    planner.plot_HS_map(planner.figure.number, occ=occ)


    planner.sampler = WeightedSampler(num_intervals=10,
                                      history=5,
                                      path=planner.path
                                      )

    #  Planner callback/params setup
    planner.batch_size = batch_size
    planner.debug = False
    planner.grad_cap = grad_cap
    planner.cost_multiplier = cost_multiplier  # 0. sampler updated only with valid samples, 1.=use the obstacle cost for invalid samples

    planner.reduce_obs_cost = reduce_obs_cost
    planner.regulariser_beta = regulariser_beta
    planner.find_support = planner.stochastic_hilbert_map_occupancy
    planner.obs_cost = planner.hilbert_map_cost_at
    planner.control_cost = planner.control_cost_at_time
    planner.obs_cost_gradient = planner.obstacle_objective_gradient_at  # planner.hilbert_map_gradient_at
    planner.control_cost_gradient = planner.control_cost_grad_at_time

    planner.draw_samples_method = 'WeightedSampler'

    planner.learning_rate = learning_rate.BottouOptimal(
        eta0=80. * planner.batch_size, t0=160.)  # 0.5 # Typically 0.05


    #init main loop

    cost_i = 0.
    rkhs_iter = 0
    safe = False

    t_plot = np.linspace(start=0.,
                          stop=1.,
                          num=101,
                          endpoint=True
                          )

    max_entropy = planner.sampler.max_entropy()
    entropy = planner.sampler.entropy()
    safe_cntr = safe_cntr_init = 10

    planner.debug=True
    while ((not safe or
            entropy < 0.98 * max_entropy or
            safe_cntr > 0) and #  Want to increase entropy to ensure uniform sampling
           rkhs_iter < 700):
        print(rkhs_iter)

        current_cost = planner.plan()

        if planner.debug:  # not save_mode:
            #  Plotting curve00
            planner.path.plot_curve(fig_num=planner.figure.number,
                                    plotstr='c-',
                                    linewidth=1.5
                                    )

        planner.cost_results = planner.obs_cost(t_plot)
        cost_ip1 = planner.reduce_obs_cost(planner.cost_results)



        if cost_ip1 < planner.occupancy_free_thr:
            safe = True
            safe_cntr -= 1
        else:
            safe = False
            safe_cntr = safe_cntr_init

        d_cost = abs(cost_ip1 - cost_i)
        cost_i = cost_ip1


        rkhs_iter += 1
        entropy = planner.sampler.entropy()

    #  Saving results

    planner.plot_HS_map(fig_num=10,
                        occ=occ
                        )
    planner.path.plot_curve(fig_num=10,
                            plotstr='b-',
                            linewidth=2.,
                            )


def main(map_fname = "model.p"):

    hmap = pickle.load(open("." + os.sep + map_fname, "rb"))

    start_point = np.array([12.6, -6.6])
    end_point = np.array([3.6, -18.6])

    plan(hmap, start_point, end_point)

    return 0


if __name__ == "__main__":

    sys.exit(main())
