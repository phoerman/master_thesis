import math

import numpy as np
import matplotlib.pyplot as pl

from Path.AbstractMeanFunction import AbstractMeanFunction
from Utils.Utils import create_transform_matrix


class spline(object):

    def __init__(self, params):

        self.n = params.shape[-1]

        self.params = params
        self.D = self.params.shape[0]

        #end point and yaw relative to [0.,0.]
        self.end_point = self.__call__([1.])
        g = self.__grad__([1.])
        self.end_yaw = math.atan2(g[1], g[0])

    def __call__(self, t, *args, **kwargs):
        # angle can be cos(angle) or a conversion matrix
        x = []

        for t_i in t:
            assert 0. <= t_i <= 1., 't_i={}'.format(t_i)

            xs = 0
            for i in range(self.n):
                xs += np.power(t_i, i)*self.params[:, self.n-1-i]

            x.append(xs)

        return np.squeeze(np.asarray(x))

    def __grad__(self, t, *args, **kwargs):

        g = []
        #if not isinstance(t, list) or not isinstance(t, np.ndarray):
        #    t = [t]
        for t_i in t:
            assert 0. <= t_i <= 1.
            gs = 0
            for i in range(1, self.n):
                 gs += float(i)*np.power(t_i, i-1) * self.params[:, self.n - 1 - i] * (i > 0)

            g.append(gs)

        return np.squeeze(np.asarray(g))

    def direction(self, t, *args, **kwargs):

        d = []
        #if not isinstance(t, list) or not isinstance(t, np.ndarray):
        #    t = [t]
        g = self.__grad__(t)
        for t_i, g_i in zip(t,g):
            assert 0. <= t_i <= 1.
            d.append(math.atan2(g_i[1],g_i[0]))

        return np.squeeze(np.asarray(d))

    def length(self):

        x = self.__call__(np.linspace(0.,1.,num=5,endpoint=True))
        dx = np.linalg.norm(x[1:,:]-x[:-1,:], axis=0)

        return np.sum(dx)


class PiecewiseSplinePath(AbstractMeanFunction):

    def __init__(self, params_list, global_pose):
        super(PiecewiseSplinePath, self).__init__()

        self.num_seg = len(params_list)
        self.global_pose = global_pose
        self.splines = []

        spline_n = np.squeeze(np.asarray(params_list[0])).shape[-1]
        for i in range(self.num_seg):
            p = np.atleast_2d(params_list[i])
            assert spline_n == p.shape[-1]
            p[:, -1] *= 0. # to ensure contnuity
            self.splines.append(spline(p))

    def mean(self, t0):

        d = 1./self.num_seg

        path = []

        t_vec = np.asarray(t0).reshape(-1, 1)

        for t_i in t_vec:
            assert 0.<=t_i<=1.
            cnt = 0
            trans_mat = create_transform_matrix(self.global_pose[0], self.global_pose[1], self.global_pose[2])
            _global_yaw = self.global_pose[2]
            while (cnt+1)*d < t_i:
                #running on different spline branches
                x = self.splines[cnt].end_point
                _global_x = trans_mat.dot(np.concatenate((x, [1.])))[:2]
                _global_yaw += self.splines[cnt].end_yaw
                trans_mat = create_transform_matrix(_global_x[0],_global_x[1], _global_yaw)
                cnt += 1

            #last branch
            t_last = min((t_i - cnt*d)/d,1.) # this will put it in the range 0:1
            try:
                x = self.splines[cnt]([t_last])
            except:
                print('lulu')
            path.append(trans_mat.dot(np.concatenate((x, [1.])))[:2])

        return np.atleast_2d(path)

    def direction(self, t0):

        d = 1. / self.num_seg
        t_vec = np.asarray(t0).reshape(-1, 1)
        global_yaw = self.global_pose[2]

        yaw = []
        for t_i in t_vec:
            assert 0. <= t_i <= 1.
            cnt = 0
            _global_yaw = global_yaw
            while (cnt + 1) * d < t_i:
                # running on different spline branches
                _global_yaw += self.splines[cnt].end_yaw
                cnt += 1

            # last branch
            t_last = min((t_i - cnt * d) / d, 1.)  # this will put it in the range 0:1
            try:
                g = self.splines[cnt].__grad__([t_last])
                _global_yaw += math.atan2(g[1], g[0])
            except:
                print('lulu')
            yaw.append(_global_yaw)

        return np.atleast_1d(yaw)

    def gradient(self, t0):
        d = 1. / self.num_seg

        t_vec = np.asarray(t0).reshape(-1, 1)

        grad = []
        for t_i in t_vec:
            assert 0. <= t_i <= 1.
            cnt = 0
            trans_mat = create_transform_matrix(0., 0., self.global_pose[2])#just angle
            _global_yaw = self.global_pose[2]
            while (cnt + 1) * d < t_i:
                # running on different spline branches
                x = self.splines[cnt].end_point
                _global_x = trans_mat.dot(np.concatenate((x, [1.])))[:2]
                _global_yaw += self.splines[cnt].end_yaw
                trans_mat = create_transform_matrix(0., 0., _global_yaw)
                cnt += 1

            # last branch
            t_last = min((t_i - cnt * d) / d, 1.)  # this will put it in the range 0:1
            g = self.splines[cnt].__grad__([t_last])
            grad.append(trans_mat.dot(np.concatenate((g, [1.])))[:2])

        return np.atleast_2d(grad)

    def gradient2(self, t0):
        raise ValueError('PiecewiseSpline::gradient2 - untested')

    def length(self):
        l=0.
        for sp in self.splines:
            l += sp.length()

        return l

    def plot_curve(self, fig_num=None, t_end=1., plotstr='r-', linewidth=1., xlim=None, ylim=None):

        t = np.atleast_2d(np.linspace(0., t_end, 1000, endpoint=True)).T
        path = self.mean(t)

        if fig_num:
            pl.figure(fig_num)
        else:
            pl.figure()

        pl.plot(path[:, 0], path[:, 1], plotstr, linewidth=linewidth)

        if xlim:
            pl.xlim(xlim)
            pl.ylim(ylim)
        pl.show(block=False)

        return path

if __name__ == "__main__":

    '''
    p = PiecewiseSplinePath([[1.,2.,3.]])

    t = np.linspace(start=0.,
                    stop=1.,
                    num=100,
                    endpoint=True
                    )
    x = p.mean(t)
    import matplotlib.pyplot as pl

    pl.plot(t ,x,'r-')
    pl.show(block=False)

    p2 = PiecewiseSplinePath([[1., 2., 3.],[4.,5.,6.]])

    t = np.linspace(start=0.,
                    stop=1.,
                    num=100,
                    endpoint=True
                    )
    x = p2.mean(t)
    import matplotlib.pyplot as pl

    pl.plot(t, x, 'b-')
    pl.show(block=False)
    '''
    p3 = PiecewiseSplinePath([np.random.rand(2,3),np.random.rand(2,3)])

    t = np.linspace(start=0.,
                    stop=1.,
                    num=100,
                    endpoint=True
                    )
    x = p3.mean(t)

    pl.figure()
    pl.plot(x[:, 0], x[:, 1], 'b-')
    pl.show(block=False)

    print('lulu')

