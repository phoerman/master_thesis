class AbstractMeanFunction(object):

    def __init__(self):
        pass

    def mean(self, x):
        raise NotImplementedError("Mean function not implemented")

    def gradient(self, x):
        raise NotImplementedError("Gradient function not implemented")

    def gradient2(self, x):
        raise NotImplementedError("Gradient2 function not implemented")