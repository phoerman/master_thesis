import numpy as np
import matplotlib.pyplot as pl

from Path.AbstractMeanFunction import AbstractMeanFunction


class WaypointsPath(AbstractMeanFunction):

    def __init__(self,N):
        '''

        :param N: number of waypoints
        '''
        super(WaypointsPath, self).__init__()

        self.pos=[]
        self.n_waypoints = N

    def update(self, pos):
        assert len(pos) == self.n_waypoints

        self.pos = pos

    def mean(self, t0=None):

        if t0 is None:
            return self.pos
        else:
            t = np.asarray(t0).reshape(-1, )
            # transform into closest index
            idx = ((self.n_waypoints-1) * t).astype(int)
            return self.pos[idx]

    def gradient(self, t0):
        print('Waypoint::gradient - untested')

        t_vec = np.asarray(t0).reshape(-1, )
        idx = ((self.n_waypoints-1) * t_vec).astype(int)
        idx2 = idx + 1
        idx2[idx2 > self.n_waypoints-1] = self.n_waypoints - 1
        idx1 = idx - 1
        idx1[idx1 < 0] = 0
        x2 = self.pos[idx2]
        x1 = self.pos[idx1]

        return 0.5*self.n_waypoints*(x2-x1)

    def gradient2(self, t0):
        print('Waypoint::gradient2 - untested')

        t_vec = np.asarray(t0).reshape(-1, )
        idx = ((self.n_waypoints-1) * t_vec).astype(int)
        idx2 = idx + 1
        idx2[idx2 > self.n_waypoints - 1] = self.n_waypoints - 1
        idx1 = idx - 1
        idx1[idx1 < 0] = 0
        x2 = self.pos[idx2]
        x1 = self.pos[idx1]
        x0 = self.pos[idx]

        return 0.25*self.n_waypoints**self.n_waypoints*(x2+x1-2*x0)

    def plot_curve(self, fig_num=None,  plotstr='r*-',linewidth=1.):

        if fig_num:
            pl.figure(fig_num)
        else:
            pl.figure()
        path = np.squeeze(np.asarray(self.pos))

        pl.plot(path[:, 0], path[:, 1], plotstr, linewidth=linewidth)
        pl.show(block=False)

if __name__ == "__main__":

    print('Waypoint path')