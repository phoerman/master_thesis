import numpy as np
import math
import matplotlib.pyplot as pl
from scipy import interpolate
from Path.AbstractMeanFunction import AbstractMeanFunction


class PiecewiseLinearPath(AbstractMeanFunction):

    def __init__(self):
        super(PiecewiseLinearPath, self).__init__()

        self.t_support = []
        self.y_support = []
        self.f = None

    def update(self, t_list, pos):

        #  assumes sorted
        assert len(t_list) == len(pos)

        assert t_list == sorted(t_list)

        assert t_list[0] == 0.
        assert t_list[-1] == 1.

        self.t_support =t_list
        self.y_support = pos

        intrp_axis = 0#np.argmax(pos[0].shape)
        self.f= interpolate.interp1d(x=np.asarray(t_list),
                                     y=np.squeeze(np.asarray(pos)),
                                     kind='linear',
                                     axis=intrp_axis,
                                     copy=True,
                                     fill_value='extrapolate',
                                     assume_sorted=True
                                     )

    def mean(self, t0):

        t = np.asarray(t0).reshape(-1, )

        return self.f(t)

    def gradient(self, t0):

        dt = 0.001
        t_vec = np.asarray(t0).reshape(-1, )

        x2 = self.mean(t_vec+dt)
        x1 = self.mean(t_vec - dt)
        g = (x2-x1)/(2*dt)

        return g

    def gradient2(self, t0):
        print('PiecewiseLinear::gradient2 - untested')

        dt = 0.001
        t_vec = np.asarray(t0).reshape(-1, )

        g2 = self.gradient(t_vec + dt)
        g1 = self.gradient(t_vec - dt)
        g = (g2 - g1) / (2 * dt)

        return g

    def direction(self, t0):

        t_vec = np.asarray(t0).reshape(-1, )

        yaw = []
        for t_i in t_vec:
            g_i = np.squeeze(self.gradient(t_i))
            yaw.append(math.atan2(g_i[1], g_i[0]))
        return np.atleast_1d(yaw)

    def plot_curve(self, fig_num=None, t_end=1., plotstr='r-', linewidth=1., xlim=None, ylim=None):

        t = np.atleast_2d(np.linspace(0., t_end, 1000, endpoint=True)).T
        path = self.mean(t)

        if fig_num:
            pl.figure(fig_num)
        else:
            pl.figure()

        pl.plot(path[:, 0], path[:,1], plotstr, linewidth=linewidth)
        if xlim:
            pl.xlim(xlim)
            pl.ylim(ylim)
        pl.show(block=False)
        pl.show(block=False)

        return path

if __name__ == "__main__":

    p = PiecewiseLinearPath()
    t = [0., 0.25, 1.]
    a = np.array([[0., 3.]])
    b = np.array([[5., 8.]])
    c = np.array([[25., 3.]])
    p.update([0., 0.25, 1.], [a,b,c])

    print(p.mean([0., 0.1, 0.25, 0.5, 0.5, 1.]))

    x= p.mean(np.linspace(start=0.,
                          stop=1.,
                          num=100,
                          endpoint=True
                          )
              )
    import matplotlib.pyplot as pl

    pl.plot(x[0,:],x[1,:],'r-')
    for p_i in p.y_support:
        pl.plot(p_i[0,0],p_i[0,1],'b*')

    pl.show(block=False)

    print('lulu')

