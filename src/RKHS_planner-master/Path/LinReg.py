import math
import numpy as np

import matplotlib.pyplot as pl

from Path.AbstractMeanFunction import AbstractMeanFunction
from features.AbstractFeature import AbstractFeature
from features.rbf import RBF
from features.rff import RFF

class LinReg_path(AbstractMeanFunction):

    def __init__(self):
        super(LinReg_path, self).__init__()
        self.path_mean = AbstractMeanFunction()

        self.features = AbstractFeature()

        self.weights = np.array([])

        self.boundary_features = AbstractFeature()
        self.boundary_weights = np.array([])

    def _initialize_features_and_weights(self, method="Nystroem", n_components=10, gamma=10., D=2):

        if method == 'Nystroem':
            features = RBF(n_components=n_components,
                                 gamma=gamma)

        elif method == 'RFF':
            features = RFF(n_components=n_components,
                                gamma=gamma)

        else:
            raise ValueError('LinReg.initialize_features: Invalid method')

        weights  = np.zeros((n_components, D))

        return features, weights

    def initialize_features(self, method="Nystroem", n_components=10, gamma=10., D=2):

        self.features, self.weights = self._initialize_features_and_weights(method=method,
                                                                            n_components=n_components,
                                                                            gamma=gamma,
                                                                            D=D
                                                                            )

    def initialize_boundary_features(self, boundary_conditions, gamma=10., D=2):

        #  _initialize_features_and_weights will create equally spaced kernel
        self.boundary_features, self.boundary_weights = \
            self._initialize_features_and_weights(method="Nystroem",
                                                  n_components=len(boundary_conditions),
                                                  gamma=gamma,
                                                  D=D
                                                  )
        # Modify centers to coincide with boundary features
        self.boundary_features.set_basis(boundary_conditions,
                                         gamma
                                         )

    def initialize_path_with_waypoints(self, t_waypoint, y_waypoint, gamma=10., D=2):

        from Path.LinearMeanFunction import LinearMeanFunction

        # find end points
        t0_idx = np.asscalar(np.where(t_waypoint == 0.)[0])
        t1_idx = np.asscalar(np.where(t_waypoint == 1.)[0])

        self.path_mean = LinearMeanFunction(x0=y_waypoint[t0_idx],
                                            x1=y_waypoint[t1_idx]
                                            )

        self.initialize_boundary_features(np.fromiter(iter(t_waypoint), dtype=float),
                                          gamma=gamma,
                                          D=D
                                          )

        self.initialize_features(D=D)  # Empty update - to ensure feature are not None

        K = self.boundary_features.evaluate(np.fromiter(iter(t_waypoint), dtype=float))
        b = np.vstack(y_waypoint) - self.path_mean.mean(t_waypoint)
        self.boundary_weights = np.linalg.solve(K, b)

        self.plot_curve(1)

    def mean(self, t):
        t_vec = np.asarray(t).reshape(-1, 1)
        phi_t = self.path_mean.mean(t_vec)

        # Boundary conditions
        if self.boundary_features:
            phi_t += self.boundary_features.evaluate(t_vec).dot(self.boundary_weights)
        # Basis functions
        if self.features:
            phi_t += self.features.evaluate(t_vec).dot(self.weights)

        return phi_t

    def gradient(self, t):
        t_vec = np.asarray(t).reshape(-1, 1)

        phi_grad = self.path_mean.gradient(t_vec)

        # Boundary conditions
        if self.boundary_features:
            phi_grad += self.boundary_features.gradient(t_vec).dot(self.boundary_weights)
        # Basis functions
        if self.features:
            phi_grad += self.features.gradient(t_vec).dot(self.weights)

        return phi_grad

    def gradient2(self, t):
        t_vec = np.asarray(t).reshape(-1, 1)

        phi_grad2 = self.path_mean.gradient2(t_vec)

        # Boundary conditions
        if self.boundary_features:
            phi_grad2 += self.boundary_features.gradient2(t_vec).dot(self.boundary_weights)

        # Basis functions
        if self.features:
            phi_grad2 += self.features.gradient2(t_vec).dot(self.weights)

        return phi_grad2

    def direction(self, t0):

        g = self.gradient(t0)

        yaw = []
        for g_i in g:
            yaw.append(math.atan2(g_i[1], g_i[0]))
        return np.atleast_1d(yaw)
    def curve_at_t(self, t):
        return self.mean(t)

    def curve_grad_at_t(self, t):
        return self.gradient(t)

    def plot_curve(self, fig_num=None, t_end=1., plotstr='r-', linewidth=1., xlim=None, ylim=None):

        t = np.atleast_2d(np.linspace(0., t_end, 1000, endpoint=True)).T
        path = self.mean(t)

        if fig_num:
            pl.figure(fig_num)
        else:
            pl.figure()

        pl.plot(*[path[:, i] for i in range(path.shape[1])], plotstr, linewidth=linewidth)
        if xlim:
            pl.xlim(xlim)
            pl.ylim(ylim)
        pl.show(block=False)
        pl.show(block=False)

        return path

    def plot_features(self):

        pl.figure()
        t = np.atleast_2d(np.linspace(0., 1., 1000, endpoint=True)).T
        t_vec = np.asarray(t).reshape(-1, 1)

        features = self.features.evaluate(t_vec)
        n =  features.shape[1]
        color = pl.cm.rainbow(np.linspace(0, 1, n))
        for i, c in zip(range(n), color):
            pl.gca().plot(t_vec, features[:,i], c=c)
        pl.xlabel('t')
        pl.ylabel('Normalised feature')

        return pl.gcf().number

def exmaple_1d():
    '''
    exmplae where the input sapce is 1D : t=[0:1]
    :return:
    '''

    start_point = np.array([1., 2.])
    end_point = np.array([15., 12.])
    gamma = 10.
    D = 2  # dimensionality of output problem, dimensionality of input is assumed 1D
    n_components = 10

    from Path.LinearMeanFunction import LinearMeanFunction
    path = LinReg_path()
    path.path_mean = LinearMeanFunction(x0=start_point,
                                        x1=end_point
                                        )
    boundary_conditions = dict()
    boundary_conditions[0.] = start_point
    boundary_conditions[1.] = end_point

    path.initialize_features(method="RFF",
                             n_components=n_components,
                             gamma=gamma,
                             D=D
                             )

    path.weights = 5 * np.random.rand(n_components, D)  # Generating a random curve for debugging
    #  Prepare boundary condition for a simple online update
    if boundary_conditions:
        path.initialize_boundary_features(np.fromiter(iter(boundary_conditions.keys()), dtype=float),
                                          gamma=gamma,
                                          D=D
                                          )
        # boundary_conditions_inv_K = np.linalg.pinv(K) -> stored in path.boundary_features.sampler.normalization_

        # Test prediction

    t = [0.2, 0.4]
    print('test scalar input\n')
    for t_i in t:
        print('path(t={0}) = {1}'.format(t_i, path.mean(t_i)))
        print('path grad(t={0}) = {1}'.format(t_i, path.gradient(t_i)))
        print('path grad2(t={0}) = {1}'.format(t_i, path.gradient2(t_i)))

    t = [0.2, 0.4]
    print('test list input\n')
    print('path(t={0}) = {1}'.format(t, path.mean(t)))
    print('path grad(t={0}) = {1}'.format(t, path.gradient(t)))
    print('path grad2(t={0}) = {1}'.format(t, path.gradient2(t)))

    t = np.array([0.2, 0.4])
    print('test numpy array input\n')
    print('path(t={0}) = {1}'.format(t, path.mean(t)))
    print('path grad(t={0}) = {1}'.format(t, path.gradient(t)))
    print('path grad2(t={0}) = {1}'.format(t, path.gradient2(t)))

    h = pl.figure()
    path.plot_curve(h.number)

    # Analyze gradient and gradient2
    dt = 0.01
    t_plot = np.arange(start=0.,
                       stop=1. + dt,
                       step=dt)

    X = path.mean(t_plot)

    numerical_grad = (X[1:, :]-X[:-1, :])/dt

    pl.figure()
    pl.plot(t_plot[1:], numerical_grad, 'm-.')
    pl.plot(t_plot, path.gradient(t_plot), 'k')
    pl.title('gradient')
    pl.show(block=False)

    numerical_grad_2 = (numerical_grad[1:, :] - numerical_grad[:-1, :]) / dt
    pl.figure()
    pl.plot(t_plot[2:], numerical_grad_2, 'm-.')
    pl.plot(t_plot, path.gradient2(t_plot), 'k')
    pl.title('gradient2')
    pl.show(block=False)


if __name__ == "__main__":

    exmaple_1d()

    print('Cmon, you sons of bitches! Do you want to live forever?')


