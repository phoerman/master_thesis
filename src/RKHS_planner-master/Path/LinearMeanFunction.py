import numpy as np
from Path.AbstractMeanFunction import AbstractMeanFunction


class LinearMeanFunction(AbstractMeanFunction):

    def __init__(self, x0, x1):
        super(LinearMeanFunction, self).__init__()

        self.slope = (x1 - x0)[np.newaxis, :]
        self.intercept = x0[np.newaxis, :]

    def mean(self, t0):
        t = np.asarray(t0).reshape(-1, 1)

        return t.dot(self.slope) + np.ones_like(t).dot(self.intercept)

    def gradient(self, t0):
        t = np.asarray(t0).reshape(-1, 1)

        return np.ones_like(t).dot(self.slope)

    def gradient2(self, t0):
        t = np.asarray(t0).reshape(-1, 1)

        return np.zeros_like(t).dot(self.slope)