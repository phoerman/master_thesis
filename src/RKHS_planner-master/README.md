# RKHS_planner
Functioal Stochastic gradient trajectory optmiser using Hilbert maps

This repository contains the code which implements the method of the paper 
"Fast Stochastic Functional Path Planning in Occupancy Maps" by Gilad Francis, Lionel Ott and Fabio Ramos and presented at ICRA 2019

# Software Requirements

To run the code you need the following software components:

    Python
    scikit-learn
    matplotlib
    NumPy
    collections
    Hilbert maps (can be found @ https://bitbucket.org/LionelOtt/hilbert_maps_rss2015/src/master/)
    

# Running the Example

The script Lin_reg_planner.py runs a planning iteration on a Hilbert map of the Intel Lab. simple way to produce a map using hilbert maps from carmen style logfiles as follows:
