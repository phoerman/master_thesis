import os
import warnings
import numpy as np
import scipy.io
import matplotlib.pyplot as pl
from Utils.Utils import *
from Utils.occ_map_handler import *
import util
import hilbert_map as hsm

class AbstractFunctionalPlanner(object):

    def __init__(self, D):
        self.D = D  # degrees of freedom
        self.n_time = 100
        self.t = np.atleast_2d(np.linspace(0., 1., self.n_time, endpoint=True)).T
        self.psi_t_0 = np.zeros((self.D, 1))  # start point
        self.psi_t_1 = np.zeros((self.D, 1))  # end point

        self.feature = {}
        self.boundary_conditions = {}
        self.grad_boundary_conditions = {}
        self.support = []
        self.n_RKHS_sections = 20
        self.points_assessed = 0

        self.obstacles = []
        self.grid_x = np.zeros(1)
        self.grid_y = np.zeros(1)
        self.grid_z = np.zeros(1)

        self.costmap = np.zeros(1)
        self.costmap_dx = np.zeros(1)
        self.costmap_dy = np.zeros(1)
        self.safety_buffer = 0.3
        self.obstacle_size = 2.
        self.robot_size = None
        self.debug = True
        try:
            self.figure = pl.figure()
        except:
            self.figure = None
        self.xlim = None
        self.ylim = None

        self.find_support = None
        self.reduce_obs_cost = None
        self.obs_cost = None
        self.control_cost = None
        self.obs_cost_gradient = None
        self.control_cost_gradient = None
        self.regulariser_beta = None
        self.path = None
        self.occupancy_free_thr = 0.4
        self.cost_results = []
        '''
         Hilbert map
         '''
        self.hilbert_map = hsm.SparseHilbertMap
        self._extent = None  # Used for plotting of map

        '''
            Work With Matlab mats
            '''
        self.ground_truth_map = None
        self.map_origin = None
        self.map_size = None

    def plot_curve(self, fig_num=None, plotstr='r-', linewidth=1.):

        #m_path, v_path = self.path.curve_at_t(self.t)

        if fig_num is None:
            fig_num = self.figure.number

        pl.figure(fig_num)
        self.path.plot_curve(fig_num=fig_num,
                            plotstr=plotstr,
                            linewidth=linewidth)
        start = np.squeeze(self.psi_t_0)
        goal = np.squeeze(self.psi_t_1)
        pl.scatter(start[0], start[1], marker='*', s=100)
        pl.scatter(goal[0], goal[1], marker='p', s=100)

        if self.xlim:
            pl.xlim(self.xlim)
            pl.ylim(self.ylim)
        pl.show(block=False)

    def hilbert_map_init(self,
                         map_bounds=None,
                         Ncntrs=100,
                         alpha=0.0027,
                         l1_ratio=0.11,
                         gamma=.5,
                         loss='log',
                         penalty='elasticnet'
                         ):
        '''
        Init of hilbert a with default params
        Init of hilbert a with default params
        :return: hilbert map
        '''

        assert map_bounds is not None

        map_centers_x, map_centers_y = np.meshgrid(np.linspace(map_bounds[0], map_bounds[2], Ncntrs, endpoint=True),
                                                   np.linspace(map_bounds[1], map_bounds[3], Ncntrs, endpoint=True))

        dl = 0.2
        self.grid_x = np.arange(map_bounds[0], map_bounds[2], dl)
        self.grid_y = np.arange(map_bounds[1], map_bounds[3], dl)





        self.hilbert_map = hsm.SparseHilbertMap(np.vstack((map_centers_x.ravel(), map_centers_y.ravel())).T,
                                                gamma=gamma
                                                )

        self.hilbert_map.set(alpha=alpha,
                             l1_ratio=l1_ratio,
                             gamma=gamma,
                             loss=loss,
                             penalty=penalty
                             )
        self.xlim = min(self.grid_x), max(self.grid_x)
        self.ylim = max(self.grid_y), min(self.grid_y)

    def hilbert_map_update(self,
                           robot_pose,
                           ranges,
                           N,
                           max_range,
                           ignore_free_beams=False,
                           fix_incermental_weights=False):
        '''
        Update hilbert map with new laser observation - incermentally
        number of set
        :return:
        '''
        if N ==1:
            p=[]
            p.append(robot_pose)
            robot_pose = p
            r=[]
            r.append(ranges)
            ranges = r

        for i in range(N):
            data, label = util.genarate_dataset_for_single_scan(robot_pose[i],
                                                                ranges[i],
                                                                max_lrf_dist=max_range,
                                                                ignore_free_beams=ignore_free_beams
                                                                )
            if len(data) == 0:
                warnings.warn('ExplorationPlanner:hilbert_map_update - empty data')
            else:
                self.hilbert_map.add(data, label, fix_incermental_weights)

    def init_planner_with_map_model(self, model):

        self.hilbert_map = model
        if hasattr(model, 'use_rkhs'): # old Hilbert map model
            self.hilbert_map.rkhs = model.use_rkhs
        if hasattr(model, 'rkhs'):  # new Hilbert map model
            self.hilbert_map.use_rkhs = model.rkhs

        # Defining map limits for visualization purpooses
        map_limits = np.vstack((np.min(np.asarray(self.hilbert_map.centers), axis=0),
                                np.max(np.asarray(self.hilbert_map.centers), axis=0))).T
        self.grid_x = map_limits[0, :]
        self.grid_y = map_limits[1, :]

    def initialize_obstacles_constant_set(self, case):
        if case == 1:
            self.obstacles = [np.array([[5.67371631], [10.86692023]]),
                              np.array([[9.86954387], [6.85713553]]),
                              np.array([[1.40961292], [12.43672021]]),
                              np.array([[14.45809716], [4.31058469]]),
                              np.array([[7.09446463], [2.3359456]]),
                              np.array([[4.5295176], [6.56468294]]),
                              np.array([[8.85002214], [14.95457972]]),
                              np.array([[9.82430166], [10.8691609]]),
                              np.array([[0.07136534], [7.02524456]]),
                              np.array([[13.1918632], [0.20088641]])]
        if case == 2:
            self.obstacles = [np.array([[6.98343951], [10.81807272]]),
                              np.array([[5.84327016], [5.01547755]]),
                              np.array([[14.1217905], [2.6504399]]),
                              np.array([[12.51657375], [6.93611191]]),
                              np.array([[1.19851681],  [5.61893243]]),
                              np.array([[7.69792254], [0.95111313]]),
                              np.array([[1.19869293], [9.66475987]]),
                              np.array([[8.12206557], [14.98346206]]),
                              ]
        if case == 3:
            self.obstacles = [np.array([[5.84327016], [5.01547755]]),
                              np.array([[14.1217905], [2.6504399]]),
                              np.array([[10.275], [8.091]]),
                              np.array([[1.19869293], [9.66475987]]),
                              np.array([[8.12206557], [14.98346206]]),
                              ]
        if case == 4:  # GOOD FOR hILBERT MAPS
            self.obstacles = [np.array([[8.52724988], [10.62116344]]),
                              np.array([[6.15757909], [0.31455697]]),
                              np.array([[11.31393465], [1.4148183]]),
                              np.array([[2.13866368], [7.48933294]]),
                              np.array([[6.7342983], [6.12553222]]),
                              np.array([[13.64985161], [5.96939136]]),
                              np.array([[4.13581437], [11.64463223]]),
                              np.array([[10.71202432], [14.99172393]]),
                              np.array([[0.0701097], [11.84373921]]),
                              ]
        if case == 5:  # debug
            self.obstacles = [np.array([[7.76358369], [16.14108677]]),
                              np.array([[3.456], [7.817]]),
                              np.array([[8.803], [3.2478]]),
                              np.array([[9.9481], [9.6421]]),
                              np.array([[2.4694], [14.0296]]),
                              np.array([[14.5986], [0.6154]]),
                              np.array([[14.2899], [7.511]]),
                              ]
        if case == 6:  # exploration
            self.obstacles = [np.array([[7.76358369], [16.14108677]]),
                              np.array([[3.456], [7.817]]),
                              np.array([[8.803], [3.2478]]),
                              np.array([[9.9481], [9.6421]]),
                              np.array([[2.4694], [14.0296]]),
                              np.array([[14.5986], [0.6154]])
                              ]

    def initialize_obstacles(self, n_obstacle, scaler):
        """
        Generate obstacles that are not close to end or start point
        :param n_obstacle: number of randimly generated obstaclkes
        :param scaler: scalar from [0:1] to [0:scaler] or if num of obstacles=0 chooses from a fixed set defined by scaler
        :return:
        """


        if n_obstacle == 0:
            self.initialize_obstacles_constant_set(scaler)
        else:
            if np.all(self.psi_t_0 == self.psi_t_1):
                raise ValueError('RKHS_trajectory:initialize_obstacles - start and end point not initialized')
            for obs_i in range(0, n_obstacle):
                obs_ok = False
                try_cnt = 0
                while not obs_ok and try_cnt < 50:
                    try_cnt += 1
                    obs_cntr = scaler * np.random.rand(self.D, 1)
                    # X = [np.all(np.abs(obs_cntr - other_obs) < self.obstacle_size) for other_obs in self.obstacles]
                    if (np.all(np.abs(obs_cntr - self.psi_t_0) < 2.*self.obstacle_size)) or \
                       (np.all(np.abs(obs_cntr - self.psi_t_1) < 2.*self.obstacle_size)) or \
                       (any([np.all(np.abs(obs_cntr - other_obs) < 2.*self.obstacle_size) for
                        other_obs in self.obstacles])):

                        obs_ok = False
                    else:
                        obs_ok = True
                        self.obstacles.append(obs_cntr)

        dl = 0.1
        obs_centers = np.squeeze(np.asarray(self.obstacles))

        self.grid_x = np.arange(np.min(obs_centers[:, 0]) - 1.1 * self.obstacle_size,
                                np.max(obs_centers[:, 0]) + 1.1 * self.obstacle_size, dl)
        self.grid_y = np.arange(np.min(obs_centers[:, 1]) - 1.1 * self.obstacle_size,
                                np.max(obs_centers[:, 1]) + 1.1 * self.obstacle_size, dl)

    def calc_cost_map(self, dl):

        X, Y = np.meshgrid(self.grid_x, self.grid_y)
        raveled_map = np.vstack((X.ravel()[np.newaxis, :], Y.ravel()[np.newaxis, :]))
        d = distance_r(np.squeeze(np.asarray(self.obstacles)).T, raveled_map)
        cost = np.sum(self.obstacles_potential_cost(d), axis=0)
        self.costmap = np.reshape(cost, X.shape)

        dmap = (self.costmap[:, 2:] - self.costmap[:, :-2])/(2. * dl)
        self.costmap_dx = np.hstack((np.atleast_2d(dmap[:, 0]).T, dmap, np.atleast_2d(dmap[:, -1]).T))
        dmap = (self.costmap[2:, :] - self.costmap[:-2, :]) / (2. * dl)
        self.costmap_dy = np.vstack((np.atleast_2d(dmap[0, :]), dmap, np.atleast_2d(dmap[-1, :])))

        self.xlim = min(self.grid_x), max(self.grid_x)
        self.ylim = max(self.grid_y), min(self.grid_y)

        if self.debug:
            extent = (min(self.grid_x), max(self.grid_x), max(self.grid_y), min(self.grid_y))
            self.plot_cost_map(1)

            if self.debug:
                pl.figure(100)
                pl.imshow(self.costmap_dx, extent=extent)
                pl.title('dx')
                pl.colorbar()
                pl.show(block=False)
                pl.figure(101)
                pl.imshow(self.costmap_dy, extent=extent)
                pl.title('dy')
                pl.colorbar()
                pl.show(block=False)

    def obstacles_potential_cost(self, d):
        # assumes obstacles size is the same

        cost = np.zeros(d.shape, dtype=float)
        # i = np.argwhere(d < self.obstacle_size+self.safety_buffer)
        cost[d < self.obstacle_size + self.safety_buffer] = ((d[d < self.obstacle_size + self.safety_buffer] -
                                                              self.obstacle_size) - self.safety_buffer) ** 2 / (
                                                              2 * self.safety_buffer)
    # i = np.argwhere(d < self.obstacle_size)
        cost[d < self.obstacle_size] = -(d[d < self.obstacle_size] - self.obstacle_size) + 0.5 * self.safety_buffer
        return cost

    def plot_obstacles(self, fig_num=None, color='b'):

        if fig_num is None:
            fig_num = 500

        fig = pl.figure(fig_num)

        #Handle different types of obstacles - circular and Matlab map
        if self.ground_truth_map is None: # circlar obstacles

            index = 1
            for obs in self.obstacles:
                circ = pl.Circle((obs[0], obs[1]), radius=self.obstacle_size, color=color, linestyle='dashed', fill=False)
                circ.set_label(str(index))

                fig.gca().add_artist(circ)

                pl.text(obs[0], obs[1], index, horizontalalignment='center',
                        verticalalignment='center')
                index += 1
        elif hasattr(self.obstacles, "occupancy"): #map loaded from png
            self.plot_png_obstacles(fig_num)
        else:  # Matlab obstacles
            self.plot_matlab_obstacles(fig_num)

    def save_frame(self, map, index=None):

        pl.figure()
        extent = (np.min(self.grid_x),  np.max(self.grid_x), np.max(self.grid_y),  np.min(self.grid_y))
        pl.imshow(map, extent=extent, interpolation='bilinear', cmap='hot', clim=(0., 1.))

        pl.tick_params(
            axis='both',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom='off',  # ticks along the bottom edge are off
            top='off',  # ticks along the top edge are off
            left='off',
            right='off',
            labelbottom='off',
            labelleft='off')

        self.plot_curve(fig_num=pl.gcf().number, plotstr='b-',linewidth = 2)
        pl.savefig('Results'+os.sep+'Planner_%05d.png' % index)
        pl.close(pl.gcf())


    def distance_to_closest_obstacle(self, pos):
        return np.min(distance_r(np.squeeze(np.asarray(self.obstacles)).T, pos), axis=0)

    def create_HS_map(self,
                      train_hyperparam=True,
                      alpha=None,
                      l1_ratio=None,
                      gamma=None,
                      loss='log',
                      penalty = 'elasticnet'
                      ):

        if train_hyperparam:
           alpha, l1_ratio, gamma, accuracy = self.hmap_grid_search()

        if alpha is None:
            alpha = 0.0015

        if l1_ratio is None:
            l1_ratio = 0.6

        if gamma is None:
            gamma = 0.7

        self.hilbert_map = self.generate_HS_map(N_training_point = 20000,
                                                alpha=alpha,
                                                l1_ratio=l1_ratio,
                                                gamma=gamma,
                                                loss=loss,
                                                penalty = penalty
                                                )
        self.hm_gamma = gamma

        if False: #  self.debug:
            self.plot_HS_map(1000)

    def generate_HS_map(self,
                        N_training_point=10000,
                        alpha=None,
                        l1_ratio=None,
                        gamma=None,
                        loss='log',
                        penalty = 'elasticnet'
                        ):

        '''
        generate data, labels from obstacles as input to maps
        :return: creates a hilbert map
        '''

        #Generate map instatiation
        #find the size of map
        map_min = np.array([np.min(self.grid_x), np.min(self.grid_y)])
        map_max = np.array([np.max(self.grid_x), np.max(self.grid_y)])

        Ncntrs = 100
        map_centers_x, map_centers_y = np.meshgrid(np.linspace(map_min[0], map_max[0], Ncntrs, endpoint=True),
                                                   np.linspace(map_min[1], map_max[1], Ncntrs, endpoint=True),
                                                   indexing='ij')

        hilbert_map = self.hilbert_map(np.vstack((map_centers_x.ravel(),map_centers_y.ravel())).T,
                                            gamma = gamma)

        hilbert_map.set(alpha = alpha, l1_ratio=l1_ratio, gamma=gamma, loss=loss, penalty = 'elasticnet')

        if self.ground_truth_map is None:
            training_data, training_labels = self.generate_hmap_training_data(N_training_point)
        else:
            training_data, training_labels = self.generate_hmap_training_data_for_mat_file(N_training_point)

        hilbert_map.add(training_data, np.squeeze(training_labels))

        if False: #self.debug:
            fig = pl.figure(600)
            self.plot_obstacles(fig.number)
            pl.scatter(training_data[0, training_labels<0.5],training_data[1, training_labels<0.5], color = 'b')
            pl.scatter(training_data[0, training_labels>0.5], training_data[1, training_labels>0.5], color = 'r')
            pl.show(block=False)

        return hilbert_map

    def generate_hmap_training_data(self,N_training_point):
        """
        return a mix of free (95%) and occupied (5%) training poings based on obstacles
        :param N_training_point: number of training points
        :return: data (free and occupied)  and corresponding labels
        """
        map_min = np.array([np.min(self.grid_x), np.min(self.grid_y)])
        map_max = np.array([np.max(self.grid_x), np.max(self.grid_y)])

        number_free_points = N_training_point
        rand_pos = map_min[:, np.newaxis] + np.diagflat(map_max - map_min).dot(
            np.random.rand(self.D, number_free_points))
        d = self.distance_to_closest_obstacle(rand_pos)
        free_data = rand_pos[:, d > self.obstacle_size + 5 * np.spacing(self.obstacle_size)].copy()
        free_labels = np.zeros((1, free_data.shape[1]), dtype=float)

        number_occupied_points = int(number_free_points / 20)  # assumes 10 free points for each laser beam
        occupied_data = []
        for obs_cntr in self.obstacles:
            rand_angle = np.random.rand(1, number_occupied_points) * 2. * np.pi
            occupied_data_i = obs_cntr + self.obstacle_size * np.vstack((np.cos(rand_angle), np.sin(rand_angle)))
            occupied_data.append(occupied_data_i)

        occupied_data = np.concatenate(occupied_data, axis=1)
        occupied_labels = np.ones((1, occupied_data.shape[1]), dtype=float)
        return np.concatenate((free_data,occupied_data),axis=1).T, np.concatenate((free_labels,occupied_labels),axis=1).T

    def hmap_grid_search(self):
        gamma = np.arange(0.5,8.,0.5)
        alpha = np.arange(0.0001,0.005,0.0002)
        l1_norm = np.arange(0.01,0.8,0.1)

        cv_data, cv_labels = self.generate_hmap_training_data(2000)

        occ_thr = 0.80
        free_thr = 0.2

        best_accuracy = 0.
        for gamma_i in gamma:
            for alpha_i in alpha:
                for l1_i in l1_norm:
                    hilbert_map = self.generate_HS_map(N_training_point = 1000, alpha=alpha_i,l1_ratio=l1_i, gamma=gamma_i)
                    prediction = hilbert_map.classify(cv_data)

                    #TP
                    TP = np.count_nonzero(np.logical_and(prediction[:,1] > occ_thr,  np.squeeze(cv_labels > occ_thr)))
                    #TN
                    TN = np.count_nonzero(np.logical_and(prediction[:,1] < free_thr,  np.squeeze(cv_labels < free_thr)))

                    #FP
                    FP = np.count_nonzero(np.logical_and(prediction[:,1] > occ_thr,  np.squeeze(cv_labels < occ_thr)))

                    # FN
                    FN = np.count_nonzero(np.logical_and(prediction[:,1] < free_thr,  np.squeeze(cv_labels > free_thr)))

                    accuracy = (TP + TN)/float(len( np.squeeze(cv_labels )))
                    print('alpha={0}, l1={1}, gamma={2}, accuracy={3}'.format(alpha_i,l1_i,gamma_i,accuracy))
                    if accuracy > best_accuracy:
                        best_accuracy = accuracy
                        best_config = [alpha_i, l1_i, gamma_i]

        print('Best setup')
        print('alpha={0}, l1={1}, gamma={2}, accuracy={3}'.format(best_config[0], best_config[1], best_config[2], best_accuracy))
        return best_config[0], best_config[1], best_config[2],best_accuracy

    def plot_cost_map(self, fig_num=None):

        if self.costmap is None:
            extent = (min(self.grid_x), max(self.grid_x), max(self.grid_y), min(self.grid_y))
            self.calc_cost_map(dl=0.1)
            self._extent = extent
        elif self._extent is not None:
            extent = self._extent
        else:
            extent = (np.min(self.grid_x), np.max(self.grid_x), np.max(self.grid_y), np.min(self.grid_y))  # Note order of Y

        if fig_num is None:
            fig_num = 1000

        fig = pl.figure(fig_num)

        pl.imshow(self.costmap, extent=extent, interpolation='bilinear')
        pl.colorbar()
        self.plot_obstacles(fig_num)
        pl.show(block=False)

    def plot_HS_map(self,fig_num=None, occ=None, res=0.2, external_limit = None):

        subdivide = 1000
        if occ is None:
            dl = res
            query_x, query_y = np.meshgrid(np.arange(np.min(self.grid_x), np.max(self.grid_x), dl),
                                       np.arange(np.min(self.grid_y), np.max(self.grid_y), dl))

            extent = (np.min(query_x.ravel()), np.max(query_x.ravel()), np.max(query_y.ravel()), np.min(query_y.ravel()))   # Note order of Y

            query_all = np.vstack((query_x.ravel(),query_y.ravel())).T
            occ=np.array([])
            for query in np.array_split(query_all,int(query_all.shape[0]/subdivide)+1 ,axis=0):
                occ_q = self.hilbert_map.classify(query)[:,1]

                occ = np.hstack([occ, occ_q]) if occ.size else occ_q

            occ = np.reshape(occ,(query_x.shape[0], query_y.shape[1]))

            self._extent = extent

        elif self._extent is not None:
            extent = self._extent
        else:
            extent = (np.min(self.grid_x), np.max(self.grid_x), np.max(self.grid_y), np.min(self.grid_y))  # Note order of Y

        if fig_num is None:
            fig_num = 1000

        fig = pl.figure(fig_num)
        pl.clf()

        pl.imshow(occ, extent=extent, interpolation='bilinear', cmap='hot', clim=(0.,1.))
        pl.colorbar()
        self.plot_obstacles(fig_num)
        pl.show(block=False)

        if external_limit is None:
            self.xlim = extent[0],extent[1]  # min(self.grid_x), max(self.grid_x)
            self.ylim = extent[2],extent[3]  # max(self.grid_y), min(self.grid_y)
        else:
            self.xlim = external_limit[0], external_limit[1]  # min(self.grid_x), max(self.grid_x)
            self.ylim = external_limit[2], external_limit[3]  # max(self.grid_y), min(self.grid_y)
            pl.xlim(self.xlim)
            pl.ylim(self.ylim)

        return occ

    def plot_HS_quiver_map(self, fig_num=None, occ=None, res=0.2, external_limit=None):

        subdivide = 1000
        if occ is None:
            dl = res
            query_x, query_y = np.meshgrid(np.arange(np.min(self.grid_x), np.max(self.grid_x), dl),
                                           np.arange(np.min(self.grid_y), np.max(self.grid_y), dl))

            extent = (np.min(query_x.ravel()), np.max(query_x.ravel()), np.max(query_y.ravel()),
                      np.min(query_y.ravel()))  # Note order of Y

            query_all = np.vstack((query_x.ravel(), query_y.ravel())).T
            occ = np.array([])
            grad_x = np.array([])
            grad_y = np.array([])
            grad_z = np.array([])
            for query in np.array_split(query_all, int(query_all.shape[0] / subdivide) + 1, axis=0):
                occ_q = self.hilbert_map.classify(query)[:, 1]

                occ = np.hstack([occ, occ_q]) if occ.size else occ_q

                grad_q = self.hilbert_map.classify_grad(query)
                grad_x = np.hstack([grad_x, np.squeeze(grad_q[0])]) if grad_x.size else np.squeeze(grad_q[0])
                grad_y = np.hstack([grad_y, np.squeeze(grad_q[1])]) if grad_y.size else np.squeeze(grad_q[1])
            #    grad_z = np.hstack([grad_y, np.squeeze(grad_q[2])]) if grad_y.size else np.squeeze(grad_q[2])

            # Occ map
            occ = np.reshape(occ, (query_x.shape[0], query_y.shape[1]))
            grad_x = np.reshape(grad_x, (query_x.shape[0], query_y.shape[1]))
            grad_y = np.reshape(grad_y, (query_x.shape[0], query_y.shape[1]))




            self._extent = extent

        elif self._extent is not None:
            extent = self._extent
        else:
            extent = (
            np.min(self.grid_x), np.max(self.grid_x), np.max(self.grid_y), np.min(self.grid_y))  # Note order of Y


        if fig_num is None:
            fig_num = 1000
        fig = pl.figure(fig_num)
        pl.clf()

        pl.imshow(occ, extent=extent, interpolation='bilinear', cmap='hot', clim=(0., 1.))
        pl.colorbar()
        self.plot_obstacles(fig_num)
        Q = pl.quiver(query_x, query_y, -grad_x, grad_y, pivot='mid', units='x', color='b')



        pl.show(block=False)

        if external_limit is None:
            self.xlim = extent[0], extent[1]  # min(self.grid_x), max(self.grid_x)
            self.ylim = extent[2], extent[3]  # max(self.grid_y), min(self.grid_y)
        else:
            self.xlim = external_limit[0], external_limit[1]  # min(self.grid_x), max(self.grid_x)
            self.ylim = external_limit[2], external_limit[3]  # max(self.grid_y), min(self.grid_y)
            pl.xlim(self.xlim)
            pl.ylim(self.ylim)

        return occ

    def plot_HS_grad_map(self, fig_num=None, res=0.2):

        dl = res
        query_x, query_y = np.meshgrid(np.arange(np.min(self.grid_x), np.max(self.grid_x), dl),
                                       np.arange(np.min(self.grid_y), np.max(self.grid_y), dl))

        extent = (np.min(query_x.ravel()), np.max(query_x.ravel()), np.max(query_y.ravel()), np.min(query_y.ravel()))

        grad = self.hilbert_map.classify_grad(np.vstack((query_x.ravel(), query_y.ravel())).T)

        grad_x = np.reshape(grad[0], (query_x.shape[0], query_y.shape[1]))
        grad_y = np.reshape(grad[1], (query_x.shape[0], query_y.shape[1]))

        if fig_num is None:
            fig_num = 1000

        fig = pl.figure(fig_num)
        pl.subplot(1, 2, 1)
        pl.imshow(grad_x, extent=extent, interpolation='bilinear', cmap='hot')
        pl.colorbar()
        self.plot_obstacles(fig_num)
        pl.subplot(1, 2, 2)
        pl.imshow(grad_y, extent=extent, interpolation='bilinear', cmap='hot')
        pl.colorbar()
        self.plot_obstacles(fig_num)
        pl.show(block=False)

    def stochastic_calc_obstacle_cost_functional_precompute(self, t):
        """
        compute the reduced cost functional - a single value for the entire path
        using a forward kynematic map - which map from configuration map (this means that each configuration is expanded
        to include a set of body points which is translated into real world coordiantes (this will enable a finite size robot)
        That means for each time t_i, there will be B set of body points, resulting in a matrix which is CxB
        :return: support points and cost (cost caluclated by reduce function)
        """

        lam = 2.

        # t = np.array([0.38232230338,0.995])

        cost = self.obs_cost(t)

        support = []
        for t_i, U_i in zip(t, cost):
            # if (t_i not in self.feature.keys()):
            if ((U_i > 1e-6) and
                    (t_i not in self.feature.keys())):
                support.append(t_i)
        if self.debug:
            pl.figure(1)

            point_m, point_v = self.path.curve_at_t(self.t)
            pl.plot(point_m[:, 0], point_m[:, 1], 'k-')

            if support:
                support_points_m, support_points_v = self.path.curve_at_t(support)
                pl.scatter(support_points_m[:, 0], support_points_m[:, 1], marker='8', color='m')

            pl.show(block=False)

        return support, self.reduce_obs_cost(cost)

    def stochastic_hilbert_map_occupancy(self, t):
        """
         compute the reduced cost functional - a single value for the entire path
        using a forward kynematic map - which map from configuration map (this means that each configuration is expanded
        to include a set of body points which is translated into real world coordiantes (this will enable a finite size robot)
        That means for each time t_i, there will be B set of body points, resulting in a matrix which is CxB

        :param self:
        :param t: ndarray with time parameter - equivalent to position along curve
        :return: support points and cost (cost caluclated by reduce function)
        """

        cost = self.hilbert_map.classify(self.path.mean(t))[:, 1]

        support = []
        for t_i, U_i in zip(t, cost):
            # if (t_i not in self.feature.keys()):
            '''
            if ((U_i > 1e-6) and
                    (U_i <= self.occupancy_free_thr) and
                    (t_i not in self.feature.keys())):
            '''
            if ((U_i <= self.occupancy_free_thr) and
                    (t_i not in self.feature.keys())):  # 2nd term affects RKHS explicit and not stochastic planners
                support.append(t_i)

        if self.debug:
            pl.figure(1)

            point_m = self.path.mean(self.t)
            pl.plot(point_m[:, 0], point_m[:, 1], 'k-')

            if support:
                support_points_m = self.path.mean(support)
                pl.scatter(support_points_m[:, 0], support_points_m[:, 1], marker='8', color='m')

            pl.show(block=False)

        return support, cost


    def costmap_cost_at_t(self, t0):

        return self.costmap_cost_at(self.path.mean(t0))


    def costmap_gradient_at_t(self, t0):


        x = self.path.mean(t0)

        return np.squeeze(np.asarray(self.costmap_grad_at(x)))

    def costmap_cost_at(self,x):
        '''
        Return the costmap cost and gradeint ar specific location. In prective convert from cartesian to map grid
        :param x: in cartesian
        :return: cost and gradient of costmap
        '''

        cost = []

        for x0 in x:
            x_i = np.argmin(np.abs(self.grid_x - x0[0]))
            y_i = np.argmin(np.abs(self.grid_y - x0[1]))
            cost.append(self.costmap[y_i, x_i])

        return cost

    def costmap_grad_at(self, x):
        '''
        Return the costmap cost and gradeint ar specific location. In prective convert from cartesian to map grid
        :param x: in cartesian
        :return: cost and gradient of costmap
        '''

        grad = []

        for x0 in x:
            x_i = np.argmin(np.abs(self.grid_x - x0[0]))
            y_i = np.argmin(np.abs(self.grid_y - x0[1]))
            dcost_x = self.costmap_dx[y_i, x_i]  # order matters
            dcost_y = self.costmap_dy[y_i, x_i]  # order matters
            grad.append(np.array([dcost_x, dcost_y]))

        return grad

    def control_cost_at_time(self, t0):
        #dt = 0.001
        #m0 = self.path.gradient(t0)
        #m_m1 = self.path.gradient(t0 - dt)
        #m_p1 = self.path.gradient(t0 + dt)
        #mumu =(m_p1+m_m1-2*m0)/(2*dt) - wrong result

        #return np.sqrt(np.sum((self.path.gradient(t0))**2))
        #return np.linalg.norm(self.path.gradient(t0), axis=1)
        return np.linalg.norm(self.path.gradient2(t0), axis=1)

    def control_cost_at_time_and_loc(self, x, t0):

        '''
        calculate the change in acceleration that will follow by replacing the position at t0 (x0) with x
        :param x:  this is the evaluated position - replace the position of the curve in x0 (at time t0)
        :param t0: time in which the acceleration is tested (corresponds to position x0)
        :return:
        '''
        dt = 0.01

        m_m1 = self.path.mean(t0 - dt)
        m_p1 = self.path.mean(t0 + dt)

        return np.sqrt(np.sum((m_p1 + m_m1 - 2 * x) **2))

    def control_cost_grad_at_time(self, t0):

        #  return the spatial(!) gradient of the control cost (2nd order gradient) with regards to path parameter
        #x0 = self.path.mean(t0)
        #return self.control_cost_grad_at_time_and_loc(x0, t0)
        return -np.squeeze(self.path.gradient2(t0))


    def control_cost_grad_at_time_and_loc(self, x0, t0):

        delta = 0.01
        base_action_cost = self.control_cost(t0)

        x=x0.copy()
        x[0, 0] += delta
        x_control_action1 = self.control_cost_at_time_and_loc(x, t0)
        x[0, 0] -= 2*delta
        x_control_action2 = self.control_cost_at_time_and_loc(x, t0)

        x = x0.copy()
        x[0, 1] += delta
        y_control_action1 = self.control_cost_at_time_and_loc(x, t0)
        x[0, 1] -= 2*delta
        y_control_action2 = self.control_cost_at_time_and_loc(x, t0)

        dSx = (x_control_action1 - x_control_action2) / (2*delta)
        dSy = (y_control_action1 - y_control_action2) / (2*delta)
        return np.array([dSx, dSy])

    def reduced_hilbert_map_cost_at(self, t0, d_points=[], f_reduce=np.max):
        '''
        The idea is to return a reduced obstacle cost for several points around the robot
        THis will allow to incorporate uncertainty in pose or robot size
        :param t0: time [0,1]
        :param d_points: change in distance from the point of interest - dim should match path
        :param f_reduce: this is the reduce function - max, mean etc.
        :return:
        '''

        x = self.path.mean(t0)
        p = [self.hilbert_map.classify(x)[:, 1]]
        for dp in d_points:
            p.append(self.hilbert_map.classify(x+dp)[:, 1])

        return f_reduce(np.asarray(p))

    def reduced_hilbert_map_cost_gradient_at(self, t0, d_points=[], f_reduce=np.mean):
        '''
        The idea is to return a reduced obstacle cost gradient for several points around the robot
        THis will allow to incorporate uncertainty in pose or robot size
        :param t0: time [0,1]
        :param d_points: change in distance from the point of interest - dim should match path
        :param f_reduce: this is the reduce function - max, mean etc.
        :return:
        '''

        x = self.path.mean(t0)
        g = [np.squeeze(np.asarray(self.hilbert_map.classify_grad(x)))]
        for dp in d_points:
            g.append(np.squeeze(np.asarray(self.hilbert_map.classify_grad(x+dp))))

        return f_reduce(np.asarray(g), axis=0)

    def hilbert_map_cost_at(self, t0):

        return self.hilbert_map.classify(self.path.mean(t0))[:, 1]

    def hilbert_map_cost_at_x(self, x):

        return self.hilbert_map.classify(x)[:, 1]

    def hilbert_map_gradient_at(self, t0):

        x = self.path.mean(t0)

        return np.squeeze(np.asarray(self.hilbert_map.classify_grad(x)))

    def obstacle_objective_gradient_at(self, t0):
        c = self.hilbert_map_cost_at(t0)
        del_c = self.hilbert_map_gradient_at(t0)

        vel = self.path.gradient(t0)
        norm_vel = vel/np.linalg.norm(vel)

        acc = self.path.gradient2(t0)

        curvature = np.squeeze((np.eye(self.D) - norm_vel.T.dot(norm_vel)).dot(acc.T)/(np.linalg.norm(vel)**2))

        grad = np.linalg.norm(vel)*((np.eye(self.D)- norm_vel.T.dot(norm_vel)).dot(del_c)-c*curvature)

        return grad


    def hilbert_map_gradient_at_x(self, x):

        return np.squeeze(np.asarray(self.hilbert_map.classify_grad(x)))

    def hilbert_map_entropy_at_x(self, x):

        return np.squeeze(np.asarray(self.hilbert_map.entropy(x)))

    def hilbert_map_entropy_grad_at_x(self, x):

        return np.squeeze(np.asarray(self.hilbert_map.entropy_grad(x)))

    def hilbert_map_MI_grad_at_x(self, x):

        return np.squeeze(np.asarray(self.hilbert_map.mutual_information_grad(x)))

    def check_waypoints_safe(self, x, yaw=None, resize = 1.5):
        '''
        Check for existance of obstacles around the way point
        :param x: waypoint x
        :param yaw: yaw ay waypoiny
        :param resize: changes effective robot size for safety checks
        :return:
        '''
        x_vec=np.atleast_2d(x)
        if self.robot_size is not None and yaw is not None:
            for x_i,yaw_i in zip(x_vec,yaw):
                d_points = x_i + np.asarray(robot_bounding_box(yaw_i, self.robot_size, resize))
                ''''
                lookahead = x_i + 1. * np.array([np.cos(yaw_i),
                                                 np.sin(yaw_i)
                                                 ]
                                                )

                x = np.concatenate((x,d_points,np.atleast_2d(lookahead)))
                '''
                x = np.concatenate((np.atleast_2d(x), d_points))

        if all(self.map_bounds[0] <= x[:, 0]) and all(x[:, 0] <= self.map_bounds[2]) and \
                all(self.map_bounds[1] <= x[:, 1]) and all(x[:, 1] <= self.map_bounds[3]) and \
                all(self.occupancy(x) <= self.occupancy_occupied_thr):  # allows path to go through unobserved space
            return True
        else:
            return False

    def check_end_point_safe(self, path=None, resize=1.5):
        '''

        :param path: path that needs to be checked. If None uses planner.path
        :param resize: changes effective robot size for safety checks
        :return:
        '''
        if path is None:
            path=self.path
        end_pose = path.mean([1.])
        direction = path.direction([1.])

        lookahead = np.squeeze(end_pose) + self.safety_buffer * np.squeeze(np.array([np.cos(direction),
                                      np.sin(direction)]))

        return self.check_waypoints_safe(lookahead, direction, resize)

    def plot_matlab_obstacles(self, fig_num=None, color='b'):

        if fig_num is None:
            fig_num=777

        pl.figure(fig_num)
        for obs in self.obstacles:
            for line in obs.vertices:
                pl.plot([line[0, 0], line[1, 0]], [line[0, 1], line[1, 1]], color=color, linewidth=2.0)
        pl.show(block=False)

