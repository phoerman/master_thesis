import numpy as np
import math

import rospy
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Pose, Point


def create_rviz_point_msg(point, frame_id="map", color='r'):
    """

    :param path_wps: waypoint of the path
    :param color: a color for the path marker
    :return:
    """

    msg = Marker()
    msg.header.frame_id = frame_id
    msg.header.stamp = rospy.Time()

    msg.type = 1  # Cube
    msg.action = 0  # ADD

    msg.pose = Pose()
    msg.pose.position.x = point[0]
    msg.pose.position.y = point[1]
    msg.pose.position.z = 0.


    msg.scale.x = 0.1
    msg.scale.y = 0.1
    msg.scale.z = 0.1

    if color == 'b':
        msg.color.a = 1.0
        msg.color.r = 1.
        msg.color.g = 0.
        msg.color.b = 0.
    elif color == 'g':
        msg.color.a = 1.0
        msg.color.r = 0.
        msg.color.g = 1.
        msg.color.b = 0.
    else:
        msg.color.a = 1.0
        msg.color.r = 1.
        msg.color.g = 0.
        msg.color.b = 0.

    return msg

def create_rviz_path_msg(path_wps, frame_id="map", color='r'):
    """

    :param path_wps: waypoint of the path
    :param color: a color for the path marker
    :return:
    """

    msg = Marker()
    msg.header.frame_id = frame_id
    msg.header.stamp = rospy.Time()

    msg.type = 4  # LINE_STRIP
    msg.action = 0  # ADD

    msg.pose = Pose()
    msg.points = []

    for wp in path_wps:
        point_msg = Point()
        point_msg.x = wp[0]
        point_msg.y = wp[1]
        point_msg.z = 0.

        msg.points.append(point_msg)

    msg.scale.x = 0.01
    msg.scale.y = 0.01
    msg.scale.z = 0.01

    if color == 'b':
        msg.color.a = 1.0
        msg.color.r = 1.
        msg.color.g = 0.
        msg.color.b = 0.
    elif color == 'g':
        msg.color.a = 1.0
        msg.color.r = 0.
        msg.color.g = 1.
        msg.color.b = 0.
    else:
        msg.color.a = 1.0
        msg.color.r = 1.
        msg.color.g = 0.
        msg.color.b = 0.

    return msg