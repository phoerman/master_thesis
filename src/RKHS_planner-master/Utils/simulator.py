#!/usr/bin/python
# coding: latin-1
import math
import copy
import numpy as np
import matplotlib.pyplot as pl
from Path import AbstractMeanFunction
from Utils.Utils import robot_bounding_box, calc_MI, line, line_intersection, in_line_segment


class ImplementationMissingError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class sim_results(object):

    def __init__(self):
        self.pose = []
        self.MIpose = []
        self.replan_needed =[]
        self.MI_results = []
        self.occ_on_path = []
        self.paths = []
        #self.waypoints = []
        self.plan_time = []

        #ref
        self.init_occ = None

    def save_results(self, filename):
        np.savetxt(filename + '_robot_pose.csv', np.asarray(self.pose), delimiter=",")
        np.savetxt(filename + '_robot_pose_MI.csv', np.asarray(self.MIpose), delimiter=",")
        np.savetxt(filename + '_replan_needed.csv', np.asarray(self.replan_needed), delimiter=",")
        np.savetxt(filename + '_MI_results.csv', np.asarray(self.MI_results), delimiter=",")
        np.savetxt(filename + '_occ_on_path_results.csv', np.asarray(self.occ_on_path), delimiter=",")
        #np.savetxt(filename + '_waypoints.csv', np.squeeze(np.atleast_2d(self.waypoints)), delimiter=",")
        np.savetxt(filename + '_plan_time_results.csv', np.asarray(self.plan_time), delimiter=",")
        '''
        if self.paths is not None:
            with open(filename + '_paths.txt', 'w') as text_file:
                for path in self.paths:
                    arraystr = ''
                    for spline in path:
                        arraystr += np.array_str(spline)

                    text_file.write(arraystr)
        '''



class simulator(object):

    def __init__(self):
        self.results = sim_results()
        self._ground_truth_map = []
        self._overall_free = None

        self._debug = False
        self._extent = None

        self._obstacles = [] # assumes circular obstacles
        self._obs_type = None
        self._obs_radius = 2.

        self._lrf_maxrange = 8.
        self._lrf_min_angle = -math.pi/2
        self._lrf_max_angle = math.pi/2
        self._num_beam = 1000

        self._lrf_angle_var = 0.
        self._lrf_range_var = 0.

        self._real_pos = None
        self._real_angle = None

        self.pos = None # offical value of pose - includes noise
        self.direction = None # offical value of direction - includes noise

        self.speed = None
        self._speed_var = 0.
        self._real_speed = None
        self._direction_var = 0.

    def init_simulator(self,pose, direction, obstacles, obs_type, obs_radius):
        '''

        :param pose:
        :param direction:
        :param obstacles:
        :param obs_radius: if None, obstacles are rectangles, o.w. this the radius of obstacles
        :return:
        '''
        self._real_pos = self.pos =  pose
        self._real_angle = self.direction = direction
        if obs_type is 'rect':
            self._obs_radius = None
            self._obs_type = 'rect'
        elif obs_type is 'circ':
            assert obs_radius is not None
            self._obs_radius = obs_radius
            self._obs_type = 'circ'
        elif obs_type is 'map':
            self._obs_radius = None
            self._obs_type = 'map'
        else:
            raise ValueError("init_simulator - Unidentified obstacle type")

        self._obstacles = obstacles


        if self._extent is None:
            min_x = max_x = self._real_pos[0]
            min_y = max_y = self._real_pos[1]

            if self._obs_type == 'circ':
                for obs in self._obstacles:
                    min_x = min(min_x, obs[0] - 3*self._obs_radius)
                    min_y = min(min_y, obs[1] - 3*self._obs_radius)
                    max_x = max(max_x, obs[0] + 3*self._obs_radius)
                    max_y = max(max_y, obs[1] + 3*self._obs_radius)
            elif self._obs_type == 'rect':
                for obs in self._obstacles:
                    min_x = min(min_x, np.min(np.ravel(obs.vertices[:, 0, 0])) - 1.5*self._lrf_maxrange)
                    min_y = min(min_y, np.min(np.ravel(obs.vertices[:, 0, 1])) - 1.5*self._lrf_maxrange)
                    max_x = max(max_x, np.max(np.ravel(obs.vertices[:, 0, 0])) + 1.5*self._lrf_maxrange)
                    max_y = max(max_y, np.max(np.ravel(obs.vertices[:, 0, 1])) + 1.5*self._lrf_maxrange)
            elif self._obs_type == 'map':
                min_x = min(min_x, min(self._obstacles.x_limits) - 1.5 * self._lrf_maxrange)
                min_y = min(min_y, min(self._obstacles.y_limits) - 1.5 * self._lrf_maxrange)
                max_x = max(max_x, max(self._obstacles.x_limits) + 1.5 * self._lrf_maxrange)
                max_y = max(max_y, max(self._obstacles.y_limits) + 1.5 * self._lrf_maxrange)

            self._extent = (float(min_x), float(max_x), float(max_y), float(min_y)) # order of y is correct

    def __repr__(self):
        str2plot = 'LRF simulator\n'
        str2plot += 'real_pose = {0}, real_pose = {1}, \n' \
                    'noisy_pose ={2}, noisy_theta = {2}'.format(self.pos,
                                                                self.direction,
                                                                self._real_pos,
                                                                self._real_angle
                                                                )
        str2plot += 'speed = {0}, real_speed = {1}, \n'.format(self.speed,
                                                                self._real_speed
                                                               )
        str2plot += 'Var: speed = {0}, direction = {1}, \n'.format(self._speed_var,
                                                               self._direction_var
                                                               )

        str2plot += 'LRF: max range = {0}, ' \
                    'min,max angle = ' \
                    '{1},{2}, #beams={3}\n'.format(self._lrf_maxrange,
                                                   self._lrf_min_angle,
                                                   self._lrf_max_angle,
                                                   self._num_beam
                                                   )
        str2plot += 'LRF Var: angle = {0}, range = {1}, \n'.format(self._lrf_angle_var,
                                                                   self._lrf_range_var
                                                                   )

        str2plot += 'Obstacle   |   location\n'
        for key, obs_pose in enumerate(self._obstacles):
            str2plot += "{0}           {1}\n".format(key,obs_pose)

        return str2plot

    def _plot_obstacles(self, fig_num=None):

        fig = pl.figure(fig_num)

        index = 1
        if self._obs_type == 'circ':
            for obs in self._obstacles:
                circ = pl.Circle((obs[0], obs[1]), radius=self._obs_radius, edgecolor='k', linestyle='dashed', fill=False)
                circ.set_label(str(index))

                fig.gca().add_artist(circ)

                pl.text(obs[0], obs[1], index, horizontalalignment='center',
                        verticalalignment='center')
                index += 1
        elif self._obs_type == 'rect':
            pl.figure(fig_num)
            for obs in self._obstacles:
                for line in obs.vertices:
                    pl.plot([line[0, 0], line[1, 0]], [line[0, 1], line[1, 1]], 'k-', linewidth=2.0)
        elif self._obs_type == 'map':
            pl.figure(fig_num)
            pl.imshow(self._obstacles.occupancy,
                      cmap='Greys',
                      interpolation='nearest',
                      alpha=0.7,
                      extent=(self._obstacles.x_limits[0],
                              self._obstacles.x_limits[1],
                              self._obstacles.y_limits[1],
                              self._obstacles.y_limits[0]
                              )
                      )


    def _debug_plot(self, hits=None, fig_num=None):

        if fig_num is None:
            fig_num = 4545

        fig = pl.figure(fig_num)
        pl.clf()

        self._plot_obstacles(fig_num)

        if hits is not None:
            dtheta = (self._lrf_max_angle - self._lrf_min_angle)/(len(hits)-1)

            for i,hit in enumerate(hits):

                real_angle = (self._real_angle + i*dtheta + self._lrf_min_angle + np.pi) % (2 * np.pi) - np.pi  # normalised angle [-pi:pi]
                hit_pose = self._real_pos + hit * np.array([np.cos(real_angle),
                                                            np.sin(real_angle)
                                                           ]
                                                          )

                x = np.array([self._real_pos[0], hit_pose[0]])
                y = np.array([self._real_pos[1], hit_pose[1]])

                pl.plot(x,y,'b-')
                if np.linalg.norm(hit_pose-self._real_pos) < self._lrf_maxrange-0.1:
                    pl.scatter(hit_pose[0], hit_pose[1], color='r' ,marker='*')

        pl.xlim((self._extent[0], self._extent[1]))
        pl.ylim((self._extent[3], self._extent[2]))

        circ = pl.Circle((self._real_pos[0], self._real_pos[1]), radius=0.1, edgecolor='g', facecolor='g',
                         linestyle='solid', fill=True)
        pl.gca().add_artist(circ)
        pl.show(block=False)

    def _generate_query(self,planner):

        dl = 0.2
        query_x, query_y = np.meshgrid(np.arange(np.min(planner.grid_x), np.max(planner.grid_x), dl),
                                       np.arange(np.min(planner.grid_y), np.max(planner.grid_y), dl))
        return np.vstack((query_x.ravel(), query_y.ravel())).T

    def check_coverage(self, planner):
        assert self._ground_truth_map is not None
        subdivide = 1000
        query_all = self._generate_query(planner)
        occ = np.array([])
        for query in np.array_split(query_all, int(query_all.shape[0] / subdivide) + 1, axis=0):
            occ_q = planner.hilbert_map.classify(query)[:, 1]

            occ = np.hstack([occ, occ_q]) if occ.size else occ_q

        return float(np.sum(occ <= planner.occupancy_free_thr))/self._overall_free


    def generate_ground_truth(self, planner):
        '''
        Generates ground truth coverage
        :param planner:
        :return:
        '''
        query_all = self._generate_query(planner)

        if self._obs_type =='circ' or self._obs_type =='rect':
            truth_value = self.__ground_truth_circ_rect_obstacles(query_all)
        elif self._obs_type == 'map':
            truth_value = self.__ground_truth_from_map(query_all)

        self._ground_truth_map = truth_value
        self._overall_free = np.sum(self._ground_truth_map<0.5)

    def __ground_truth_circ_rect_obstacles(self,query):

        truth_value = np.zeros((query.shape[0],))
        for i, q in enumerate(query):
            for j, obs in enumerate(self._obstacles):
                if self._obs_type == 'circ' and np.linalg.norm(q - np.squeeze(obs)) <= self._obs_radius:
                    truth_value[i] = 1.
                    continue
                elif self._obs_type == 'rect' and j > 0:
                    # check if point M is inside rectangle with vertices A,B,C,D
                    # if (0 < AM⋅AB < AB⋅AB)∧(0 < AM⋅AD < AD⋅AD) then point is inside

                    # vertices strcutre of each obstacle => shape (4,2,2)  - 4 vertices and start point and end point for each line
                    AB = obs.vertices[0, 1, :] - obs.vertices[0, 0, :]
                    AD = obs.vertices[-1, 0, :] - obs.vertices[-1, 1, :]
                    AM = q - obs.vertices[0, 0, :]
                    if (0. <= AM.dot(AB) < AB.dot(AB)) and (0 <= AM.dot(AD) < AD.dot(AD)):
                        truth_value[i] = 1.
                        continue
        return truth_value

    def __ground_truth_from_map(self, query):

        #needed in case a change in resolution or size
        truth_value = np.zeros((query.shape[0],))

        miny = min(self._extent[2], self._extent[3])
        maxy = max(self._extent[2], self._extent[3])
        minx = min(self._extent[0], self._extent[1])
        maxx = max(self._extent[0], self._extent[1])

        for i, q in enumerate(query):
            if not (miny <= q[1] <= maxy) or \
                    not (minx <= q[0] <= maxx):
                truth_value[i] = 1. # outside map
                continue

            map_indx = self._obstacles.to_grid(q)
            occ = self._obstacles.occupancy[map_indx[1], map_indx[0]]
            if occ >= 0.5:
                truth_value[i] = 1.
                continue

        return truth_value

    def read_LRF(self):

        hits = []
        robot_pose = np.ndarray.tolist(np.append(self._real_pos,self._real_angle))

        for angle in np.linspace(start = self._lrf_min_angle,
                                 stop = self._lrf_max_angle,
                                 num = self._num_beam,
                                 endpoint=True
                                 ):
            if self._obs_type is 'rect' or self._obs_type is 'circ':
                hit, pose = self._hit(angle)
            elif self._obs_type is 'map':
                hit, pose = self._hit_ogm(angle)
            else:
                raise ValueError("read_LRF - incorrect obstacle type")

            hits.append(hit)

        if self._debug:
            self._debug_plot(hits)

        return hits, robot_pose

    def _hit(self,beam_angle):
        """
        beam = line from [Ax,Ay] to [Bx,By]=>
        line = A + t(B-A) for t in [0,1]

        Assuming circular obstacles with radius R=self._obs_radius,
        the equation that needs to be solved is:
        (Ax+t(Bx-Ax))^2+(Ay+t(By-Ay))^2 = R^2 ===>

        (A2x+A2y−R2)+2(Ax(Bx−Ax)+Ay(By−Ay))t+((Bx−Ax)2+(By−Ay)2)t2=0

        if discriminante is >=0 there is a solution, o.w. there is no intersect

        To ease computationv, check if within a bounding box


        :param beam_angle:
        :return: range to hit (required by map) + position of hit (for debug purposes only)
        """

        if self._obs_type == 'circ':
            bbox_max = self._real_pos + self._lrf_maxrange + self._obs_radius
            bbox_min = self._real_pos - self._lrf_maxrange - self._obs_radius

        real_angle = (self._real_angle + beam_angle + np.pi) % (2 * np.pi ) - np.pi # normalised angle [-pi:pi]
        A = self._real_pos.copy()
        B = self._real_pos + np.squeeze(self._lrf_maxrange*np.array([np.cos(real_angle),
                                                          np.sin(real_angle)
                                                          ]
                                                         ))
        l_beam = line(np.squeeze(A),np.squeeze(B))

        hit = 1.
        hit_pose = B
        for obs in self._obstacles:
            if self._obs_type=='circ':
                obs = np.squeeze(obs)
                if np.any(obs < bbox_min) or np.any(obs > bbox_max): # check if close enough
                    continue

                _A = A - obs
                _B = B - obs

                c = _A[0]*_A[0] + _A[1]*_A[1] - self._obs_radius*self._obs_radius
                b = 2*(_A[0]*(_B[0]-_A[0]) + _A[1]*(_B[1]-_A[1]))
                a = ((_B[0]-_A[0])**2 + (_B[1]-_A[1])**2)

                disc = b**2 - 4*a*c
                if disc < 0.:
                    continue # no change to hit

                disc = np.sqrt(disc)

                t1 = (-b + disc) / (2 * a)
                t2 = (-b - disc) / (2 * a)

                if 0 < t1 < 1 and t1 < hit:
                    hit = t1
                    hit_pose = A + hit*(B-A)

                if 0 < t2 < 1 and t2 < hit:
                    hit = t2
                    hit_pose = A + hit * (B - A)

            elif self._obs_type=='rect':
                for edge in obs.vertices:
                    intersect = line_intersection(l_beam, line(edge[0,:],edge[1,:]))
                    if intersect:
                        t1 = min((intersect[0]- A[0])/(B[0] - A[0]),(intersect[1]- A[1])/(B[1] - A[1])) #should be the same - only erelrevant when NAN (divide by 0)
                        if 0 < t1 < 1 and \
                            in_line_segment(edge[0,:], edge[1,:], np.asarray(intersect))  and \
                            t1 < hit: #inside the two line segment and smaller han previous result
                            hit = t1
                            hit_pose = A + hit * (B - A)


        # A is inside the map byy definition
        if not hit < 1.: #no hit - check that inside map
            beyond_map = False
            if B[1] <= self._extent[3]: #min_y
                t=(self._extent[3]-A[1])/(B[1]-A[1])
                beyond_map = True
            if B[1] >= self._extent[2]:  # max_y
                t = (self._extent[2] - A[1]) / (B[1] - A[1])
                beyond_map = True
            if B[0] <= self._extent[0]:  # min_x
                t = (self._extent[0] - A[0]) / (B[0] - A[0])
                beyond_map = True
            if B[0] >= self._extent[1]:  # max_x
                t = (self._extent[1] - A[0]) / (B[0] - A[0])
                beyond_map = True

            if beyond_map:
                hit = t
                hit_pose = A + hit * (B - A)

        return hit*self._lrf_maxrange, hit_pose

    def _hit_ogm(self, beam_angle):
        """
        beam = line from [Ax,Ay] to [Bx,By]=>
        line = A + t(B-A) for t in [0,1]

        check hit with occupancy map

        :param beam_angle:
        :return: range to hit (required by map) + position of hit (for debug purposes only)
        """
        miny = min(self._extent[2], self._extent[3])
        maxy = max(self._extent[2], self._extent[3])
        minx = min(self._extent[0], self._extent[1])
        maxx = max(self._extent[0], self._extent[1])

        real_angle = (self._real_angle + beam_angle + np.pi) % (2 * np.pi) - np.pi  # normalised angle [-pi:pi]

        theta = np.stack((np.cos(real_angle), np.sin(real_angle)), axis=0)

        t_vec = np.linspace(start=0., stop=1., endpoint=True, num=100).reshape(-1, 1)

        points = self._real_pos + self._lrf_maxrange * t_vec * theta

        hit = 1.
        hit_pose = points[-1,:]
        for i, point in enumerate(points):

            if not (miny <= point[1] <= maxy) or \
               not (minx <= point[0] <= maxx):
                hit = t_vec[i]
                hit_pose = point
                break

            map_indx = self._obstacles.to_grid(point)
            occ = self._obstacles.occupancy[map_indx[1], map_indx[0]]
            if occ >= 0.5:
                hit = t_vec[i]
                hit_pose = point
                break

        return hit * self._lrf_maxrange, hit_pose

    def follow_path(self, path, start_t=0., end_t=1.):

        assert 0 <= start_t <= 1.
        assert 0 < end_t <= 1. and start_t < end_t

        res = 0.1
        _delta_t = max(end_t-start_t, res)
        num = int(_delta_t/res)+1
        t_list = np.linspace(start=start_t,
                        stop=end_t,
                        num=num,
                        endpoint=True
                        )
        path_hits=[]
        path_poses = []
        #note: t is not time but parameter of the curve
        for t in t_list:
            #pose
            x = np.squeeze(path.mean(t))
            dx = np.squeeze(path.gradient(t))

            self._real_pos = x
            self._real_angle = math.atan2(dx[1],dx[0])

            self.pos = self._real_pos
            self.direction = self._real_angle

            #LRF
            hits, robot_pose = self.read_LRF()
            path_poses.append(robot_pose)
            path_hits.append(hits)
            if self._debug:
                path.plot_curve(fig_num=4545)

        return path_hits, path_poses

    def reverse_on_path(self, way_point_path, final_pose, planner=None):
        assert planner is not None

        path_poses = []
        path_hits = []

        for x in np.flipud(way_point_path):

            path_poses.append(np.squeeze(x))
            path_hits.append([])

            self.results.pose.append(np.squeeze(x))
            #self.results.MI_results.append(self.results.MI_results[-1])
            self.results.occ_on_path.append(planner.occupancy(np.atleast_2d(x)))

        self.results.MIpose.append(x)
        self.results.pose.append(x)
        self.results.MI_results.append(self.results.MI_results[-1])
        self.results.occ_on_path.append(planner.occupancy(np.atleast_2d(x)))

        return path_hits, path_poses

    def follow_path_check_collision(self, path, start_t=0., end_t=1., planner=None, safe_end_cnt=2, safe_on_path=True):
        #planner is required in order to update and query map
        assert planner is not None

        assert 0 <= start_t <= 1.
        assert 0 < end_t <= 1. and start_t < end_t

        res = 0.01
        _delta_t = max(end_t - start_t, res)
        num = int(_delta_t / res) + 1
        t_list = np.linspace(start=res,
                             stop=end_t,
                             num=num,
                             endpoint=True
                             )
        path_hits = []
        path_poses = []
        # note: t is not time but parameter of the curve
        safe_t = start_t
        last_LRF_pose = -np.inf
        last_LRF_direction = -np.inf
        temp_safe_end_cnt =  safe_end_cnt

        map_update_indx = 0

        for k, t in enumerate(t_list):
            # pose
            x = np.squeeze(path.mean(t))
            dx = np.squeeze(path.gradient(t))

            self._real_pos = x
            self._real_angle = math.atan2(dx[1], dx[0])

            self.pos = self._real_pos
            self.direction = self._real_angle

            #check if to update

            if (np.linalg.norm(last_LRF_pose - x) > 0.25) or \
                    (abs(last_LRF_direction-self._real_angle) > 10*math.pi/180) or\
                t is t_list[-1]:
                last_LRF_pose = x
                last_LRF_direction = self._real_angle
                # LRF
                hits, robot_pose = self.read_LRF()
                path_poses.append(robot_pose)
                path_hits.append(hits)
                #planner.lrf_pose.append(robot_pose)
                #planner.lrf_hits.append(hits)
                if self._debug:
                    path.plot_curve(fig_num=4545)

                planner.map_update(robot_pose,
                                   hits,
                                   1,
                                   planner.lrf_maxrange,
                                   fix_incermental_weights=True)
                #occ = planner.map_plot(fig_num=123)

                self.results.pose.append(x)
                #self.results.MI_results.append(calc_MI(occ, self.results.init_occ))
                if planner.robot_size is not None and last_LRF_direction is not None:
                   d_points = x + np.asarray(robot_bounding_box(last_LRF_direction, planner.robot_size))
                   d_points = np.concatenate((np.atleast_2d(x), d_points))
                   self.results.occ_on_path.append(np.max(planner.occupancy(d_points)))
                else:
                    self.results.occ_on_path.append(planner.occupancy(np.atleast_2d(x)))
                '''
                if k-map_update_indx > 10:
                    all_lrf = copy.deepcopy(planner.hilbert_map.lrf_obserations)
                    planner.hilbert_map = []
                    planner.map_init(planner.map_bounds,
                                     planner.map_n_centers,
                                     alpha=0.0027,
                                     l1_ratio=0.11,
                                     gamma=.5,
                                     loss='log',
                                     penalty='l2'
                                     )
                    planner.map_init(planner.map_bounds)
                    planner.hilbert_map.update_all(all_lrf)
                    map_update_indx=k
                '''

                # check if rest of path is safe - otherwise re-plan
                safe_end = planner.check_end_point_safe(resize=1.5)
                if not safe_end:
                    temp_safe_end_cnt -= 1
                else:
                    temp_safe_end_cnt = safe_end_cnt

                if safe_on_path:
                    safe = planner.check_waypoints_safe(np.squeeze(path.mean(t_list[min(num - 1, k + 5):])),
                                                        path.direction(t_list[min(num - 1, k + 5):]),
                                                        resize=1.
                                                        )



            safe_t = t
            if not safe or temp_safe_end_cnt < 0:
                self.results.replan_needed.append(x)
                break

        #last update of map
        hits, robot_pose = self.read_LRF()
        planner.map_update(robot_pose,
                            hits,
                            1,
                            planner.lrf_maxrange,
                            fix_incermental_weights=True)
        #self.results.waypoints.append(path_poses)

        #planner.map_update(planner.lrf_pose, planner.lrf_hits, len(planner.lrf_pose), planner.lrf_maxrange)
        '''
        all_lrf = copy.deepcopy(planner.hilbert_map.lrf_obserations)
        Ncenters = int(math.sqrt(len(planner.hilbert_map.centers)))
        planner.hilbert_map = []
        planner.map_init(planner.map_bounds,
                         planner.map_n_centers,
                         alpha=0.0027,
                         l1_ratio=0.11,
                         gamma=.5,
                         loss='log',
                         penalty='l2'
                         )
        planner.hilbert_map.update_all(all_lrf)
        '''
        self.results.MIpose.append(self.pos)
        self.results.pose.append(self.pos)
        occ = planner.map_plot(fig_num=123)
        self.results.MI_results.append(calc_MI(occ, self.results.init_occ))
        if planner.robot_size is not None and last_LRF_direction is not None:
            d_points = self.pos + np.asarray(robot_bounding_box(self.direction, planner.robot_size))
            d_points = np.concatenate((np.atleast_2d(x), d_points))
            self.results.occ_on_path.append(np.max(planner.occupancy(d_points)))
        else:
            self.results.occ_on_path.append(planner.occupancy(np.atleast_2d(x)))

        return path_hits, path_poses, safe_t


def simulator_exmaple():
    sim = simulator()
    sim._debug = True

    obstacles = [np.array([[7.76358369], [16.14108677]]),
                 np.array([[3.456], [7.817]]),
                 np.array([[8.803], [3.2478]]),
                 np.array([[9.9481], [9.6421]]),
                 np.array([[2.4694], [14.0296]]),
                 np.array([[14.5986], [0.6154]]),
                 np.array([[14.2899], [7.511]]),
                 ]

    #path
    start_point = np.array([1., 2.])
    end_point = np.array([15., 12.])
    gamma = 10.
    D = 2  # dimensionality of output problem, dimensionality of input is assumed 1D
    n_components = 10

    from Path.LinearMeanFunction import LinearMeanFunction
    from Path.LinReg import LinReg_path
    path = LinReg_path()
    path.path_mean = LinearMeanFunction(x0=start_point,
                                        x1=end_point
                                        )
    boundary_conditions = dict()
    boundary_conditions[0.] = start_point
    boundary_conditions[1.] = end_point

    path.initialize_features(method="RFF",
                             n_components=n_components,
                             gamma=gamma,
                             D=D
                             )

    path.weights = 5 * np.random.rand(n_components, D)  # Generating a random curve for debugging
    #  Prepare boundary condition for a simple online update
    if boundary_conditions:
        path.initialize_boundary_features(np.fromiter(iter(boundary_conditions.keys()), dtype=float),
                                          gamma=gamma,
                                          D=D
                                          )

    #pose = np.array([1., 2.])
    #direction = 0.
    obs_radius = 2.
    pose = np.squeeze(path.mean(0))
    path_grad = np.squeeze(path.gradient(0.))
    direction = math.atan2(path_grad[1], path_grad[0])
    sim.init_simulator(pose, direction, obstacles, obs_radius)
    sim._debug_plot()

    hits, pose = sim.read_LRF()

    # compute Hilbert map
    import hilbert_map as hm
    import util

    map_min = np.array([sim._extent[0], sim._extent[3]])
    map_max = np.array([sim._extent[1], sim._extent[2]])

    Ncntrs = 100
    map_centers_x, map_centers_y = np.meshgrid(np.linspace(map_min[0], map_max[0], Ncntrs, endpoint=True),
                                               np.linspace(map_min[1], map_max[1], Ncntrs, endpoint=True))

    alpha = 0.0027
    l1_ratio = 0.11
    gamma = .5

    hilbert_map = hm.SparseHilbertMap(np.vstack((map_centers_x.ravel(), map_centers_y.ravel())).T,
                                      gamma=gamma
                                      )

    hilbert_map.set(alpha=alpha, l1_ratio=l1_ratio, gamma=gamma)

    data, label = util.genarate_dataset_for_single_scan(pose, hits, max_lrf_dist=sim._lrf_maxrange)
    hilbert_map.add(data, label)
    # plot map
    dl = 0.2

    query_x, query_y = np.meshgrid(np.arange(sim._extent[0], sim._extent[1], dl),
                                   np.arange(sim._extent[3], sim._extent[2], dl)
                                   )

    occ = hilbert_map.classify(np.vstack((query_x.ravel(), query_y.ravel())).T)

    occ = np.reshape(occ[:, 1], (query_x.shape[0], query_y.shape[1]))

    fig = pl.figure(555)

    pl.imshow(occ, extent=sim._extent, interpolation='bilinear', cmap='hot', clim=(0., 1.))
    pl.colorbar()
    sim._plot_obstacles(fig_num=fig.number)
    pl.show(block=False)



    path.plot_curve(fig_num=4545)
    sim.follow_path(path, 0., 1.)

if __name__ == "__main__":

    import sys

    simulator_exmaple()

    sys.exit()
