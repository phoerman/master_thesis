import numpy as np


class ImplementationMissingError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class AbstractLearningRateMeanFunction(object):

    def __init__(self):
        pass

    def __str__(self, *args, **kwargs):
        raise ImplementationMissingError("__str__ function not implemented")

    def __call__(self, *args, **kwargs):
        raise ImplementationMissingError("call function not implemented")


class BottouOptimal(AbstractLearningRateMeanFunction):

    def __init__(self, eta0, t0):
        super(BottouOptimal, self).__init__()

        assert t0 > 0, "BottouOptimal - t0 must be larger than 0"
        self.eta0 = eta0
        self.t0 = t0
        self.t = 0.

    def __str__(self):

        str2plot = "Learning rate: BottouOptimal, eta0={0}, t0={1}\n".format(self.eta0, self.t0)
        return str2plot

    def __call__(self, *args, **kwargs):

        eta = self.eta0 / (self.t0+self.t)
        self.t += 1.

        return eta


class Constant(AbstractLearningRateMeanFunction):

    def __init__(self, eta0 = 0.5):
        super(Constant, self).__init__()

        assert eta0 > 0, "Constant - eta0 must be larger than 0"
        self.eta0 = eta0

    def __str__(self):

        str2plot = "Learning rate: Constant, eta0={0}\n".format(self.eta0)
        return str2plot

    def __call__(self, *args, **kwargs):

        return  self.eta0


class InvScaling(AbstractLearningRateMeanFunction):
    def __init__(self, eta0=1., power_t=0.5):
        super(InvScaling, self).__init__()

        assert power_t > 0, "Power - power_t must be larger than 0"

        self.eta0 = eta0
        self.power_t = power_t
        self.t = 1.

    def __str__(self):

        str2plot = "Learning rate: InvScaling, eta0={0}, power_t={1}\n".format(self.eta0, self.power_t)
        return str2plot

    def __call__(self, *args, **kwargs):
        eta = self.eta0 / pow(self.t, self.power_t)
        self.t += 1.

        return eta


if __name__ == "__main__":

    L1 = BottouOptimal(eta0=1., t0=1.)
    L2 = Constant(0.5)
    L3 = InvScaling(eta0=1., power_t=0.5)

    l1 = []
    l2 = []
    l3 = []

    for i in range(1000):
        l1.append(L1())
        l2.append(L2())
        l3.append(L3())


    import matplotlib.pyplot as pl
    t = range(1000)
    pl.plot(t, l1, 'r')
    pl.plot(t, l2, 'b')
    pl.plot(t, l3, 'g')

