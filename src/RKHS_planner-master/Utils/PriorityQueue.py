import heapq
import itertools

REMOVED = '<entry - removed>'                # placeholder for a removed task


class PriorityQueue():
    """
    Builds a priority queue that can manage removal or update of entries in the heap
    Based on https://docs.python.org/2/library/heapq.html
    """
    def __init__(self):
        self.pq = []                         # list of entries arranged in a heap
        self.entry_finder = {}               # mapping of tasks to entries
        self.counter = itertools.count()     # unique sequence count

    def is_empty(self):
        return not(self.pq)

    def add_entry(self,entry, priority=0):
        'Add a new entry or update the priority of an existing task'
        if entry in self.entry_finder:
            self.remove_entry(entry)
        count = next(self.counter)
        new_entry = [priority, count, entry]
        self.entry_finder[entry] = new_entry
        heapq.heappush(self.pq, new_entry)

    def remove_entry(self, entry):
        'Mark an existing task as REMOVED.  Raise KeyError if not found.'
        removed_entry = self.entry_finder.pop(entry)
        removed_entry[-1] = REMOVED

    def pop_entry(self):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, entry = heapq.heappop(self.pq)
            if entry is not REMOVED:
                del self.entry_finder[entry]
                return entry
        raise KeyError('pop from an empty priority queue')

    def clear(self):
         del self.pq[:] #clear list
         self.entry_finder.clear()

