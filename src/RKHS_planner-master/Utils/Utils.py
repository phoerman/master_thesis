import numpy as np
import math


def distance_r(r1, r2):
    """
    This is norm calculation
    :param r1: matrix nxm ; n - dimension of vector, m - number of vectors
    :param r2: matrix nxm ; n - dimension of vector, m - number of vectors
    :return: matrix of distance between all vector of r1 and r2
    """
    R1Dim=r1.shape[0]
    R2Dim=r2.shape[0]

    if r1.ndim < 2: #r1 is a vector
        r1 = np.atleast_2d(r1).T
    R1_N=r1.shape[1]

    if r2.ndim < 2: #r2 is a vector
        r2 = np.atleast_2d(r2).T
    R2_N=r2.shape[1]



    if R1_N == 0 | R2_N == 0:
        return 0

    #check that dimension are right
    if R1Dim!=R2Dim:
        raise ValueError('distanceR: Dimension error between input vectors')


    XX1 = np.atleast_2d(np.sum(r1 * r1, axis = 0)) #%sum on X,Y,Z with the appropriate length scale
    XX2 = np.atleast_2d(np.sum(r2 * r2, axis = 0))
    X1X2 = r1.T.dot(r2)
    XX1T = XX1.T
    z = np.tile(XX1T,(1,R2_N)) + np.tile(XX2,(R1_N,1)) - 2*X1X2
    return np.sqrt(z)

def create_transform_matrix(tx,ty,yaw):
    """
    2D coordinate change - rotation + shift.
    Return matrix is multplied by coordinate in order to get converted coordinates
    :param tx: x shift in coordinates
    :param ty: y shift in coordinates
    :param yaw: rotation angle of coordinate axis
    :return: tranformation matrix
    """

    return np.array([[math.cos(yaw), -math.sin(yaw),tx],
                     [math.sin(yaw), math.cos(yaw),ty],
                     [0., 0., 1.]])

def RBF_kernel(x, y, lengthscale):
    """
       Calculate RBF kernel matrix
       Row dim of x and y (M) should be the same
       :param x: MxN array - N=number of entries with dimension M.
       :param y: MxK array - K=number of entries with dimension M
       :param lengthscale: can be isotrpic (dim=1) or M-dim vector
       :return: RBF kernel matrix
       """

    x = np.atleast_2d(x)
    y = np.atleast_2d(y)

    xDim = x.shape[0]
    yDim = y.shape[0]

    x_N = x.shape[1]
    y_N = y.shape[1]

    if xDim != yDim:
       raise ValueError("RBF_kernel: Incompatible dim for x and y")

    if x.size == 0 or \
       y.size == 0:
        return
    if ~isinstance(lengthscale, np.ndarray):
        lengthscale =  np.asarray(lengthscale).reshape(1, -1)[0,:]

    if (lengthscale.size != 1) and \
        (lengthscale.shape[0] != yDim):
        ValueError('Incorrect LengthScale input - Dimension mismatch')
    elif lengthscale.shape[0]==1:
        lengthscale= lengthscale*np.ones((1, yDim),dtype=np.float32)

    w=lengthscale.T**-2;
    XX1 = np.sum(np.tile(w,(1,x_N))*x*x,0)
    XX2 = np.sum(np.tile(w,(1,y_N))*y*y,0)

    X1X2 = ((np.tile(w,(1,x_N))*x).T).dot(y)


    z = np.tile(XX1[:,np.newaxis],(1,y_N)) + np.tile(XX2[np.newaxis,:],(x_N,1)) - 2*X1X2;

    return np.exp(-z/2)


def robot_bounding_box(yaw, size, resize=1.):
    '''
    return the bounding box of a robot given size
    :param yaw: yaw
    :param size: depend on size if len=1, circle, if len=2 then rect
    :param resize: change to size of robot - for safety checks
    :return: points n 2d relaive to pose
    '''

    d_points = []
    size = resize*np.asarray(size)
    if size.size == 1: # circle
        for a in np.linspace(start=0., stop=2*np.pi, num=8, endpoint=False):
            d_points.append(size*np.array([np.cos(yaw+a),np.sin(yaw+a)]))

    elif size.size == 2: #rect
        R = np.array([[np.cos(yaw),-np.sin(yaw)],[np.sin(yaw),np.cos(yaw)]])
        corners = [np.array([size[0]/2, size[1]/2]),
                   np.array([-size[0]/2, size[1]/2]),
                   np.array([-size[0]/2, -size[1]/2]),
                   np.array([size[0]/2, -size[1]/2])
                   ]
        for c in corners:
            d_points.append(R.dot(c))
    else:
        raise ValueError("robot_bounding_box: size {} is unsupported".format(size))

    return d_points


def calc_MI(occ1, occ2):

    e1 = -(1-occ1)*np.log2((1-occ1))-occ1* np.log2(occ1)
    e1[np.isnan(e1)] = 0

    e2 = -(1 - occ2) * np.log2((1 - occ2)) - occ2 * np.log2(occ2)
    e2[np.isnan(e2)] = 0

    return np.sum(e1 - e2)


def line(p1, p2):
    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0] * p2[1] - p2[0] * p1[1])
    return A, B, -C


def line_intersection(L1, L2):
    D = L1[0] * L2[1] - L1[1] * L2[0]

    D = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x, y
    else:
        return False


def in_line_segment(A,B,C):
    #C inside line defined by A and B
    return -np.finfo(np.float32).eps <  distance_r(A, C) + distance_r(C, B) - distance_r(A, B) < np.finfo(np.float32).eps

def path_length(waypoints):
    if waypoints.shape[0] > 1:
        return np.sum(np.linalg.norm(waypoints[1:,:]-waypoints[:-1,:], axis=1))
    else:
        return 0.


def normal_vect(v):
    assert isinstance(v, np.ndarray)
    assert np.size(v) == 2

    u = np.array([1., -v[0]/v[1]])

    return u/np.linalg.norm(u)

