import sys
import os
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.image as im


def import_from_png(file, threshold=0.5):
    '''
    Return and occupancy map from a png file
    :param file:
    :return:
    '''
    img = (pl.imread(file))

    img = np.average(img[:,:,:-1],axis=2) # rgb2grayscale
    map = np.zeros(img.shape, dtype=float)
    map[img >0.5] = 1.

    #pl.imshow(img, cmap='Greys')
    #pl.show(block=False)

    return map



if __name__ == "__main__":
    import_from_png('/home/giladfrancis/code/Python/Src/RKHS/intel_modified_BW_scaled.png' )

    sys.exit()
