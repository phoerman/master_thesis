#!/usr/bin/env python
from __future__ import division, print_function, unicode_literals

import sys
import os
import time
import warnings
import numpy as np

import rospy
from laser_geometry import LaserProjection
from tf import TransformListener
from tf import transformations
from tf import Exception as tf_exception
from sensor_msgs.msg import LaserScan
import sensor_msgs.point_cloud2 as pc2
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from visualization_msgs.msg import Marker
from std_msgs.msg import Bool, Header

import math
from ros_utils import create_rviz_path_msg, create_rviz_point_msg


class ROSMapGenerator:
    """
    Listen to sensor data and log so can be used for map training

    """


    def __init__(self):

        #subscribers:
        self.__init = True

        self._laser_sub = None  # laser scan subscriber
        self._save_data_sub = None # subscriber listening to save_data request from user


        #publishers
        self._collected_data_pub = None


        self.laser_projector = LaserProjection()


        self.__is_map_update = False
        self.__last_map_publish = time.time()

        #tf
        self.tf = TransformListener()
        rospy.Rate(1).sleep()
        print(self.tf.getFrameStrings())
        self.map_frame = None
        self.base_frame = None

        #laser
        self.max_laser_range = None
        self.laser_min_angle = None
        self.laser_max_angle = None
        self.laser_linear_update_thr = None
        self.laser_angular_update_thr = None
        self._lasr_data_update_pose = None
        self._lasr_data_update_quanternion= None

        self.last_map_msg = None  # this is used to publish same map if no change to map
        #map and planner update

        self._last_published_data = None
        # initialisation
        self.__read_ros_params()

        self.__init_subscribers()
        self.__init_publishers()

        self.__init = False

        self.data=[]

    def __del__(self):
        #close all timer and release all locks
        print('Map generator destroyed')


    def __str__(self):

        str2plot = "ROS Map Generator \n"
        str2plot += "ROS Params:\n"
        str2plot += "map_frame: {0}\n".format(self.map_frame)
        str2plot += "base_frame: {0}\n".format(self.base_frame)

        pose = self.__calculate_pose_at_time(ref_frame="base_link")
        str2plot += "Robot Pose: {0}\n".format(pose)
        str2plot += "Map is updated? {0}\n".format(self.__is_map_update)

        return str2plot


    def __read_ros_params(self):
        print("ROSMapGenerator: read params")

        # map
        self.map_frame = rospy.get_param("/autonomy/map_frame",
                                         "/odom"
                                         )

        print(self.map_frame)
        #base link
        self.base_frame =rospy.get_param("/autonomy/base_frame",
                                         "/base_link"
                                         )

        # linear update - Process a scan each time the robot translates this far
        self.laser_linear_update_thr = rospy.get_param("/autonomy/exploration/linearUpdate",
                                               0.5
                                               )
        # angular update - Process a scan each time the robot rotates this far
        self.laser_angular_update_thr = rospy.get_param("/autonomy/exploration/angularUpdate",
                                                       0.2
                                                       )

    def __init_subscribers(self):

        print("ROSMapGenerator: Init subscribers")
        #Map Update
        scan_topic = rospy.get_param("scan_topic", "/scan")
        self._laser_sub = rospy.Subscriber(scan_topic, LaserScan, self.__init_laser_params, queue_size=1)

        self._save_data_sub = rospy.Subscriber(str("save_data"), Bool, self._save_data_sub)




    def __init_publishers(self):

        print("ROSMapGenerator: Init publishers")

        # path publisger
        self._collected_data_pub = rospy.Publisher(str("points_marker"),
                                         Marker,
                                         queue_size=10
                                         )

    def __calculate_pose_at_time(self,ref_frame="/base_link", t=None):


        if t is None:
        #    warnings.warn("ROSWrapper: __calculate_pose_at_time - using lastest commonn time")
            t = self.tf.getLatestCommonTime(ref_frame, self.map_frame)

        try:
            self.tf.waitForTransform(self.map_frame, ref_frame, t, rospy.Duration(2.))
        except:
            warnings.warn("ROSMapGenerator: __calculate_pose_at_time - tranform issues")

        pose, quanternion = self.tf.lookupTransform(self.map_frame, ref_frame, t)

        return pose, quanternion

    def __init_laser_params(self, laser_msg):

        if self.__init:  # No update during init
            return

        print("ROSMapGenerator: __init_laser_params - init laser params")

        if self.max_laser_range is None:
            self.max_laser_range = laser_msg.range_max

        print(self.max_laser_range)

        self.laser_min_angle = laser_msg.angle_min
        self.laser_max_angle = laser_msg.angle_max

        self._laser_sub.unregister()

        scan_topic = rospy.get_param("scan_topic", "/scan")
        self._laser_sub = rospy.Subscriber(scan_topic, LaserScan, self.__read_laser_ranges_callback, queue_size=1)

        self.__read_laser_ranges_callback(laser_msg)

    def __read_laser_ranges_callback(self, laser_msg):

        if self.__init:  # No update during init
            return

        delta_t = rospy.Time.now() - laser_msg.header.stamp
        if delta_t > rospy.Duration(2.):
            # need to flush queue - od nothing in here
            print(" delta time: Now-laser: {0}".format(delta_t.to_sec()))
            return
        try:
            self.tf.waitForTransform(laser_msg.header.frame_id, self.map_frame, laser_msg.header.stamp,
                                     rospy.Duration(0.1))
        except(tf_exception):
            warnings.warn("ROSMapGenerator:__read_laser_ranges_callback - too old update")
            return  # no update

        print("__read_laser_ranges_callback...")
        update_data = False

        pose, quanternion = self.__calculate_pose_at_time(ref_frame=laser_msg.header.frame_id,
                                                 t=laser_msg.header.stamp
                                                 )


        if self._lasr_data_update_pose is None or self._lasr_data_update_quanternion is None:
            update_data = True
        else:
            prev_euler = transformations.euler_from_quaternion(self._lasr_data_update_quanternion)
            prev_roll = prev_euler[0]
            prev_pitch = prev_euler[1]
            prev_yaw = prev_euler[2]

            euler = transformations.euler_from_quaternion(quanternion)
            roll = euler[0]
            pitch = euler[1]
            yaw = euler[2]

            if np.linalg.norm(np.asarray(pose) - self._lasr_data_update_pose) > self.laser_linear_update_thr or \
                math.fabs(prev_roll - roll) > self.laser_angular_update_thr or \
                math.fabs(prev_pitch - pitch) > self.laser_angular_update_thr or \
                math.fabs(prev_yaw - yaw) > self.laser_angular_update_thr:            \
                update_data = True

        print("LRF update: need to update map: {}".format(update_data))
        if update_data:
            #pose was calculated before
            print("ROSMapGenerator: map update")

            self._lasr_data_update_pose = pose
            self._lasr_data_update_quanternion= quanternion

            cloud = self.laser_projector.projectLaser(laser_msg)


            print(cloud)

            print("Need to save to CSV, rviz?")


    def __create_data_marker_for_rviz_msg(self):

        if self.last_map_msg is not None:
            return self.last_map_msg

        #continue only if new msg need to be created

        msg = Marker()

        print("__create_data_marker_for_rviz_msg - not implmented yet")
        return msg

    def _save_data_sub(self, save_msg):

        if save_msg.data:
            self.save_data()




    def save_data(self, extra_str=''):

        if self.data is None:
            return

        timestr = time.strftime("%Y%m%d-%H%M%S")
        print('save_map_and_path - how to implement????')
        np.savetxt(timestr+"_laser_data_"+extra_str+".csv", self._last_published_map, delimiter=",")

def main():

    rospy.init_node(str("global_planner_main"))

    ros_wrap = ROSMapGenerator()

    rate = rospy.Rate(50)# rospy.get_param('~hz', 5))
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == "__main__":

    main()

    sys.exit()