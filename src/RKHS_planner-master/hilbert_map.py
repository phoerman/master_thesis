#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Implementation of Fourier, Nystroem, and sparse feature based Hilbert maps."""

import numpy as np
import random
import itertools
import warnings
import copy


from scipy import sparse

from sklearn.cluster import MiniBatchKMeans
from sklearn.kernel_approximation import RBFSampler, Nystroem
from sklearn.linear_model import SGDClassifier
from sklearn.metrics.pairwise import rbf_kernel, check_pairwise_arrays
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils.extmath import safe_sparse_dot
from sklearn.utils.validation import check_array

class UsageError(BaseException):

    """Exception raised when the code is used incorrectly."""

    def __init__(self, msg):
        """Creates a new UsageError instance.

        :param msg the error message
        """
        self.msg = msg

    def __str__(self):
        return "Usage error: {}".format(self.msg)


class IncrementalHilbertMap(object):

    """Incremental version of Hilbert maps using stochastic gradient descent."""

    def __init__(self, method, dimensions, components, gamma=1.0, alpha=0.0001, l1_ratio=0.15, use_rkhs=False):
        """Creates a new IncrementalHilbertMap instance.

        :param method the random feature transformation method to use
        :param parameters dictionary of parameters used by the random feature
            transformation method
        :param dimensions the dimensionality of the raw data
        :param components the number of random components to use
        """
        if method == "fourier":
            self.sampler = RBFSampler(
                    gamma,
                    n_components=components
            )
        elif method == "nystroem":
            self.sampler = Nystroem(
                    kernel="rbf",
                    gamma=gamma,
                    n_components=components
            )
        else:
            raise UsageError("Invalid method provided: {}".format(method))

        self.components = components
        self.feature_method = method
        self.scaler = MinMaxScaler()
        self.rkhs = use_rkhs

        # SGD version of logistic regression with elasticnet penalty
        self.classifier = SGDClassifier(
                loss="log",
                class_weight="auto",
                penalty="elasticnet",
                alpha=alpha,
                l1_ratio=l1_ratio
        )

    def fit(self, data):
        """Uses the provided data to fit the transoformation matrix.

        :param data the data to use in the fitting of the random feature
            transform
        """
        # Scale the data such that they all lie within [0, 1]
        if self.feature_method == "nystroem":
            # Select good inducing points for the Nystroem feature
            mbkm = MiniBatchKMeans(
                    n_clusters=self.sampler.get_params()["n_components"],
                    compute_labels=False,
                    batch_size=1000,
                    init_size=10000
            )
            mbkm.fit(data)
            self.sampler.fit(mbkm.cluster_centers_)
            self.scaler.fit(self.sampler.transform(mbkm.cluster_centers_))
        else:
            self.sampler.fit(data)

            # Build training dataset for the scalar
            indices = random.sample(range(len(data)), min(len(data), 10000))
            scaler_data = [data[i] for i in indices]
            self.scaler.fit(self.sampler.transform(scaler_data))

    def add(self, data, labels):
        """Updates the classifier with new data.

        :param data the raw data points to add
        :param labels the labels associated with the data
        """
        if self.rkhs:
            avg_features = []
            for entry in data:
                # Generate 25 samples with fixed variation from the model
                sample_coords = np.random.multivariate_normal(entry, [(0.2, 0.0), (0.0, 0.2)], 25)
                features = self.sampler.transform(sample_coords)
                avg_features.append(features.mean(axis=0))
                assert(len(avg_features[-1]) == self.components)
            rbf_data = avg_features
        else:
            rbf_data = self.sampler.transform(data)

        # Scale data and update the classifier
        scaled_data = self.scaler.transform(rbf_data)
        self.classifier.partial_fit(scaled_data, labels, classes=[0, 1])


class SparseHilbertMap(object):

    """Hilbert map using the sparse feature."""

    def __init__(self, centers, gamma=1.0, cutoff=0.001, alpha=0.0001, l1_ratio=0.5, rkhs=False):
        """Creates a new sparse hilbert map.

        :param centers the locations at which to create sparse kernels
        :param gamma the gamma parameter of the RBF kernel
        :param cutoff value below which kernel values are set to 0
        :param rkhs whether or not to use reproducing kernel hilbert spaces
        """
        self.centers = centers
        self.gamma = gamma
        self.cutoff = cutoff

        self.origin = np.min(self.centers, axis=0)
        self.max_map = np.max(self.centers, axis=0)
        self.map_res = np.max(self.centers[1:] - self.centers[:-1], axis=0)
        self.map_res_lrf_observations = self.map_res/3.
        self.origin_idx = np.array(self.origin - self.origin, dtype=np.int)
        self.lrf_obserations = dict()
        self.max_num_of_observation = 5
        self.cutoff_distance = np.sqrt(-np.log(self.cutoff) / self.gamma)

        self.map_bounds= []
        for origin, max_map in zip(self.origin, self.max_map):
            self.map_bounds.append((origin - self.cutoff_distance, max_map + self.cutoff_distance)) # (min_x,max_x), (min_y,max_y), etc.

        self.idx_cnt = 1 +  np.array(np.round((self.max_map - self.origin) / self.map_res),dtype=int)
        self.cutoff_delta_idx = np.ceil(self.cutoff_distance/self.map_res)
        #self.kernels_distance =
        self.rkhs = rkhs
        self.batch_size = 10000
        self.classifier = SGDClassifier(
            loss="log",
            penalty="elasticnet", #penalty="l2"
            #class_weight="auto",
            alpha=alpha,
            l1_ratio=l1_ratio,
        )

        self.perturbations = None
        self.alpha = None


    def set(self,alpha=0.0001, l1_ratio=0.8, gamma=1.0, loss='log', penalty = 'elasticnet'):

        self.gamma = gamma

        self.classifier= SGDClassifier(loss=loss,
                                       penalty=penalty,
                                       alpha=alpha,
                                       #class_weight="auto",
                                       l1_ratio=l1_ratio,
                                       )

    @staticmethod
    def chunked(it, size):
        it = iter(it)
        while True:
            p = tuple(itertools.islice(it, size))
            if not p:
                break
            yield p

    def update_all(self,all_lrf):
        self.lrf_obserations = all_lrf
        lrf = self.chunked(self.lrf_obserations, self.batch_size)
        offset = 0
        for lrf_data in lrf:
            print('add in progress: {}'.format(float(offset) / len(self.lrf_obserations)))

            d = []
            l = []
            for key in lrf_data:
                obs_lst = self.lrf_obserations[key]
                for obs in obs_lst:
                    d.append(obs[0])
                    l.append(obs[1])

            kernel = self._generate_sparse_kernel(d)

            # Update the classifier using the kernel matrix
            self.classifier.partial_fit(kernel, l, classes=[0, 1])
            offset += self.batch_size
        self.classifier.intercept_ = np.array([0])


    def add(self, data, labels, fix_incermental_weights=False):
        """Updates the classifier with new data.

        :param data the raw data points to add
        :param labels the labels associated with the data
        """
        # Since occupancy data is often skewed, we enforce _intercept =0 while traing and after training.
        # Optimal solution will be not to train of the intercept, however Scipi SGD calssifier training is a general
        # framework (not just for occupancy)

        self.classifier.intercept_ = np.array([0])
        if self.rkhs:
            process_data = self.rkhs_data(data)
            if process_data.shape[0] < 100:
                self.classifier.fit(process_data, labels)
            else:
                offset = 0
                while offset < process_data.shape[0]:
                    self.classifier.partial_fit(
                            process_data[offset:offset+self.batch_size],
                            labels[offset:offset+self.batch_size],
                            classes=[0, 1]
                    )
                    offset += self.batch_size
        else:
            if len(data) < 100:
                print('add in progress: small dataset')
                d,l = self.check_data_in_range(data, labels)
                kernel = self._generate_sparse_kernel(d)

                #kernel = rbf_kernel(data, self.centers, self.gamma)
                #kernel = sparse.csr_matrix(kernel * (kernel > self.cutoff))
                self.classifier.fit(kernel, l)
            else:

                offset = 0

                #if fix_incermental_weights:
                #    self.update_lrf_observationse(data, labels)
                weights_before = None
                if hasattr(self.classifier, "coef_"):
                    weights_before = copy.deepcopy(self.classifier.coef_)

                while offset < len(data):
                    print('add in progress: {}'.format(float(offset)/len(data)))
                    # Compute kernel matrix and sparsify it

                    #kernel = rbf_kernel(data[offset:offset+self.batch_size], self.centers, self.gamma)
                    #kernel = sparse.csr_matrix(kernel * (kernel > self.cutoff))


                    #l = labels[offset:offset + self.batch_size]
                    #self.update_lrf_observationse(data[offset:offset+self.batch_size],
                    #                                labels[offset:offset + self.batch_size]
                    #                                )


                    d, l = self.check_data_in_range(data[offset:offset+self.batch_size],
                                                    labels[offset:offset + self.batch_size]
                                                    )

                    kernel = self._generate_sparse_kernel(d)

                    # Update the classifier using the kernel matrix

                    if fix_incermental_weights and weights_before is not None: #Only update weights
                        # SGD changes all weights when it applies penalty, regardless if it is close to
                        #actual date. This may cause "amensia" with incermental data.
                        # To solve that, we need to create a mask of kernels that should not change (emphaise on the not!)
                        #
                        #Using set we choosing unique list of kernel that hav changed
                        modified_kernel_idx = set((kernel.indices % kernel.shape[1]).astype(int))

                        #Creating a mask if the negative
                        mask = np.ones(self.classifier.coef_.shape, dtype=bool)  # np.ones_like(a,dtype=bool)
                        mask[:, list(modified_kernel_idx)] = False #making an inverse mask - coef that should not change

                        #Update all weights!!!
                        self.classifier.partial_fit(kernel, l, classes=[0, 1])
                        #Reverting change in weights for features far away from data
                        self.classifier.coef_[mask] = weights_before[mask]

                    else:
                        self.classifier.partial_fit(kernel, l, classes=[0, 1])

                    offset += self.batch_size

        self.classifier.intercept_ = np.array([0])

    def check_data_in_range(self, data, labels):
        d_out = []
        l_out =[]


        for d, l in zip(data, labels):
            if all([self.map_bounds[i][0] <= d[i] <= self.map_bounds[i][1] for i in range(len(d))]): #check in rnage for each dim
                d_out.append(d)
                l_out.append(l)

        return d_out, l_out

    def update_lrf_observationse(self, data, labels):
        cnt = 0
        for d, lbl in zip(data, labels):
            idx = tuple(self.pose2index_lrf_observation(d))
            if idx in self.lrf_obserations:
                if lbl == 1:
                    self.lrf_obserations[idx].append(tuple([d,lbl]))
                elif len(self.lrf_obserations[idx]) < self.max_num_of_observation:
                    self.lrf_obserations[idx].append(tuple([d, lbl]))
            else:
                self.lrf_obserations[idx]=[tuple([d,lbl])]

    def rkhs_data(self, data):
        """Return features obtained using the RKHS for the given data.
        
        :param data the data entries for which to create RKHS content
        :return RKHS based features for the given input data
        """
        avg_features = None
        # Compute average feauters for each input point
        for entry in data:
            mean_map_sample_coords = np.random.multivariate_normal(
                    entry,
                    [(0.2, 0.0), (0.0, 0.2)],
                    10
            )
            kernel = rbf_kernel(mean_map_sample_coords, self.centers, self.gamma)
            kernel = kernel.mean(axis=0)
            kernel = sparse.csr_matrix(kernel * (kernel > self.cutoff))

            if avg_features is None:
                avg_features = kernel
            else:
                avg_features = sparse.vstack((avg_features, kernel))
        return avg_features

    def classify(self, query, kernel=None):
        """Returns the probabilistic prediction for the given data points.

        :param query the data points to perform predictions on
        :return predicted values for the given inputs
        """
        if self.rkhs:
            kernel = self.rkhs_data(query)
        else:
            if kernel is None:
                kernel = self._generate_sparse_kernel(query)

            #y2= self.classifier.predict_proba(kernel2)
            #kernel = rbf_kernel(query, self.centers, self.gamma)
            #kernel = sparse.csr_matrix(kernel * (kernel > self.cutoff))
            #print(y2-self.classifier.predict_proba(kernel))
        return self.classifier.predict_proba(kernel)

    def classify_grad(self, query, res=None):
        """Returns the probabilistic prediction gradient around query

        :param query the data points to perform predictions on
        :return predicted values for the given inputs
        """
        if self.rkhs:
            raise ValueError("SparseHilbertMap:classify_grad - unsupported RKHS")
            #kernel = self.rkhs_data(query)
        else:
            '''
            kernel = rbf_kernel(query, self.centers, self.gamma)
            query_mesh_x, cntr_mesh_x = np.meshgrid(self.centers[:, 0], query[:, 0])
            query_mesh_y, cntr_mesh_y = np.meshgrid(self.centers[:, 1], query[:, 1])
            dkernel_x = 2 * self.gamma * (query_mesh_x - cntr_mesh_x) * kernel
            dkernel_y = 2 * self.gamma * (query_mesh_y - cntr_mesh_y) * kernel

            dkernel_x = sparse.csr_matrix(dkernel_x * (kernel > self.cutoff))
            dkernel_y = sparse.csr_matrix(dkernel_y * (kernel > self.cutoff))

            if res is None: # to save computation if calculating grad after classification
                res = self.classify(query)
            res = res[:, 1][:, np.newaxis] * res[:, 0][:, np.newaxis]

            grad_x = res * safe_sparse_dot(dkernel_x, self.classifier.coef_.T, dense_output=True)
            grad_y = res * safe_sparse_dot(dkernel_y, self.classifier.coef_.T, dense_output=True)
            '''

            kernel, dkernel = self._generate_sparse_kernel_grad(query)
            if res is None:  # to save computation if calculating grad after classification
                if kernel is None:
                    res = self.classify(query)
                else:
                    res = self.classify(query, kernel=kernel)
            res = res[:, 1][:, np.newaxis] * res[:, 0][:, np.newaxis]


            D = self.centers[0].size
            grad = []
            for d in range(D):
                grad.append(res * safe_sparse_dot(dkernel[d], self.classifier.coef_.T, dense_output=True))


        # return only grad of the p(y=+1|x) since gradient of p(y=-1|x) is just the negative
        return grad

    def entropy(self, query, include_perturbation=False):
        '''
            Return the entropy of query
        :param query: query the data points to perform predictions on
        :return: entropy of prediction
        '''

        if include_perturbation:
            assert self.alpha is not None
            res = self.classify_with_perturbation(query)
        else:
            res = self.classify(query)

        return -res[:, 0][:, np.newaxis] *np.log2(res[:, 0][:, np.newaxis])-res[:, 1][:, np.newaxis] * np.log2(res[:, 1][:, np.newaxis])

    def entropy_grad(self, query, include_perturbation=False, debug=False):
        '''
            Return the entropy gradient of query. As this is a Bernoulli variable the grad of entropy
            is dH/dx = DH/dP * dP/dx = -log2(p/(1-P))*grad_x(p)
        :param query: query the data points to perform predictions on
        :return: entropy gradient of prediction
        '''

        if include_perturbation and self.alpha is not None:
            assert self.alpha is not None
            res = self.classify_with_perturbation(query)
            grad_x, grad_y = self.classify_grad_with_perturbation(query, res=res)
            if debug:
                extent = (
                np.min(query[:, 0]), np.max(query[:, 0]), np.max(query[:, 1]), np.min(query[:, 1]))  # Note order of Y

                res_plot = np.reshape(res[:, 1], (40, 33))
                import matplotlib.pyplot as pl
                pl.figure(17)
                pl.imshow(res_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.))

                grad_x_plot = np.reshape(grad_x, (40, 33))
                import matplotlib.pyplot as pl
                pl.figure(19)
                pl.imshow(grad_x_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.))


        else:
            res = self.classify(query)
            grad_x, grad_y = self.classify_grad(query, res=res)

            if debug:
                res_plot = np.reshape(res[:,1], (40,33))
                import matplotlib.pyplot as pl
                pl.figure(9)
                pl.imshow(res_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.))

                grad_x_plot = np.reshape(grad_x, (40,33))
                import matplotlib.pyplot as pl
                pl.figure(11)
                pl.imshow(grad_x_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.))

        dHdp = -np.log2(res[:, 0]/res[:, 1])[:,np.newaxis]
        dHdp[np.isinf(dHdp)] = 0.

        if debug:
            dHdp_plot = np.reshape(dHdp, (40,33))
            import matplotlib.pyplot as pl
            pl.figure(13)
            pl.imshow(dHdp_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.))

            pupu = dHdp*grad_x
            dHdp_plot = np.reshape(pupu, (40,33))
            import matplotlib.pyplot as pl
            pl.figure(15)
            pl.imshow(dHdp_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.))

        return dHdp*grad_x, dHdp*grad_y

    def set_perturbations(self, X, Q):
        '''

        :param X: location of perturbation
        :param Q: mdesired probablity at X
        :return:
        '''
        if X.shape[0] == 0:
            return

        X = check_array(X)
        #if not isinstance(X, np.ndarray):
        #    X = np.asarray(X)

        #if not isinstance(Q, np.ndarray):
        Q = np.array(Q).reshape(1,-1)
        Q = check_array(Q)
        s_n = 0.1
        p = self.classify(X)
        k_xs_xs = rbf_kernel(X, X, self.gamma) + s_n**2*np.eye(X.shape[0])
        k_xs_xs = k_xs_xs * (k_xs_xs > self.cutoff)
        self.alpha = np.linalg.solve(k_xs_xs,(np.log(np.reciprocal(p[:,1])-1)-np.log(np.reciprocal(Q)-1)).T).T
        self.perturbations = X

    def classify_with_perturbation(self,query, debug=False):

        if self.rkhs:
            raise ValueError('SparseHilbertMap:classify_with_perturbation - rkhs option not supported')

        if self.perturbations is None or self.alpha is None:
            raise ValueError('SparseHilbertMap:classify_with_perturbation - perturbations are undefined')
        query = check_array(query)
        k_x = rbf_kernel(query, self.perturbations, self.gamma)

        if debug:

            extent = (np.min(query[:,0]), np.max(query[:,0]), np.max(query[:,1]), np.min(query[:,1]))  # Note order of Y

            res_plot = np.reshape(safe_sparse_dot(k_x, self.alpha.T, dense_output=True), (40, 33))
            import matplotlib.pyplot as pl
            pl.figure(21)
            pl.imshow(res_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.), extent=extent)

        kernel = self._generate_sparse_kernel(query)
        #kernel = rbf_kernel(query, self.centers, self.gamma)
        #kernel = sparse.csr_matrix(kernel * (kernel > self.cutoff))
        if debug:
            res_plot = np.reshape(safe_sparse_dot(kernel, self.classifier.coef_.T, dense_output=True) +
                self.classifier.intercept_, (40, 33))
            import matplotlib.pyplot as pl
            pl.figure(23)
            pl.imshow(res_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.), extent=extent)

        prob = (safe_sparse_dot(kernel, self.classifier.coef_.T, dense_output=True) +
                self.classifier.intercept_ +
                safe_sparse_dot(k_x, self.alpha.T, dense_output=True)
                ).ravel()

        if debug:
            res_plot = np.reshape(prob, (40, 33))
            import matplotlib.pyplot as pl
            pl.figure(25)
            pl.imshow(res_plot, interpolation='bilinear', cmap='hot', clim=(-1., 1.), extent=extent)
        prob *= -1
        np.exp(prob, prob)
        prob += 1
        np.reciprocal(prob, prob)
        if prob.ndim == 1:
            return np.vstack([1 - prob, prob]).T
        #print('prob={}'.format(prob[0, 1]))

    def classify_grad_with_perturbation(self, query, res=None):
        """Returns the probabilistic prediction gradient around query

        :param query the data points to perform predictions on
        :return predicted values for the given inputs
        """
        if self.rkhs:
            raise ValueError('classify_grad_with_perturbation:classify_with_perturbation - rkhs option not supported')

        if self.perturbations is None or self.alpha is None:
            raise ValueError('classify_grad_with_perturbation:classify_with_perturbation - perturbations are undefined')

        #standard grad
        '''
        kernel = rbf_kernel(query, self.centers, self.gamma)
        query_mesh_x, cntr_mesh_x = np.meshgrid(self.centers[:, 0], query[:, 0])
        query_mesh_y, cntr_mesh_y = np.meshgrid(self.centers[:, 1], query[:, 1])
        dkernel_x = 2 * self.gamma * (query_mesh_x - cntr_mesh_x) * kernel
        dkernel_y = 2 * self.gamma * (query_mesh_y - cntr_mesh_y) * kernel

        dkernel_x = sparse.csr_matrix(dkernel_x * (kernel > self.cutoff))
        dkernel_y = sparse.csr_matrix(dkernel_y * (kernel > self.cutoff))
        '''
        kernel, dkernel_x, dkernel_y = self._generate_sparse_kernel_grad(query)


        #Perturbations
        k_x = rbf_kernel(query, self.perturbations, self.gamma)
        per_query_mesh_x, per_per_mesh_x = np.meshgrid(self.perturbations[:, 0], query[:, 0])
        per_query_mesh_y, per_per_mesh_y = np.meshgrid(self.perturbations[:, 1], query[:, 1])

        dk_x_x = 2 * self.gamma * (per_query_mesh_x - per_per_mesh_x) * k_x
        dk_x_y = 2 * self.gamma * (per_query_mesh_y - per_per_mesh_y) * k_x

        if res is None: # to save computation if calculating grad after classification
            res = self.classify_with_perturbation(query)

        res = res[:, 1][:, np.newaxis] * res[:, 0][:, np.newaxis]

        grad_x = res * (safe_sparse_dot(dkernel_x, self.classifier.coef_.T, dense_output=True) +
                        safe_sparse_dot(dk_x_x, self.alpha.T, dense_output=True)
                        )

        grad_y = res * (safe_sparse_dot(dkernel_y, self.classifier.coef_.T, dense_output=True) +
                        safe_sparse_dot(dk_x_y, self.alpha.T, dense_output=True)
                        )

        # return only grad of the p(y=+1|x) since gradient of p(y=-1|x) is just the negative
        return grad_x, grad_y

    def mutual_information(self, query):

        return self.entropy(query, include_perturbation=False)-self.entropy(query, include_perturbation=True)

    def mutual_information_grad(self, query):

        g_map_x, g_map_y = self.entropy_grad(query, include_perturbation=False)
        g_per_x, g_per_y = self.entropy_grad(query, include_perturbation=True)

        return g_map_x-g_per_x, g_map_y-g_per_y

    def index2pose(self, idx):
        return np.array(idx*self.map_res + self.origin, dtype=np.float)

    def pose2index(self, x):
        return np.array((x - self.origin) / self.map_res, dtype=np.int)

    def pose2index_lrf_observation(self, x):
        return np.array((x - self.origin) / self.map_res_lrf_observations, dtype=np.int)

    def generate_index_box(self,idx):
        idx = np.squeeze(idx)
        ax_idxs = np.meshgrid(*[np.arange(max(0, idx[i] - self.cutoff_delta_idx[0]),
                                          min(self.idx_cnt[i], idx[i] + self.cutoff_delta_idx[i] + 1),
                                          dtype=np.int
                                          ) for i in range(idx.size)],
                              indexing='ij'
                              )

        mult = 1
        indices = None
        for i in reversed(range(len(ax_idxs))):
            indices = mult * ax_idxs[i].ravel() if indices is None \
                else indices + mult * ax_idxs[i].ravel()
            mult *= self.idx_cnt[i]

        return indices

    def _generate_sparse_kernel(self, query):
        import matplotlib.pyplot as pl
        kernel = []
        idx_list = []
        data = []
        indptr = [0]
        if isinstance(query,list):
            query = np.asarray(query)
        for q in query:
            idx = self.pose2index(q)
            try:
                centre_flat_idx = self.generate_index_box(idx)

                s_kernel = np.squeeze(rbf_kernel(q.reshape(1, -1), self.centers[centre_flat_idx], self.gamma))
                mask = s_kernel > self.cutoff
                idx_list.extend(centre_flat_idx[mask])
                data.extend(s_kernel[mask])
                indptr.append(indptr[-1] + len(mask[mask]))

            except Exception as e:
                print(e)
                warnings.warn('HilbertSparseMap: _generate_sparse_kernel - unable to compute rbf for {}'.format(q))

                idx_list.extend([])
                data.extend([])
                indptr.append(indptr[-1])


        kernel = sparse.csr_matrix((np.array(data),
                                    np.array(idx_list),
                                    indptr
                                    ),
                                   (len(query), self.centers.shape[0])
                                   )

        return kernel

    def _generate_sparse_kernel_grad(self, query):
        import matplotlib.pyplot as pl
        idx_list = []
        data = []
        data_x = []
        data_y = []
        indptr = [0]

        D = self.centers[0].size
        dkernel = [[] for d in range(D)]

        for q in query:
            idx = self.pose2index(q)
            centre_flat_idx = self.generate_index_box(idx)
            try:
                s_kernel = np.squeeze(rbf_kernel(q.reshape(1, -1), self.centers[centre_flat_idx], self.gamma))
                centre_flat_idx = self.generate_index_box(idx)

                mask = s_kernel > self.cutoff
                idx_list.extend(centre_flat_idx[mask])
                data.extend(s_kernel[mask])
                for d in range(D):
                    grad_data = 2 * self.gamma * (self.centers[centre_flat_idx][:,d]-q[d]) * s_kernel # order is different than standard classify_grad because of they way meshgrod works
                    dkernel[d].extend(grad_data[mask])

                indptr.append(indptr[-1] + len(mask[mask]))

            except Exception as e:
                warnings.warn('HilbertSparseMap: _generate_sparse_kernel_grad - unable to compute rbf for {}'.format(q))
                print(e)

                idx_list.extend([])
                data.extend([])
                data_x.extend([])
                data_y.extend([])
                indptr.append(indptr[-1])

        kernel = sparse.csr_matrix((np.array(data),
                                    np.array(idx_list),
                                    indptr
                                    ),
                                   (len(query), self.centers.shape[0])
                                   )
        for d in range(D):
            dkernel[d] = sparse.csr_matrix((np.array(dkernel[d]),
                                    np.array(idx_list),
                                    indptr
                                    ),
                                   (len(query), self.centers.shape[0])
                                   )

        return kernel, dkernel

