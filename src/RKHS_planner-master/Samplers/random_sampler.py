import numpy as np

from Samplers.AbstractSampler import AbstractSampler


class RandomSampler(AbstractSampler):

    def __init__(self):

        super(RandomSampler, self).__init__()

    def __str__(self):

        return 'Random Sampler'

    def draw_samples(self, n):
        """

        :param n: number of draws
        :return: returns weighted samples.
        """

        return np.random.rand(n)

    def update(self, samples_update):
        pass

    def entropy(self):
        return 0. # unifrom sampling - differential entopry ln(b-a) = ln(1.) = 0.

    def max_entropy(self):
        return 0