class AbstractSampler(object):

    def __init__(self):
        pass

    def draw_scaled_samples(self, n, interval=(0,1)):

        samples_0_1 = self.draw_samples(n)

        return [(interval[1]-interval[0])*sample + interval[0] for sample in samples_0_1]

    def draw_samples(self, n):
        raise NotImplementedError("AbstractSampler: draw_samples function not implemented")

    def update(self, samples_update):
        raise NotImplementedError("AbstractSampler: update function not implemented")

    def entropy(self):
        raise NotImplementedError("AbstractSampler: entropy function not implemented")

    def max_entropy(self):
        raise NotImplementedError("AbstractSampler: max_entropy function not implemented")
