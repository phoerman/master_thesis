import numpy as np
from collections import deque

from Samplers.AbstractSampler import AbstractSampler


class Weight:

    def __init__(self, history, beta):
        '''

        :param history: depth of queue
        :param beta: an offset when weight is computed
        '''
        self._buffer = deque(maxlen=history)
        self._beta = beta

    def add(self, value):
        self._buffer.append(value)

    def __call__(self):
        return sum(self._buffer)/len(self._buffer) + self._beta


class WeightedSampler(AbstractSampler):

    #  Returns random samples drawn from weighted intervals of [0 1]
    #  Weights define the importance of a specific interval [a, b].
    #  Weights are controlled by the external process
    def __init__(self, num_intervals, history, **kwargs):
        """

        :param num_intervals: number of interval in [0,1]
        :param history: Memory depth. This parameter allow the sampler to record sample success/failure and adjust
                        sampling accordingly.
        """
        super(WeightedSampler, self).__init__()

        self._n = num_intervals

        self._step = 1./self._n

        self._weights = []
        self._intervals = []

        self._normaliser = 0

        self._last_samples_indices = []

        self._beta = 1./self._n

        #  Init intervals and weigths
        for i in range(0, self._n):
            w = Weight(history, self._beta)
            w.add(1.)
            self._weights.append(w)
            self._intervals.append((i*self._step, (i+1)*self._step))
            self._normaliser += self._weights[i]()

        self.const_length_bool = False
        if 'path' in kwargs.keys():
            self.const_length_bool = True
            self.path = kwargs['path']  # reference to path




    def __str__(self):

        str2plot = "WeightedSampler, number of intervals: {}\n".format(self._n)
        str2plot += "Interval   |   Weight\n"
        for key, weight in zip(self._intervals, self._weights):
            str2plot += "{0}           {1}\n".format(key, weight())

        return str2plot

    def draw_samples(self, n, return_p=False, add_points=list([])):
        """

        :param n: number of draws
        :param return_p: Bool to indicate whether to return the change in p
        :param add_points: add_points is a llist of additional points we would like to draw - this is done so everythiong is consitent
        :return: returns weighted samples.
        """

        pvals = [weight()/self._normaliser for weight in self._weights]

        num_of_samples = np.random.multinomial(n, pvals)

        samples = []
        samples_indices = []
        samples_p_change = []
        unbiased_sampling_p = 1./self._n

        for i, interval in enumerate(self._intervals):

            new_samples = list(np.random.random(num_of_samples[i])*self._step + interval[0])
            samples.extend(new_samples)
            samples_indices.extend([i]*len(new_samples))
            samples_p_change.extend([pvals[i]/unbiased_sampling_p] * len(new_samples))

        for point in add_points:
            for i, interval in enumerate(self._intervals):
                if interval[0] < point <= interval[1]:
                    samples.extend([point])
                    samples_indices.extend([i])
                    samples_p_change.extend([pvals[i] / unbiased_sampling_p])

        self._last_samples_indices = samples_indices

        if return_p:
            return samples, samples_p_change
        else:
            return samples

    def update(self, samples_update):

        self.update_sampling_weights(self._last_samples_indices, samples_update)

        if self.const_length_bool:
            self._update_intervals()

    def update_sampling_weights(self, samples_indices, sample_success):
        """
        Updates the weights for sampling based on the success/failure of the sampling.

        :param samples_indices: The samples used. Provide the key for the weight update
        :param sample_success: A list of weight update. should be a positive number
        :return:
        """

        assert len(samples_indices) == len(sample_success)

        for index , weight_update in zip(samples_indices, sample_success):
            self._weights[index].add(weight_update)

        self._normaliser = 0
        for weight in self._weights:
            self._normaliser += weight()

    def _update_intervals(self):

        if not self.const_length_bool:
            return
        num = 100
        X = self.path.mean(np.linspace(start=0.,
                                       stop=1.,
                                       num=num,
                                       endpoint=True
                                       )
                           )

        d = np.linalg.norm(X[1:,:]-X[:-1,:], axis=1) #  Distance betweem consecutive points

        d_cum = np.cumsum(d)  # accumalitive Length

        new_interval_length = d_cum[-1] / len(self._intervals)

        a = 0.
        segment = 0
        segment_cum_length = (segment + 1) * new_interval_length
        for idx, dl in enumerate(d_cum):
            if dl >= segment_cum_length:
                b = float(idx) / (num - 1)
                self._intervals[segment] = (a,b)
                # prepare nextx segment
                a = b
                segment += 1
                segment_cum_length = (segment + 1) * new_interval_length

        #  Ensure closure

        self._intervals[-1] = (self._intervals[-1][0], 1.)



    def entropy(self):

        pvals = np.asarray([weight() / self._normaliser for weight in self._weights])

        return np.sum(-pvals*np.log2(pvals))

    def max_entropy(self):

        return np.asscalar(np.log2(self._n))

if __name__ == "__main__":

    sampler = WeightedSampler(10, 5)

    print(sampler)

    samples, samples_indices = sampler.draw_samples(20)
    print("samples - unbiased:")
    print(samples)

    success = [ (x<0.5) for x in samples]
    sampler.update_sampling_weights(samples_indices,success)

    samples2, samples_indices2 = sampler.draw_samples(20)
    print("samples - biased #1:")
    print(samples2)

    success = [(x < 0.5) for x in samples2]
    sampler.update_sampling_weights(samples_indices2, success)

    samples3, samples_indices3 = sampler.draw_samples(20)
    print("samples - biased #2:")
    print(samples3)