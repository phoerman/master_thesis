import numpy as np

from Samplers.AbstractSampler import AbstractSampler


class DetermisticSampler(AbstractSampler):
    #  samples cost from current path and find points that maximize cost (wiht in specific resolution) without
    #  voilate max value

    def __init__(self, planner, valid_cost_thr, number_of_segments):

        super(DetermisticSampler, self).__init__()

        self._planner = planner
        self._valid_cost_thr = valid_cost_thr

        self._resolution = 0.001  # time (path latent variable) resolution
        self._number_of_segments = number_of_segments

        self._t = []
        segment_deltaT = 1./self._number_of_segments

        for i in range(0, self._number_of_segments):
            t_init = i*segment_deltaT
            self._t.append(np.arange(start=t_init,
                                     stop=t_init+segment_deltaT,
                                     step=self._resolution))

    def __str__(self):

        str2plot = 'Determinstic Sampler:\n'
        str2plot += 'valid_cost_thr = {0}\n'.format(self._valid_cost_thr)
        str2plot += 'resolution = {0}\n'.format(self._resolution)
        str2plot += 'number_of_segments = {0}\n'.format(self._number_of_segments)

        return str2plot

    def draw_samples(self, n):
        """

        :param n: number of draws
        :return: returns weighted samples.
        """

        assert (n == self._number_of_segments)

        samples = []

        for t in self._t:
            cost = self._planner.obs_cost(t) + \
                   self._planner.regulariser_beta * self._planner.control_cost(t)
             # self._planner.regulariser_beta * [self._planner.control_cost(t_i) for t_i in t]
            cost[cost >= self._valid_cost_thr ] = np.nan
            try:
                samples.append(t[np.nanargmax(cost)])
            except ValueError: #  could not find non-nan value
                samples.append(t[0])

        return samples

    def update(self, samples_update):
        pass

    def entropy(self):
        return 0. # Deterministic sampler - no entropy

    def max_entropy(self):
        return 0

