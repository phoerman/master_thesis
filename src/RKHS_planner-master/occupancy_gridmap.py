"""Module representing a simple counting occupancy grid map."""


import math
import matplotlib.pyplot as plt
import numpy as np

from util import bounding_box, bresenham, normalize_angle, data_generator


class OccupancyGridmap(object):

    def __init__(self, x_limits, y_limits, resolution):
        """Creates a new OccupancyGrid instance.

        :params x_limits x axis limits
        :params y_limits y axis limits
        :params resolution grid size resolution
        """
        self.x_limits = x_limits
        self.y_limits = y_limits
        #self.x_limits = y_limits
        #self.y_limits = x_limits
        self.resolution = resolution

        x_count = int(math.ceil((x_limits[1] - x_limits[0]) / resolution))
        y_count = int(math.ceil((y_limits[1] - y_limits[0]) / resolution))

        #print(x_count, y_count, x_limits, y_limits)
        self.free = np.zeros((x_count, y_count), dtype=np.uint16)
        self.hit = np.zeros((x_count, y_count), dtype=np.uint16)
        self.occupancy = np.zeros((x_count, y_count))
        self.occupancy.fill(0.5)

    def add(self, pose, scan):
        """Adds a new observation to the grid map.

        :params pose the pose the observation was made from
        :params scan the laser scan ranges of the observation
        """
        start_point = (pose[0], pose[1])
        angle_increment = math.pi / len(scan)
        for i, dist in enumerate(scan):
            # Ignore max range readings
            if dist > 40:
                continue

            angle = normalize_angle(
                    pose[2] - math.pi + i * angle_increment + (math.pi / 2.0)
            )

            # Add laser endpoint
            end_point = (
                pose[0] + dist * math.cos(angle),
                pose[1] + dist * math.sin(angle)
            )

            self.mark_along_line(start_point, end_point)

    def to_grid(self, coord):
        """Returns the tile key corresponding to the given word coordinates.

        :param x coordinate along the x axis
        :param y coordinate along the y axis
        :return tuple of the corresponding tile indices
        """
        rel_x = coord[0] - self.x_limits[0]
        rel_y = coord[1] - self.y_limits[0]

        if not (0 <= rel_x <= (self.x_limits[1] - self.x_limits[0])) or \
           not (0 <= rel_y <= (self.y_limits[1] - self.y_limits[0])):
            print("Invalid coordinate requested")
            return (0, 0)

        return (
                int(math.floor(rel_x / self.resolution)),
                int(math.floor(rel_y / self.resolution))
        )

    def mark_along_line(self, start_point, end_point):
        """Marks all points along the line between start and end point.

        :params start_point starting location for the laser beam
        :params end_point end location for the laser beam
        """

        # Transform world coordinates to grid coordinates
        grid_start = self.to_grid(start_point)
        grid_end = self.to_grid(end_point)

        # Mark all points on the line between start and end
        coords = bresenham(grid_start, grid_end)
        for pt in coords[:-1]:
            self.free[pt] += 1
            self.occupancy[pt] = self.hit[pt] / float(self.hit[pt] + self.free[pt])
        pt = coords[-1]
        self.hit[pt] += 1
        self.occupancy[pt] = self.hit[pt] / float(self.hit[pt] + self.free[pt])

    def visualize_map(self, xlim, ylim, res, fname=None):
        x = xlim[0]
        y = ylim[0]

        x_count = int(math.ceil((xlim[1] - xlim[0]) / res))
        y_count = int(math.ceil((ylim[1] - ylim[0]) / res))

        occupancy = self.occupancy
        occ = np.zeros((x_count+1, y_count+1))
        
        xid = 0
        while x < xlim[1]:
            yid = y_count-1
            while yid >= 0:
                occ[(xid, yid)] = occupancy[self.to_grid((x, y))]
                yid -= 1
                y += res
            y = ylim[0]
            xid += 1
            x += res

        plt.clf()
        plt.imshow(occ.transpose(), vmin=0.0, vmax=1.0)
        # plt.imshow(occupancy.transpose())
        plt.colorbar()

        if fname is None:
            plt.show()
        else:
            plt.savefig(fname, dpi=500)



class OccupancyGridmapISM(object):

    def __init__(self, x_limits, y_limits, resolution):
        """Creates a new OccupancyGrid instance.

        :params x_limits x axis limits
        :params y_limits y axis limits
        :params resolution grid size resolution
        """
        self.x_limits = x_limits
        self.y_limits = y_limits
        self.resolution = resolution

        x_count = int(math.ceil((x_limits[1] - x_limits[0]) / resolution))
        y_count = int(math.ceil((y_limits[1] - y_limits[0]) / resolution))

        # Sensor model parameters
        self.p_free = math.log(0.2)
        self.p_prior = math.log(0.5)
        self.p_occ = math.log(0.8)
        self.radius = 0.1

        self._log_odds = np.zeros((x_count, y_count))
        self._log_odds.fill(0.0)

    @property
    def occupancy(self):
        return 1.0 - (1.0 / (1.0 + np.exp(self._log_odds)))

    def add(self, pose, scan):
        """Adds a new observation to the grid map.

        :params pose the pose the observation was made from
        :params scan the laser scan ranges of the observation
        """
        start_point = (pose[0], pose[1])
        angle_increment = math.pi / len(scan)
        for i, dist in enumerate(scan):
            # Ignore max range readings
            if dist > 40:
                continue

            angle = normalize_angle(
                    pose[2] - math.pi + i * angle_increment + (math.pi / 2.0)
            )

            # Add laser endpoint
            end_point = (
                pose[0] + (0.1 + dist) * math.cos(angle),
                pose[1] + (0.1 + dist) * math.sin(angle)
            )

            self.mark_along_line(start_point, end_point, dist)

    def to_grid(self, coord):
        """Returns the tile key corresponding to the given word coordinates.

        :param x coordinate along the x axis
        :param y coordinate along the y axis
        :return tuple of the corresponding tile indices
        """
        rel_x = coord[0] - self.x_limits[0]
        rel_y = coord[1] - self.y_limits[0]

        if not (0 <= rel_x <= (self.x_limits[1] - self.x_limits[0])) or \
           not (0 <= rel_y <= (self.y_limits[1] - self.y_limits[0])):
            print("Invalid coordinate requested")
            return (0, 0)

        return (
                int(math.floor(rel_x / self.resolution)),
                int(math.floor(rel_y / self.resolution))
        )

    def mark_along_line(self, start_point, end_point, dist):
        """Marks all points along the line between start and end point.

        :params start_point starting location for the laser beam
        :params end_point end location for the laser beam
        :params dist the length of the beam
        """

        # Transform world coordinates to grid coordinates
        grid_start = self.to_grid(start_point)
        grid_end = self.to_grid(end_point)

        # Mark all points on the line between start and end
        coords = bresenham(grid_start, grid_end)
        coords.extend(bresenham(grid_start, (grid_end[0]-1, grid_end[1])))
        coords.extend(bresenham(grid_start, (grid_end[0]+1, grid_end[1])))
        coords.extend(bresenham(grid_start, (grid_end[0], grid_end[1]-1)))
        coords.extend(bresenham(grid_start, (grid_end[0], grid_end[1]+1)))
        coords = list(set(coords))

        for pt in coords:
            cell_dist = math.sqrt(
                    ((grid_start[0] - pt[0]) * self.resolution)**2  +
                    ((grid_start[1] - pt[1]) * self.resolution)**2 
            )
            
            self._log_odds[pt] += self._sensor_model(dist, cell_dist) - self.p_prior

    def _sensor_model(self, beam_dist, cell_dist):
        if cell_dist > (beam_dist + self.radius):
            return self.p_prior
        elif cell_dist > (beam_dist - self.radius):
            return self.p_occ
        else:
            return self.p_free

    def visualize_map(self, xlim, ylim, res, fname=None):
        x = xlim[0]
        y = ylim[0]

        x_count = int(math.ceil((xlim[1] - xlim[0]) / res))
        y_count = int(math.ceil((ylim[1] - ylim[0]) / res))

        occupancy = self.occupancy
        occ = np.zeros((x_count+1, y_count+1))
        
        xid = 0
        while x < xlim[1]:
            yid = y_count-1
            while yid >= 0:
                occ[(xid, yid)] = occupancy[self.to_grid((x, y))]
                yid -= 1
                y += res
            y = ylim[0]
            xid += 1
            x += res

        plt.clf()
        plt.imshow(occ.transpose(), vmin=0.0, vmax=1.0)
        # plt.imshow(occupancy.transpose())
        plt.colorbar()

        if fname is None:
            plt.show()
        else:
            plt.savefig(fname, dpi=500)



def create_occupancy_grid_map(dataset):
    """Generates an occupancy grid map from the data.

    The occupancy values are obtained using the simple counting method.

    :param fname path to the carmen log file to parse
    """
    # Determine size of the gridmap
    scan_endpoints = []
    for data, label in data_generator(dataset["poses"], dataset["scans"]):
        scan_endpoints.extend(data)
    xlim, ylim = bounding_box(scan_endpoints, 10)

    # Build actual gridmap
    gridmap = OccupancyGridmap(xlim, ylim, 0.1)
    for pose, scan in zip(dataset["poses"], dataset["scans"]):
        gridmap.add(pose, scan)

    return gridmap
