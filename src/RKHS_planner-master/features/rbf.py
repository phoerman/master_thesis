import numpy as np
from scipy.special import erf
from sklearn.kernel_approximation import Nystroem
from sklearn.metrics.pairwise import rbf_kernel

from sklearn.utils import check_array, check_random_state, as_float_array
from sklearn.utils.extmath import safe_sparse_dot
from  sklearn.utils.validation import check_is_fitted

from features.AbstractFeature import AbstractFeature


class RBF(AbstractFeature):

    def __init__(self, n_components=100, gamma=10., D=1):

        assert D > 0

        super(RBF, self).__init__()

        n_components = pow(n_components, 1/D)

        self.centers = np.meshgrid(*[np.linspace(start=0.,
                                                 stop=1.,
                                                 num=n_components,
                                                 endpoint=True
                                                 ) for _ in range(D)])

        self._init_sampler(components=self.centers,
                           gamma=gamma)

    def set_basis(self, centers, gamma=10.):

            self._init_sampler(components=centers,
                               gamma=gamma
                               )

    def evaluate(self, t):
        return self.sampler.transform(X=np.asarray(t).reshape(-1, 1))

        #  return rbf_kernel(self.t0, np.asarray(t).reshape(-1, 1), self.gamma)

    def gradient(self, t):

        #  t = check_array(np.asarray(t).reshape(-1, 1), accept_sparse='csr')
        #  return -2 * self.gamma * (t - self.centers) * self.evaluate(t)

        #  check_is_fitted(self.sampler, 'random_weights_')
        t = np.asarray(t).reshape(-1, 1)

        embedded = rbf_kernel(t,
                              self.sampler.components_,
                              gamma=self.gamma
                              )
        embedded *= 2*self.gamma*(self.centers.T - t)

        return np.dot(embedded, self.sampler.normalization_.T)

    def gradient2(self, t):

        t = np.asarray(t).reshape(-1, 1)

        embedded = rbf_kernel(t,
                              self.sampler.components_,
                              gamma=self.gamma
                              )
        embedded *= (-2 * self.gamma * (1 - 2*self.gamma*(t - self.centers.T)*(t - self.centers.T)))
        return np.dot(embedded, self.sampler.normalization_.T)

    def integral(self, t):
        #Integral from [0,t]
        t = np.asarray(t).reshape(-1, 1)

        #NOTE: scipy erf has slightly different definition
        #erf_scipy = 2/sqrt(pi)*integral(exp(-t**2)
        erf0 = erf(-self.sampler.components_*np.sqrt(self.gamma))

        erft = erf((t-self.sampler.components_.T)*np.sqrt(self.gamma))

        return (np.sqrt(np.pi)/(2*np.sqrt(self.gamma)))*np.dot((erft-erf0.T), self.sampler.normalization_.T)







    def _init_sampler(self, components, gamma):
        self.sampler = Nystroem(gamma=gamma,
                                n_components=len(components)
                                )

        self.gamma = gamma
        self.centers = np.asarray(components).reshape(-1, 1)

        basis_kernel = rbf_kernel(self.centers,
                                  gamma=self.gamma
                                  )

        # sqrt of kernel matrix on basis vectors
        U, S, V = np.linalg.svd(basis_kernel)
        S = np.maximum(S, 1e-12)

        self.sampler.normalization_ = np.dot(U * 1. / np.sqrt(S), V)
        self.sampler.components_ = self.centers

if __name__ == "__main__":

    import matplotlib.pyplot as pl

    n_components = 10
    P = RBF(n_components=n_components,
            gamma=10.
            )

    w = np.ones((n_components,),
                dtype=float)

    dt = 0.01
    t_plot = np.arange(start=0.,
                       stop=1. + dt,
                       step=dt)

    X = P.evaluate(t_plot).dot(w)
    pl.figure()
    pl.plot(t_plot, np.squeeze(X), 'm.')
    pl.plot(t_plot, np.sum(P.sampler.transform(X=np.asarray(t_plot).reshape(-1, 1)), 1), 'k--')
    pl.show(block=False)

    numerical_grad = (X[1:] - X[:-1]) / dt
    t_plot_for_grad = (t_plot[:-1]) + dt
    pl.figure()
    pl.plot(t_plot_for_grad, np.squeeze(numerical_grad), 'm.')
    pl.plot(t_plot, P.gradient(t_plot), 'r--')
    pl.plot(t_plot, P.gradient(t_plot).dot(w), 'k-.')
    pl.show(block=False)
    pl.title('gradient')

    numerical_grad_2 = (numerical_grad[1:] - numerical_grad[:-1]) / dt
    pl.figure()
    pl.plot(t_plot[1:-1], np.squeeze(numerical_grad_2), 'm.')
    pl.plot(t_plot, P.gradient2(t_plot).dot(w), 'k-.')
    pl.title('gradient2')
    pl.show(block=False)


    pl.figure()
    numerical_integral = np.cumsum(X)*dt
    pl.plot(t_plot,numerical_integral, 'm.')
    pl.plot(t_plot, P.integral(t_plot).dot(w), 'k-.')

    print('Cmon, you sons of bitches! Do you want to live forever?')

