

class ImplementationMissingError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class AbstractFeature(object):

    def __init__(self):
        pass

    def evaluate(self, x):
        raise ImplementationMissingError("AbstractFeature: Evaluate function not implemented")

    def gradient(self, x):
        raise ImplementationMissingError("AbstractFeature: Gradient function not implemented")

    def gradient2(self, x):
        raise ImplementationMissingError("AbstractFeature: G2nd radient function not implemented")

    def integral(self, x):
        raise ImplementationMissingError("AbstractFeature: integral radient function not implemented")
