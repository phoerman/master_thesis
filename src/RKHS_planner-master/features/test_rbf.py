import numpy as np
import matplotlib.pyplot as pl

from features.rbf import RBF



r = RBF(0.5, 20)


pl.figure()

t = np.linspace(0.,1., 1000, endpoint=True)[np.newaxis, :]
dt = t[0,1]-t[0,0]
pl.plot(np.squeeze(t),np.squeeze(r.evaluate(t)),'r-',linewidth=2.)
pl.show(block=False)

pl.plot(np.squeeze(t),np.squeeze(r.gradient(t)),'b-',linewidth=2.)
pl.plot(np.squeeze(t),np.squeeze(r.gradient2(t)),'m-',linewidth=2.)

rr = np.squeeze(r.gradient(t))
rr = (rr[1:]-rr[:-1])/dt
pl.plot(np.squeeze(t)[:-1],rr,'k-',linewidth=2.)


pl.show(block=False)

print('End')