import numpy as np
from sklearn.kernel_approximation import RBFSampler
from sklearn.utils import check_array, check_random_state, as_float_array
from sklearn.utils.extmath import safe_sparse_dot
from sklearn.utils.validation import check_is_fitted


from features.AbstractFeature import AbstractFeature


class RFF(AbstractFeature):
    # Random Fourier Features of RBF - Rahimi and Recht

    def __init__(self, n_components=100, gamma=10, D=1):
        """

        :param n_components: number of Fourier compnenets
        :param D: dimensionality of the problem
        :param gamma: lengthscale
        """

        super(RFF, self).__init__()
        self.sampler = RBFSampler(gamma=gamma,
                                  n_components=n_components,
                                  )
        self.gamma = gamma

        self.sampler.fit(np.ones((1,D)))  # Generating the random features


    def evaluate(self, t):
        return self.sampler.transform(X=np.asarray(t).reshape(-1, 1))

    def gradient(self, t):

        """Gradient is based on Scipi kernel approximation evaluation (trasnfrom).
           Derivation is performed by applying the approximate feature map to X.

          Parameters
            ----------
          X : {array-like, sparse matrix}, shape (n_samples, n_features)
             New data, where n_samples in the number of samples
             and n_features is the number of features.

          Returns
          -------
          X_new : array-like, shape (n_samples, n_components)
        """

        check_is_fitted(self.sampler, 'random_weights_')

        t = check_array(np.asarray(t).reshape(-1, 1), accept_sparse='csr')
        projection = safe_sparse_dot(t, self.sampler.random_weights_)
        projection += self.sampler.random_offset_
        np.sin(projection, projection)
        projection = self.sampler.random_weights_ * projection
        projection *= -np.sqrt(2.) / np.sqrt(self.sampler.n_components)

        return projection

    def gradient2(self, t):
        """Gradient2 is based on Scipi kernel approximation evaluation (trasnfrom).
           Derivation is performed by applying the approximate feature map to X.

          Parameters
            ----------
          X : {array-like, sparse matrix}, shape (n_samples, n_features)
             New data, where n_samples in the number of samples
             and n_features is the number of features.

          Returns
          -------
          X_new : array-like, shape (n_samples, n_components)
        """

        check_is_fitted(self.sampler, 'random_weights_')

        t = check_array(np.asarray(t).reshape(-1, 1), accept_sparse='csr')
        projection = safe_sparse_dot(t, self.sampler.random_weights_)
        projection += self.sampler.random_offset_
        np.cos(projection, projection)
        projection = (self.sampler.random_weights_**2) * projection
        projection *= -np.sqrt(2.) / np.sqrt(self.sampler.n_components)

        return projection

    def integral(self, t):
        check_is_fitted(self.sampler, 'random_weights_')

        t = check_array(np.asarray(t).reshape(-1, 1), accept_sparse='csr')
        projection = safe_sparse_dot(t, self.sampler.random_weights_)
        projection += self.sampler.random_offset_
        np.sin(projection, projection)

        projection -= np.sin(self.sampler.random_offset_)
        projection /= self.sampler.random_weights_
        projection *= np.sqrt(2.) / np.sqrt(self.sampler.n_components)

        return projection


if __name__ == "__main__":

    import matplotlib.pyplot as pl

    n_components = 5

    P = RFF(n_components=n_components,
            gamma=10
            )

    w = np.ones((n_components,),
                dtype=float)

    dt = 0.001
    t_plot = np.arange(start=0.,
                       stop=1. + dt,
                       step=dt)

    X = P.evaluate(t_plot).dot(w)
    pl.figure()
    pl.plot(t_plot, np.squeeze(X),'k')
    pl.plot(t_plot, np.sum(P.sampler.transform(X=np.asarray(t_plot).reshape(-1, 1)), 1), 'b--')
    pl.show(block=False)

    numerical_grad = (X[1:] - X[:-1]) / dt
    pl.figure()
    pl.plot(t_plot[1:], np.squeeze(numerical_grad), 'm.')
    pl.plot(t_plot, P.gradient(t_plot).dot(w), 'k--')
    pl.title('gradient')
    pl.show(block=False)

    numerical_grad_2 = (numerical_grad[1:] - numerical_grad[:-1]) / dt
    pl.figure()
    pl.plot(t_plot[2:], np.squeeze(numerical_grad_2), 'm.')
    pl.plot(t_plot, P.gradient2(t_plot).dot(w), 'k--')
    pl.title('gradient2')
    pl.show(block=False)

    pl.figure()
    numerical_integral = np.cumsum(X) * dt
    pl.plot(t_plot, numerical_integral, 'm.')
    pl.plot(t_plot, P.integral(t_plot).dot(w), 'k-.')

    print('Cmon, you sons of bitches! Do you want to live forever?')

