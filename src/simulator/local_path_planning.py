# inspired by:
# https://atsushisakai.github.io/PythonRobotics/modules/mapping/lidar_to_grid_map_tutorial/lidar_to_grid_map_tutorial.html

import numpy as np
import matplotlib.pyplot as plt
from math import cos, sin, pi, dist, tan, acos, atan2

import src.PythonRobotics.Mapping.lidar_to_grid_map.lidar_to_grid_map as lg
from collections import deque
import random
import csv
from scipy.optimize import curve_fit, minimize

import cv2
import glob

import params
import time


def func(x, l1_func, l2_func, fei_func, m1_func, v1_func):
    t = (x - l1_func) / (l2_func - l1_func)
    h0 = 1 - 3 * t ** 2 + 2 * t ** 3
    h1 = 3 * t ** 2 - 2 * t ** 3
    h2 = t - 2 * t ** 2 + t ** 3
    h3 = - t ** 2 + t ** 3
    m0 = -(fei_func * (l1_func ** 3) / 3)
    v0 = -tan(fei_func * l1_func ** 2 / 2)

    cubichermite = h0 * m0 + h1 * m1_func + h2 * v0 * (l2_func - l1_func) + h3 * v1_func * (l2_func - l1_func)
    beam = -(fei_func * (3 * l1_func * x ** 2 - x ** 3) / 6)
    line = v1_func * x + m1_func - v1_func * l2_func

    if l1_func <= l2_func:

        arr = beam[np.where(x < l1_func)]
        arr = np.append(arr, cubichermite[np.where((x >= l1_func) & (x < l2_func))])
        arr = np.append(arr, line[np.where(x >= l2_func)])
    else:
        arr = beam[np.where(x < l2_func)]
        arr = np.append(arr, cubichermite[np.where((x >= l2_func) & (x < l1_func))])
        arr = np.append(arr, line[np.where(x >= l1_func)])

    return arr


def plot_map(my_map, index, safe, directory_plot):
    # Originally it would invert x and y-axis and start the y-axis from the top (good for images)
    # adjust colormap with: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    plt.imshow(my_map.T, origin='lower', cmap='pink', vmin=params.plot_min_val, vmax=params.plot_max_val)
    # plt.colorbar()
    if safe:
        for i in range(params.number_of_images):
            plt.savefig(directory_plot + str(index * params.number_of_images + i) + '.png', dpi=300)
    else:
        plt.show()


# fill a shape given the center point. Inspired by:
# https://atsushisakai.github.io/PythonRobotics/modules/mapping/lidar_to_grid_map_tutorial/lidar_to_grid_map_tutorial.html
def flood_fill(c_point, pmap, desired_value):
    # Fill empty areas with queue method
    sx, sy = pmap.shape
    fringe = deque()
    fringe.appendleft(c_point)
    while fringe:
        n = fringe.pop()
        nx, ny = n
        # West
        if nx > 0:
            if pmap[nx - 1, ny] != desired_value:
                pmap[nx - 1, ny] = desired_value
                fringe.appendleft((nx - 1, ny))
        # East
        if nx < sx - 1:
            if pmap[nx + 1, ny] != desired_value:
                pmap[nx + 1, ny] = desired_value
                fringe.appendleft((nx + 1, ny))
        # North
        if ny > 0:
            if pmap[nx, ny - 1] != desired_value:
                pmap[nx, ny - 1] = desired_value
                fringe.appendleft((nx, ny - 1))
        # South
        if ny < sy - 1:
            if pmap[nx, ny + 1] != desired_value:
                pmap[nx, ny + 1] = desired_value
                fringe.appendleft((nx, ny + 1))


def bresenham_circle(center_point, radius):
    # from
    # https://babavoss.pythonanywhere.com/python/bresenham-circle-algorithm-in-python#:~:text=This%20algorithm%20takes%20the%20center,the%20circle%20on%20a%20canvas.
    x = radius
    y = 0
    err = 0
    x0 = center_point[0]
    y0 = center_point[1]

    points = []

    while x >= y:
        points.append((x0 + x, y0 + y))
        points.append((x0 + y, y0 + x))
        points.append((x0 - y, y0 + x))
        points.append((x0 - x, y0 + y))
        points.append((x0 - x, y0 - y))
        points.append((x0 - y, y0 - x))
        points.append((x0 + y, y0 - x))
        points.append((x0 + x, y0 - y))

        y += 1
        err += 1 + 2 * y
        if 2 * (err - x) + 1 > 0:
            x -= 1
            err += 1 - 2 * x

    return np.array(points)


def quad_solver(a, b, c):
    sol1 = (-b + pow(pow(b, 2) - 4 * a * c, 0.5)) / (2 * a)
    sol2 = (-b - pow(pow(b, 2) - 4 * a * c, 0.5)) / (2 * a)

    return sol1, sol2


def transformation_coordinate_frames(angle, position, data):
    # https://atsushisakai.github.io/PythonRobotics/modules/mapping/lidar_to_grid_map_tutorial/lidar_to_grid_map_tutorial.html
    data = data.T
    coordinates = np.ones((len(data[0]), 2))

    rotation_mat = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
    translation_mat = position.reshape(len(position), 1)

    transformation_mat = np.eye(3)

    transformation_mat[np.ix_([0, 1], [0, 1])] = rotation_mat
    transformation_mat[np.ix_([0, 1], [2])] = translation_mat
    data_with_ones = np.array((data[0], data[1], np.ones(np.size(data[0]))))
    dot_product = np.dot(transformation_mat, data_with_ones)
    coordinates[:, 0] = np.delete(dot_product, 2, 0).T[:, 0]
    coordinates[:, 1] = np.delete(dot_product, 2, 0).T[:, 1]

    return coordinates


def move_center_point(center, ref, rad, boundary):
    if center[0] - ref[0] == 0:
        if boundary[1] - 1 > ref[0] + rad > 0:
            center[1] = ref[0] + rad
        else:
            center[1] = ref[0] - rad
    else:
        if dist(center, ref) < rad:
            m = (center[1] - ref[1]) / (center[0] - ref[0])
            n = ref[1] - m * ref[0]
            a = 1 + pow(m, 2)
            b = 2 * m * n - 2 * ref[0] - 2 * m * ref[1]
            c = pow(ref[0], 2) + pow(n, 2) - 2 * n * ref[1] + pow(ref[1], 2) - pow(rad, 2)

            e1, e2 = quad_solver(a, b, c)
            f1 = m * e1 + n
            f2 = m * e2 + n

            if boundary[0] - 1 > e1 > 0 and boundary[1] - 1 > f1 > 0:
                center[0] = e1
                center[1] = f1
            else:
                center[0] = e2
                center[1] = f2
    return center


# creates obstacles in different sizes and numbers and writes them to csv. Shape between 0 and 1, 0 is perfect circle,
# edges denotes the amount of edges per obstacle
def obstacle_creator(number, radius, shape, edges, start, end, clearance_radius, directory):
    boundary = params.grid_size
    radius = int(radius)
    alpha = 2 * pi / edges

    with open(directory, 'w') as f:

        for i in range(number):
            val_center = np.array([random.randint(0, boundary[0] - 1), random.randint(0, boundary[1] - 1)])
            val_center = move_center_point(val_center, start, clearance_radius + radius, boundary)
            val_center = move_center_point(val_center, end, clearance_radius + radius, boundary)

            if val_center[0] < 0:
                val_center[0] = 0
            if val_center[1] < 0:
                val_center[1] = 0
            if val_center[0] >= boundary[0]:
                val_center[0] = boundary[0] - 1
            if val_center[1] >= boundary[1]:
                val_center[1] = boundary[1] - 1

            for edge in range(edges):
                edge_coord = val_center + np.array(
                    [sin(edge * alpha) * radius + shape * random.randint(int(-radius / 2 + 2), int(radius / 2 - 2)),
                     cos(edge * alpha) * radius + shape * random.randint(int(-radius / 2), int(radius / 2))])

                if edge_coord[0] < 0:
                    edge_coord[0] = 0
                if edge_coord[1] < 0:
                    edge_coord[1] = 0
                if edge_coord[0] >= boundary[0]:
                    edge_coord[0] = boundary[0] - 1
                if edge_coord[1] >= boundary[1]:
                    edge_coord[1] = boundary[1] - 1

                writer = csv.writer(f)
                if i == 0 and edge == 0:
                    header = ['edge_x', 'edge_y', 'number', 'center_x', 'center_y']
                    writer.writerow(header)
                data = np.concatenate((edge_coord, np.array([i, val_center[0], val_center[1]])))
                writer.writerow(data)



def make_grid(file, number_obstacles):
    # read csv
    boundary = params.grid_size
    newmap = np.zeros(boundary)
    if number_obstacles == 0:
        return newmap
    edge_coordinates = np.genfromtxt(file, delimiter=",", skip_header=1).astype(int)

    # check points so that they are within range
    for i in range(len(edge_coordinates)):
        if edge_coordinates[i][0] >= boundary[0] or edge_coordinates[i][0] < 0:
            raise Exception("[make_grid] x value of edge ", i, " is out of bound")

        if edge_coordinates[i][1] >= boundary[1] or edge_coordinates[i][1] < 0:
            raise Exception("[make_grid] y value of edge ", i, " is out of bound")

    # make map and separate obstacles
    helper_map = newmap
    current_edge = edge_coordinates[0][2]
    first_point = edge_coordinates[0][0:2]
    center_point = np.zeros((edge_coordinates[len(edge_coordinates) - 1][2] + 1, 2))
    center_point[0] = edge_coordinates[0][3:5]

    for i in range(len(edge_coordinates)):
        if i == len(edge_coordinates) - 1 or edge_coordinates[i + 1][2] != current_edge:
            # close obstacle and fill the center
            line = lg.bresenham(edge_coordinates[i][0:2], first_point)
            if i != len(edge_coordinates) - 1:
                first_point = edge_coordinates[i + 1][0:2]
                current_edge = edge_coordinates[i + 1][2]
                center_point[edge_coordinates[i + 1][2]] = edge_coordinates[i + 1][3:5]
            for l1 in line:
                helper_map[l1[0]][l1[1]] = 1
            flood_fill((center_point[edge_coordinates[i][2]].astype(int)), helper_map, 1)  # fill center of obstacle
            newmap = np.logical_or(helper_map, newmap).astype(float)
            helper_map = np.zeros(boundary)

        else:
            line = lg.bresenham(edge_coordinates[i][0:2], edge_coordinates[i + 1][0:2])
            for l2 in line:
                helper_map[l2[0]][l2[1]] = 1
        newmap = newmap * params.value_obstacles

    # mark goal point
    draw_filled_circle(newmap, params.robot_endpoint, 8, params.value_goal_point)
    occupancy_counter = 0
    for i in range(len(newmap)):
        for j in range(len(newmap[0])):
            if newmap[i][j] == params.value_obstacles:
                occupancy_counter = occupancy_counter + 1
    number_points_in_map = len(newmap) * len(newmap[0])
    return newmap, occupancy_counter / number_points_in_map


def depth_read(current_position, current_rotation, my_map):
    max_angle = params.angle_depth_sensor
    max_reach = params.max_reach_depth_sensor
    min_reach = params.min_reach_depth_sensor
    resolution = params.number_of_rays

    # make measurements that tof would make, max_reach + 5 = nothing detected within range
    sensor_measurement = np.zeros((resolution, 2))
    for i in range(resolution):
        current_angle = i * max_angle / (resolution - 1)
        gamma = current_rotation - max_angle / 2 + current_angle
        endpoint_ray = np.array([current_position[0] + cos(gamma) * max_reach,
                                 current_position[1] + sin(gamma) * max_reach]).astype(int)

        line = lg.bresenham(current_position, endpoint_ray)
        has_obstacle = False
        remember = current_position
        for lin in line:
            # check borders
            if lin[0] > len(my_map) - 1 or lin[1] > len(my_map[0]) - 1 or lin[0] < 0 or lin[1] < 0:
                endpoint_ray = remember
                break
            remember = lin
            # register depth_read
            if my_map[lin[0]][lin[1]] == 1.0 and dist(current_position, lin) > min_reach:
                has_obstacle = True
                sensor_measurement[i] = [current_angle, dist(current_position, lin)]  # This conversion can cause errors
                endpoint_ray = lin
                break
            my_map[lin[0]][lin[1]] = params.value_lidar_ray
        if not has_obstacle:
            sensor_measurement[i] = [current_angle, max_reach + 5]
        my_map[endpoint_ray[0]][endpoint_ray[1]] = params.value_lidar_end_ray

    return sensor_measurement


def depth_map_creator(tof_measurements_input, my_map, current_position, current_angle):
    # first make readings cartesian

    def polar_to_cartesian(tof_measurements, current_position_cart, current_angle_cart):
        max_angle = params.angle_depth_sensor
        max_reach = params.max_reach_depth_sensor
        grid = params.grid_size
        # converts polar coordinates to cartesian coordinates in world frame
        # inspired by: https://github.com/balzer82/3D-OccupancyGrid-Python/blob/master/3D-Occupancy-Grid-ibeo-Lux.ipynb

        # check if a measurement is past max_reach
        coordinates_cartesian = np.ones((len(tof_measurements), 3))
        for ii in range(len(tof_measurements)):
            if tof_measurements[ii][1] > max_reach:
                tof_measurements[ii][1] = max_reach
                coordinates_cartesian[ii][2] = 0

        # from polar to cartesian
        coordinates_xy = np.vstack((np.multiply(tof_measurements[:, 1], np.cos(tof_measurements[:, 0])),
                                    np.multiply(tof_measurements[:, 1], np.sin(tof_measurements[:, 0]))))

        data_transformed = transformation_coordinate_frames(current_angle_cart - max_angle / 2, current_position_cart,
                                                            coordinates_xy.T)

        # fill matrix with transformed data
        coordinates_cartesian[:, 0] = data_transformed[:, 0]
        coordinates_cartesian[:, 1] = data_transformed[:, 1]

        # regulate what happens at edge of grid
        for ii in range(len(coordinates_cartesian)):
            if (coordinates_cartesian[ii][0] > grid[0] - 1 or coordinates_cartesian[ii][0] < 0 or
                    coordinates_cartesian[ii][1] > grid[1] - 1 or coordinates_cartesian[ii][1] < 0):
                if params.map_edges_are_obstacles:
                    coordinates_cartesian[ii][2] = 1
                else:
                    coordinates_cartesian[ii][2] = 0

                lines = lg.bresenham(current_position_cart, coordinates_cartesian[ii][0:2].astype(int))
                remember_point = current_position_cart
                for line_polar in lines:
                    if line_polar[0] > grid[0] - 1 or line_polar[0] < 0 or line_polar[1] > grid[1] - 1 or \
                            line_polar[1] < 0:
                        coordinates_cartesian[ii][0:2] = remember_point
                        break
                    remember_point = line_polar

        return coordinates_cartesian.astype(int)

    measurement_points = polar_to_cartesian(tof_measurements_input, current_position, current_angle)

    for i in range(len(measurement_points)):
        line = lg.bresenham(current_position, measurement_points[i][0:2])
        for li in line:
            if my_map[li[0]][li[1]] != params.value_robot:
                my_map[li[0]][li[1]] = params.value_empty
        if measurement_points[i][2] == 1:
            my_map[measurement_points[i][0]][measurement_points[i][1]] = params.value_occupied

    return my_map, measurement_points


def draw_filled_circle(my_map, center_point, radius, color):
    points1 = bresenham_circle(center_point, radius)
    points2 = bresenham_circle(center_point, radius - 1)
    for point in points1:
        point = point.astype(int)
        if 0 > point[0]:
            point[0] = 0.0
        if my_map.shape[0] <= point[0]:
            point[0] = my_map.shape[0] - 1
        if 0 > point[1]:
            point[1] = 0.0
        if my_map.shape[1] <= point[1]:
            point[1] = my_map.shape[1] - 1

        my_map[point[0]][point[1]] = color

    for point in points2:
        point = point.astype(int)
        if 0 < point[0] < my_map.shape[0] and 0 < point[1] < my_map.shape[1]:
            my_map[point[0]][point[1]] = color

    flood_fill(center_point, my_map, color)
    return my_map


def reconstruct_robot(head_location, head_rotation, start_point, start_rotation, my_map, draw_until=0):
    if head_rotation == start_rotation:
        head_rotation = head_rotation + 1.0 * pi / 180.0
    head_length = params.length_head
    actuator_length = params.length_actuator
    robot_thick = params.robot_thickness
    color_robot = params.value_robot
    # go from real to robot coordinate frame (x alongside base of robot), then change to world frame for end
    data_to_transform = np.array(
        [start_point, head_location])

    # convert translation vector to rotated frame

    new_translation = np.array([-(sin(start_rotation) * start_point[1] + cos(start_rotation) * start_point[0]),
                                -(cos(start_rotation) * start_point[1] - sin(start_rotation) * start_point[0])])

    data_robot_frame = transformation_coordinate_frames(-start_rotation, new_translation, data_to_transform)
    x_robot_frame = np.arange(0.0, data_robot_frame[1][0], 1.0)

    head_rotation_robot_frame = head_rotation - start_rotation

    # Determine parameters
    v1 = tan(head_rotation_robot_frame)
    fei = 1.3732572356845278e-08  # Average value

    if data_robot_frame[1][1] > data_robot_frame[0][1]:
        fei = -fei
    b_lin = data_robot_frame[1][1] - v1 * data_robot_frame[1][0]
    a = pow(v1, 2) + 1
    b = 2 * v1 * b_lin - 2 * data_robot_frame[1][0] - 2 * data_robot_frame[1][1] * v1
    c = pow(b_lin, 2) - pow(head_length, 2) + pow(data_robot_frame[1][0], 2) + pow(data_robot_frame[1][1], 2) - 2 * \
        data_robot_frame[1][1] * b_lin

    l2 = min(quad_solver(a, b, c))
    m1 = v1 * l2 + b_lin

    # calculate l1

    def calculate_arc_length(l1_arc_length, l2_arc_length, fei_arc_length, m1_arc_length, v1_arc_length):
        m0_arc_length = fei_arc_length * pow(l1_arc_length, 3) / 3
        tan_arc_length = tan(fei_arc_length * pow(l1_arc_length, 2) / 2)
        xm_arc_length = (m1_arc_length + 1 / v1_arc_length * l2_arc_length - m0_arc_length + l1_arc_length /
                         tan_arc_length) / (1 / tan_arc_length + 1 / v1_arc_length)
        ym_arc_length = (m0_arc_length - m1_arc_length - 1 / v1_arc_length * l2_arc_length - l1_arc_length /
                         tan_arc_length) / (v1_arc_length / tan_arc_length + 1)
        r_arc_length = pow((pow((l2_arc_length - xm_arc_length), 2) + pow((m1_arc_length - ym_arc_length), 2)), 0.5)

        if abs((1 - 1 / v1_arc_length * 1 / tan_arc_length) / pow(r_arc_length, 2)) <= 1.0:

            alpha_arc_length = acos((1 - 1 / v1_arc_length * 1 / tan_arc_length) / pow(r_arc_length, 2))

            return alpha_arc_length * r_arc_length
        else:
            return 10000000

    def minimize_length_diff(l1_minimize):
        return abs(actuator_length - calculate_arc_length(l1_minimize, l2, fei, m1, v1))

    l1 = minimize(minimize_length_diff, data_robot_frame[1][0] - head_length - actuator_length, method='Powell')
    param = np.array([l1.x[0], l2, fei, m1, v1])

    # calculate function
    y_robot_frame = func(x_robot_frame, *param)

    robot_robot_frame = np.array([x_robot_frame, y_robot_frame]).T

    # draw robot with given thickness
    data_world_frame = transformation_coordinate_frames(start_rotation, start_point, robot_robot_frame)
    safe_data = start_point.astype(int)
    helper_map = np.zeros(my_map.shape)
    robot_line = []
    counter = 0
    for data in data_world_frame:
        if not (int(data[0]) == start_point[0] and int(data[1]) == start_point[1]):

            data = data.astype(int)
            if data[0] < len(my_map[0] - robot_thick / 2) and data[1] < len(my_map[1] - robot_thick / 2):

                line = lg.bresenham(safe_data, data)
                for li in line:
                    if counter >= draw_until + 1 and draw_until != 0:
                        break
                    else:
                        robot_line.append(li)
                        my_map[li[0]][li[1]] = color_robot

                        if counter % params.number_of_robot_circles == 0:
                            # in order to get a continuous line, a circle with a slightly smaller radius is needed
                            helper_map = draw_filled_circle(helper_map, li, robot_thick / 2, color_robot)
                            for i in range(len(my_map)):
                                for j in range(len(my_map[0])):
                                    # new_value = my_map[i][j] + helper_map[i][j]
                                    if helper_map[i][j] > 0:
                                        # if new_value > color_robot:
                                        #     new_value = color_robot
                                        my_map[i][j] = color_robot
                                    # if my_map[i][j] > color_robot:
                                    #     my_map[i][j] = color_robot
                            # my_map = my_map + helper_map
                            helper_map = np.zeros(my_map.shape)
                    counter = counter + 1

            else:
                print(data)
            safe_data = data
    # for i in range(len(my_map)):
    #     for j in range(len(my_map[0])):
    #         if my_map[i][j] > color_robot:
    #             my_map[i][j] = color_robot
    return my_map, param, np.array(robot_line)


def planner(my_map, base_position, base_rotation, new_length, tof_points, old_head_rotation):
    goal_point = params.robot_endpoint
    robot_map = np.copy(my_map)
    # Go into direction of Goal. If obstacles detected: Go into direction of the lowest density until no obstacle
    # detected or way to goal is free
    # Or: detect passage with the lowest cost depending on direction to goal and rate of change to current position

    # determine possible waypoints for robot
    new_straight_endpoint = base_position + np.array([new_length * cos(base_rotation), new_length * sin(base_rotation)])
    head_length_center_direction = np.array([cos(base_rotation) * params.length_head,
                                             sin(base_rotation) * params.length_head])
    actuator_length_center_direction = np.array([cos(base_rotation) * params.length_actuator,
                                                 sin(base_rotation) * params.length_actuator])
    new_arc_center = new_straight_endpoint - head_length_center_direction - actuator_length_center_direction
    radius = params.length_head + params.length_actuator

    possible_next = np.zeros((params.number_of_waypoints, 2))
    increment = 2 * params.max_angle / (params.number_of_waypoints - 1)

    rang = params.number_of_waypoints
    i = 0
    while i < rang:
        possible_next[i][0] = new_arc_center[0] + radius * cos(base_rotation - params.max_angle + i * increment)
        possible_next[i][1] = new_arc_center[1] + radius * sin(base_rotation - params.max_angle + i * increment)

        if (possible_next[i][0] >= len(my_map) - params.robot_thickness / 2 or
                possible_next[i][1] >= len(my_map[0]) - params.robot_thickness / 2 - 10):
            possible_next = np.delete(possible_next, i, 0)
            rang = rang - 1
            i = i - 1
        i = i + 1
    weights = np.zeros(len(possible_next))
    for i in range(len(possible_next)):

        if params.plot_waypoints:
            robot_map[possible_next[i][0].astype(int)][
                possible_next[i][1].astype(int)] = params.value_possible_waypoints

        # find weights for direction in robot frame according to goal, obstacles, param difference

        # Goal
        weight_goal_direction = (dist(possible_next[i], goal_point) - dist(new_arc_center, goal_point)) * 10 / radius
        # weight_goal_direction = 0

        # obstacle occupancy
        smallest_distance = 100000
        angle_to_center = atan2(possible_next[i][1] - new_arc_center[1], possible_next[i][0] - new_arc_center[0])
        once = False
        for j in range(len(tof_points)):
            angle_to_point = atan2(tof_points[j][1] - possible_next[i][1], tof_points[j][0] - possible_next[i][0])
            angle_diff = angle_to_point - angle_to_center

            if -15.0 * pi / 180.0 < angle_diff < 15.0 * pi / 180.0:
                once = True
                distance = dist(possible_next[i], np.array([tof_points[j][0], tof_points[j][1]]))
                if distance < smallest_distance:
                    smallest_distance = distance
        weight_obstacle = -2 / 1000 * smallest_distance + 2
        if not once:
            weight_obstacle = 25
        # weight_obstacle = 0

        # param difference

        # add all up
        weights[i] = weight_obstacle + weight_goal_direction
    best_point = np.argmin(weights)
    new_robot_position = possible_next[best_point]
    # Obstacle rejection

    # draw new robot onto map
    rotation_front = atan2(new_robot_position[1] - new_arc_center[1],
                           new_robot_position[0] - new_arc_center[0])

    robot_map, param, line = reconstruct_robot(new_robot_position, rotation_front, base_position, base_rotation,
                                               robot_map)

    # what to do if robot intersects with obstacle
    check_collision = True
    counter = 0
    has_new_position = False
    points_intersection_memory = []
    direction_correction = True
    counter_direction_change = 0
    # First, check for collision and move head in small increments
    while check_collision and counter <= params.iterations_collision_check:
        points_intersection = []
        robot_in_obstacle = False
        for i in range(len(my_map)):
            for j in range(len(my_map[0])):
                if robot_map[i][j] == params.value_robot and my_map[i][j] == params.value_obstacles:
                    points_intersection.append([i, j])
        if counter < 10:
            if len(points_intersection) > 40:
                robot_in_obstacle = True
        else:
            if len(points_intersection) > 160:
                robot_in_obstacle = True
        if robot_in_obstacle:
            if counter == 0:
                direction_correction = old_head_rotation >= base_rotation
            else:
                if len(points_intersection) > len(points_intersection_memory):
                    if counter_direction_change == 2 and counter < 20:
                        print("change direction")
                        counter_direction_change = 0
                        direction_correction = not direction_correction
                        counter = counter - 2
                    else:
                        counter_direction_change = counter_direction_change + 1
            has_new_position = True
            print("in obstacle")
            increment = params.increment_collision_avoidance
            if direction_correction:
                rotation_front = rotation_front + increment
            else:
                rotation_front = rotation_front - increment
            new_robot_position[0] = new_arc_center[0] + radius * cos(rotation_front)
            new_robot_position[1] = new_arc_center[1] + radius * sin(rotation_front)
            robot_map = np.copy(my_map)
            robot_map, param, line = reconstruct_robot(new_robot_position, rotation_front, base_position, base_rotation,
                                                       robot_map)
            points_intersection_memory = points_intersection
        else:
            check_collision = False

        counter = counter + 1
    collision = False
    points_intersection_memory = np.array(points_intersection_memory)

    if counter > params.iterations_collision_check:
        print("most likely in a local minima, stop simulation")
        collision = True

    # If a position without collision is found, change start point
    if has_new_position and not collision:
        old_base_position = base_position
        old_base_rotation = base_rotation
        print("Change base position and rotation")
        # search for closest point of robot to obstacle, take it as start point and position
        minimal_distance = 100000
        point_minimal_dist = 0
        for i in range(len(line)):
            for point in points_intersection_memory:
                if dist(line[i], point) < minimal_distance:
                    minimal_distance = dist(line[i], point)
                    point_minimal_dist = i
        if point_minimal_dist < 100:
            point_minimal_dist = 100
        base_position = line[point_minimal_dist].astype(int)
        if len(line) - 1 < point_minimal_dist + 10:
            base_rotation = atan2(line[len(line) - 1][1] - line[point_minimal_dist][1],
                                  line[len(line) - 1][0] - line[point_minimal_dist][0])
        else:
            base_rotation = atan2(line[point_minimal_dist + 10][1] - line[point_minimal_dist][1],
                                  line[point_minimal_dist + 10][0] - line[point_minimal_dist][0])

        new_length = 150
        new_my_map = np.copy(my_map)
        new_my_map, param, line = reconstruct_robot(new_robot_position, rotation_front, old_base_position,
                                                    old_base_rotation,
                                                    new_my_map, point_minimal_dist)
        my_map = np.copy(new_my_map)

    return robot_map, new_robot_position.astype(
        int), rotation_front, possible_next, collision, my_map, base_position, base_rotation, new_length


def main():
    for i in range(38, 50):
        start_time = time.perf_counter()
        # init
        directory = '/home/patricia/workspaces/master_thesis/src/simulator/experiment_obstacle_number/iteration_' + str(
            i) + '/coordinates_edges.csv'
        directory_plot = '/home/patricia/workspaces/master_thesis/src/simulator/experiment_obstacle_number/iteration_' + str(
            i) + '/'
        # directory = '/home/patricia/workspaces/master_thesis/src/simulator/good_examples/example_0/coordinates_edges.csv'
        # directory_plot = '/home/patricia/workspaces/master_thesis/src/simulator/good_examples/example_0/'
        obstacle_creator(params.number_of_obstacles, params.radius_obstacles, params.shape_distortion,
                         params.number_of_edges, params.robot_startpoint, params.robot_endpoint, params.clearance_rad,
                         directory)
        map_with_obstacles, occupancy = make_grid(directory, params.number_of_obstacles)

        # loop
        finish = False
        forward_movement_per_frame = params.forward_movement_per_second / params.fps
        new_length = dist(params.robot_startpoint, params.head_first_location) + forward_movement_per_frame
        head_location = params.head_first_location
        head_rotation = params.head_first_rotation
        base_position = params.robot_startpoint
        base_rotation = params.robot_start_rotation
        counter = 0
        map_robot = np.copy(map_with_obstacles)
        map_planner, param, line = reconstruct_robot(head_location, head_rotation, base_position,
                                                     base_rotation, map_robot)
        reached_goal = False
        while not finish:
            print("iteration number: ", counter)
            map_depth_read = np.copy(map_with_obstacles)
            map_depth = np.copy(map_planner)
            map_robot = np.copy(map_with_obstacles)
            # depth_map = np.ones(params.grid_size) * params.value_unknown  # first we have no clue, hens value of 0.5
            # measure
            # Measurement: angle(lower to higher), distance
            depth_reading = depth_read(head_location, head_rotation, map_depth_read)

            depth_map, measure_points = depth_map_creator(depth_reading, map_depth, head_location,
                                                          head_rotation)

            plot_map(depth_map, counter, True, directory_plot)

            map_planner, head_location, head_rotation, \
                next_points_array, collision, map_with_obstacles, base_position, base_rotation, new_length = planner(
                map_robot, base_position, base_rotation, new_length, measure_points, head_rotation)
            new_length = new_length + forward_movement_per_frame
            reason = 0
            if dist(head_location, params.robot_endpoint) < params.close_enough:
                print("goal reached!")
                reason = 0
                finish = True
                reached_goal = True
            if counter > params.max_iterations:
                print("Max iteration reached.")
                reason = 1
                print("Failed to get to reach goal.")
                finish = True
            if len(next_points_array) < params.number_of_waypoints / 2:
                print("Unlikely to converge to goal because of lack of manoeuvrability (number of waypoints).")
                reason = 2
                print("Failed to reach goal.")
                finish = True
            if collision:
                print("Collision.")
                reason = 3
                finish = True
            counter = counter + 1
        # Plot final image
        if reached_goal:
            map_depth_read = np.copy(map_with_obstacles)
            map_depth = np.copy(map_planner)
            depth_reading = depth_read(head_location, head_rotation, map_depth_read)

            depth_map, measure_points = depth_map_creator(depth_reading, map_depth, head_location,
                                                          head_rotation)
            plot_map(depth_map, counter, True, directory_plot)
        end_time = time.perf_counter()
        elapsed_time = end_time - start_time
        with open('/home/patricia/workspaces/master_thesis/src/simulator/experiment_obstacle_number/iteration_' + str(
                i) + '/evaluation.csv', 'w') as f:
        # with open('/home/patricia/workspaces/master_thesis/src/simulator/good_examples/example_0/evaluation.csv', 'w') as f:
            writer = csv.writer(f)
            header = ['goal reached', 'ending reason', 'distance to goal', 'number of steps', 'obstacle density', 'computation time', 'number of obstacles']
            writer.writerow(header)
            data = [reached_goal, reason, dist(params.robot_endpoint, head_location), counter / params.fps - 1, occupancy, elapsed_time, number_obstacles]
            writer.writerow(data)


if __name__ == "__main__":
    main()
