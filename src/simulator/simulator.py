import numpy as np
import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegWriter
from matplotlib.patches import Rectangle
from matplotlib.patches import Polygon
import csv
import os
import pandas as pd
import math

# Code borrowed from: https://github.com/codinglikemad/pyAnimTutorial
df=pd.read_csv("/home/patricia/workspaces/master_thesis/src/trajectory_approximator/outputs/2023_03_09-10_52_21-1fps/params.csv")

alll1 = df["non lin least square param l1"].values
alll2 = df["non lin least square param l2"].values
allfei = df["non lin least square param fei"].values
allm1 = df["non lin least square param m1"].values
allv1 = df["non lin least square param v1"].values
allxmax =df["x max"].values


metadata = dict(title='RoBoa simulation', artist='Patricia Hoermann')
writer = FFMpegWriter(fps=4, metadata=metadata)

fig, ax = plt.subplots()



def func(x, l1_func, l2_func, fei_func, m1_func, v1_func):
    # if (l1_func > l2_func):
    #     l2_func = l1_func + 1

    t = (x - l1_func) / (l2_func - l1_func)
    h0 = 1 - 3 * t ** 2 + 2 * t ** 3
    h1 = 3 * t ** 2 - 2 * t ** 3
    h2 = t - 2 * t ** 2 + t ** 3
    h3 = - t ** 2 + t ** 3
    m0 = -(fei_func * (l1_func ** 3) / 3)
    v0 = -math.tan(fei_func * l1_func ** 2 / 2)

    cubichermite = h0 * m0 + h1 * m1_func + h2 * v0 * (l2_func - l1_func) + h3 * v1_func * (l2_func - l1_func)
    beam = -(fei_func * (3 * l1_func * x ** 2 - x ** 3) / 6)
    line = v1_func * x + m1_func - v1_func * l2_func
    if (l1_func <= l2_func):

        arr = beam[np.where(x < l1_func)]
        arr = np.append(arr, cubichermite[np.where((x >= l1_func) & (x < l2_func))])
        arr = np.append(arr, line[np.where(x >= l2_func)])
    else:
        arr = beam[np.where(x < l2_func)]
        arr = np.append(arr, cubichermite[np.where((x >= l2_func) & (x < l1_func))])
        arr = np.append(arr, line[np.where(x >= l1_func)])


    return arr



with writer.saving(fig, "videos/2023_03_09-10_52_21-1fps.mp4", 200): # define dpi
    for tval in range(133): # start t, stop t, Number of frames
        xvec = np.linspace(0, allxmax[tval], 1000, endpoint=True)
        zval = func(xvec, alll1[tval], alll2[tval], allfei[tval], allm1[tval], allv1[tval])

        ax.set_xlim([0, 978])
        ax.set_ylim([-413, 912-413])
        ax.plot(xvec, zval, lw=30, solid_capstyle='round')
        #add rectangle to plot
        ax.add_patch(Polygon([[(978-189), (656-413)],[(978-181), (656-413)],[(978-175), (292-413)],[(978-183), (292-413)]], closed = True,
                             edgecolor = 'blue', facecolor = 'blue', fill=True, lw=1))

        writer.grab_frame()
        plt.cla()
