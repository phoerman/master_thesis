import numpy as np
from math import pi
# define params

# grid params
grid_size = np.array([978, 912])  # equal to a field of 207 * 193 cm --> 1 cm = 4.725 pix
conversion_factor = 4.725  # 1 cm = conversion_factor pixel
# Colors
value_empty = -1.0
value_occupied = 2.0
value_unknown = 0.5
value_lidar_ray = 0.3
value_lidar_end_ray = 0.8
value_robot = 3.0
value_obstacles = 1.0
value_goal_point = 5.0
value_possible_waypoints = 5.0

plot_min_val = -1.0
plot_max_val = 5.0

number_of_robot_circles = 80  # determines how smooth the robot is drawn, the smaller the smoother

# obstacle params
number_of_obstacles = 3
radius_obstacles = 20.0 * conversion_factor
shape_distortion = 0.5
number_of_edges = 10
clearance_rad = 50.0 * conversion_factor  # radius with no obstacles

map_edges_are_obstacles = True  # TODO: make this default?

# goal point
robot_endpoint = np.array([(grid_size[0] - 1), (grid_size[1] - 1)/2]).astype(int)

# depth sensor parameters
angle_depth_sensor = 70.0 * pi / 180.0
min_reach_depth_sensor = 20.0 * conversion_factor
max_reach_depth_sensor = 400.0 * conversion_factor
number_of_rays = 30

# robot state: is defined by translation and rotation vector
robot_startpoint = np.array([0, (grid_size[1] - 1) / 2]).astype(int)
robot_start_rotation = 0.0 * pi / 180.0  # yaw rotation, rotation around z axis, in rad
head_first_location = np.array([110, robot_startpoint[1]])  # TODO: remove
head_first_rotation = 1.0 * pi / 180.0

# robot parameters
robot_thickness = 10.0 * conversion_factor
length_head = 20 * conversion_factor
length_actuator = 30 * conversion_factor
forward_movement_per_second = 30.0 * conversion_factor
max_angle = 90.0 * pi / 180.0

# simulation params
fps = 1.0
number_of_waypoints = 17
close_enough = 200
max_iterations = 14
iterations_collision_check = 40
increment_collision_avoidance = 2.0 * pi / 180.0
plot_waypoints = True

# video parameters
number_of_images = 1
# afterwards in terminal: ffmpeg -r 30 -i %d.png -c:v libx264 -pix_fmt yuv420p output_example.mp4
# -r = fps, somehow has to be 30 for ios
