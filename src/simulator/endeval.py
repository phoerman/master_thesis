import csv
import pandas as pd
import numpy as np

def main():
    # read in data
    goal_reached = []
    ending_reason = []
    distance_to_goal = []
    number_of_steps = []
    obstacle_density = []

    # optional
    computation_time = []
    number_of_obstacles = []

    for i in range(50):

        df = pd.read_csv('/home/patricia/workspaces/master_thesis/src/simulator/videos_simulation/iteration_' +
                         str(i) + '/evaluation.csv')

        goal_reached.append(df["goal reached"].values[0])
        ending_reason.append(df["ending reason"].values[0])
        distance_to_goal.append(df["distance to goal"].values[0])
        number_of_steps.append(df["number of steps"].values[0])
        obstacle_density.append(df["obstacle density"].values[0])

        # optional
        computation_time.append(df["computation time"].values[0])
        number_of_obstacles.append(df["number of obstacles"].values[0])

    goal_reached = np.array(goal_reached)
    ending_reason = np.array(ending_reason)
    distance_to_goal = np.array(distance_to_goal)
    number_of_steps = np.array(number_of_steps)
    obstacle_density = np.array(obstacle_density)

    # optional
    computation_time = np.array(computation_time)
    number_of_obstacles = np.array(number_of_obstacles)

    # goal reached
    number_of_reached = goal_reached.sum()
    print("Percentage goal reached: ", number_of_reached / 50 * 100)

    # determine which indexes reached the goal or not
    indexes_goal_reached = []
    indexes_goal_not_reached = []
    for i in range(50):
        if goal_reached[i]:
            indexes_goal_reached.append(i)
        else:
            indexes_goal_not_reached.append(i)

    indexes_goal_reached = np.array(indexes_goal_reached)
    indexes_goal_not_reached = np.array(indexes_goal_not_reached)

    # ending reason
    for i in range(len(indexes_goal_reached)):
        ending_reason[indexes_goal_reached[i]] = 0

    counter_0 = 0
    counter_1 = 0
    counter_2 = 0
    counter_3 = 0
    for i in range(50):
        if ending_reason[i] == 0:
            counter_0 = counter_0 + 1
        if ending_reason[i] == 1:
            counter_1 = counter_1 + 1
        if ending_reason[i] == 2:
            counter_2 = counter_2 + 1
        if ending_reason[i] == 3:
            counter_3 = counter_3 + 1

    print("Percentage terminating reason: Goal reached: ", counter_0 / 50 * 100)
    print("Percentage terminating reason: Max iteration reached: ", counter_1 / 50 * 100)
    print("Percentage terminating reason: Unlikely to converge to goal because of lack of manoeuvrability: ", counter_2 / 50 * 100)
    print("Percentage terminating reason: Collision: ", counter_3 / 50 * 100)

    # distance to goal
    print("Average distance to goal: ", np.average(distance_to_goal))

    distance_goal_not_reached = []
    for i in range(len(indexes_goal_not_reached)):
        distance_goal_not_reached.append(distance_to_goal[indexes_goal_not_reached[i]])
    distance_goal_not_reached = np.array(distance_goal_not_reached)

    distance_goal_reached = []
    for i in range(len(indexes_goal_reached)):
        distance_goal_not_reached.append(distance_to_goal[indexes_goal_reached[i]])
    distance_goal_reached = np.array(distance_goal_reached)

    print("Average distance to goal, goal not reached: ", np.average(distance_goal_not_reached))
    print("Average distance to goal, goal reached: ", np.average(distance_goal_reached))

    # number of steps
    print("Average number of steps: ", np.average(number_of_steps))

    number_of_steps_goal_not_reached = []
    for i in range(len(indexes_goal_not_reached)):
        number_of_steps_goal_not_reached.append(number_of_steps[indexes_goal_not_reached[i]])
    number_of_steps_goal_not_reached = np.array(number_of_steps_goal_not_reached)

    number_of_steps_goal_reached = []
    for i in range(len(indexes_goal_reached)):
        number_of_steps_goal_reached.append(number_of_steps[indexes_goal_reached[i]])
    number_of_steps_goal_reached = np.array(number_of_steps_goal_reached)

    print("Average number of steps, goal not reached: ", np.average(number_of_steps_goal_not_reached))
    print("Average number of steps, goal reached: ", np.average(number_of_steps_goal_reached))

    # obstacle density
    print("Average obstacle density: ", np.average(obstacle_density))

    obstacle_density_goal_not_reached = []
    for i in range(len(indexes_goal_not_reached)):
        obstacle_density_goal_not_reached.append(obstacle_density[indexes_goal_not_reached[i]])
    obstacle_density_goal_not_reached = np.array(obstacle_density_goal_not_reached)

    obstacle_density_goal_reached = []
    for i in range(len(indexes_goal_reached)):
        obstacle_density_goal_reached.append(obstacle_density[indexes_goal_reached[i]])
    obstacle_density_goal_reached = np.array(obstacle_density_goal_reached)

    print("Average obstacle density, goal not reached: ", np.average(obstacle_density_goal_not_reached))
    print("Average obstacle density, goal reached: ", np.average(obstacle_density_goal_reached))

    #optional
    # average computation time
    print("Average computation time", np.average(computation_time))

    computation_time_goal_not_reached = []
    for i in range(len(indexes_goal_not_reached)):
        computation_time_goal_not_reached.append(computation_time[indexes_goal_not_reached[i]])
    computation_time_goal_not_reached = np.array(computation_time_goal_not_reached)

    computation_time_goal_reached = []
    for i in range(len(indexes_goal_reached)):
        computation_time_goal_reached.append(computation_time[indexes_goal_reached[i]])
    computation_time_goal_reached = np.array(computation_time_goal_reached)

    print("Average computation time, goal not reached: ", np.average(computation_time_goal_not_reached))
    print("Average computation time, goal reached: ", np.average(computation_time_goal_reached))

    # number of obstacles
    print("Average number of obstacles", np.average(number_of_obstacles))

    number_of_obstacles_goal_not_reached = []
    for i in range(len(indexes_goal_not_reached)):
        number_of_obstacles_goal_not_reached.append(number_of_obstacles[indexes_goal_not_reached[i]])
    number_of_obstacles_goal_not_reached = np.array(number_of_obstacles_goal_not_reached)

    number_of_obstacles_goal_reached = []
    for i in range(len(indexes_goal_reached)):
        number_of_obstacles_goal_reached.append(number_of_obstacles[indexes_goal_reached[i]])
    number_of_obstacles_goal_reached = np.array(number_of_obstacles_goal_reached)

    print("Average number of obstacles, goal not reached: ", np.average(number_of_obstacles_goal_not_reached))
    print("Average number of obstacles, goal reached: ", np.average(number_of_obstacles_goal_reached))

    # Evaluation according to number of obstacles
    indexes_3_obstacles = []
    indexes_4_obstacles = []
    indexes_5_obstacles = []
    indexes_6_obstacles = []
    indexes_7_obstacles = []
    for i in range(50):
        if number_of_obstacles[i] == 3:
            indexes_3_obstacles.append(i)
        if number_of_obstacles[i] == 4:
            indexes_4_obstacles.append(i)
        if number_of_obstacles[i] == 5:
            indexes_5_obstacles.append(i)
        if number_of_obstacles[i] == 6:
            indexes_6_obstacles.append(i)
        if number_of_obstacles[i] == 7:
            indexes_7_obstacles.append(i)



if __name__ == "__main__":
    main()