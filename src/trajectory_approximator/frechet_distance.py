import numpy
import numpy as np
from scipy.optimize import minimize
from scipy.optimize import basinhopping
from scipy.optimize import least_squares

import matplotlib.pyplot as plt
import math
from scipy.optimize import curve_fit

# from https://pypi.org/project/frechetdist/, inspired by Eiter, T. and Mannila, H., 1994. Computing discrete Fréchet distance. Tech. Report CD-TR 94/64, Information Systems Department, Technical University of Vienna
# with the help of chatGPT
def frechet_distance(x, y1, y2):

    n = len(y1)
    m = len(y2)
    ca = np.full((n, m), -1)

    p1 = np.vstack((x, y1)).T
    p2 = np.vstack((x, y2)).T

    def c(i, j):
        if ca[i, j] > -1:
            return ca[i, j]
        elif i == 0 and j == 0:
            ca[i, j] = math.dist(p1[0], p2[0])
        elif i > 0 and j == 0:
            ca[i, j] = max(c(i - 1, 0), math.dist(p1[i], p2[0]))
        elif i == 0 and j > 0:
            ca[i, j] = max(c(0, j - 1), math.dist(p1[0], p2[j]))
        elif i > 0 and j > 0:
            ca[i, j] = max(min(c(i - 1, j), c(i - 1, j - 1), c(i, j - 1)), math.dist(p1[i], p2[j]))
        else:
            ca[i, j] = 1e6
            print(1e6)
        return ca[i, j]

    return c(n - 1, m - 1)


def curve_function(x, a):
    # Your implementation of the curve function with parameters
    y = a * x**2
    return y


def optimize_params(x, y, curve_func):
    def loss(params):
        predicted_curve = curve_func(x, *params)
        frechet = frechet_distance(x, predicted_curve, y)
        print(frechet)
        return frechet

    # Initial parameter values
    initial_params = [-300.0]

    # Minimize the loss function using Scipy's minimize function
    result = minimize(loss, initial_params, method='Powell')

    return result.x

# Example
x = np.array([0, 1, 2, 3, 4, 5])
y = np.array([0, 1, 4, 9, 16, 25])
params = optimize_params(x, y, curve_function)
# params = curve_fit(curve_function, x, y, p0=[-300.0])
print(params)

plt.plot(x, y, 'o', label='data')
plt.plot(x, curve_function(x, params[0]), label='fit')
plt.legend()
plt.show()
