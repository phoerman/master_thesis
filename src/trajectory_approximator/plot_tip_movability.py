import os

import matplotlib.pyplot as plt
import numpy as np
from shapely import wkt
import math
import matplotlib

endpoints = np.zeros((72-45, 2))
for image in range(13, 26):
    progress_param = []

    # Load data
    pts = np.loadtxt(
        "/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/2023_03_09-10_29_59-1fps/image" + str(
            image) + "/voronoi_points.csv",
        skiprows=1, delimiter=',')

    fig4 = wkt.loads(
        open(
            "/home/patricia/workspaces/master_thesis/src/image_processing/test/outputs/2023_03_09-10_29_59-1fps/image" + str(
                image) + "/wkt.txt").read())
    for geom in fig4.geoms:
        xs, ys = geom.exterior.xy

    allid = []
    allpts = []
    ma = np.loadtxt(
        "/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/2023_03_09-10_29_59-1fps/image" + str(
            image) + "/voronoi_edges.csv",
        dtype=int, skiprows=1, delimiter=',')
    for mal in ma:
        if not (mal[0] in allid):
            allid.append(mal[0])
        if not (mal[1] in allid):
            allid.append(mal[1])
    allid.sort()
    for ids in allid:
        allpts.append([pts[ids][0], pts[ids][1]])

    # Prepare Data in order to look in the right direction and to be horizontal in the beginning
    allpts.sort(reverse=True)
    xdata = []
    ydata = []
    for pt in allpts:
        xdata.append(pt[0])
        ydata.append(pt[1])
    xnumpy = np.asarray(xdata)
    ynumpy = np.asarray(ydata)

    offsety = ynumpy[0]
    ynumpy = ynumpy - offsety
    xnumpy = -xnumpy
    offsetx = np.amin(xnumpy)
    xnumpy = xnumpy - offsetx

    theta = math.atan(ynumpy[10] / xnumpy[10])
    xnumpy = xnumpy * math.cos(theta) + ynumpy * math.sin(theta)
    ynumpy = - xnumpy * math.sin(theta) + ynumpy * math.cos(theta)

    max_value = np.argmin(ynumpy)
    endpoints[image - 14][0] = xnumpy[max_value]
    endpoints[image - 14][1] = ynumpy[max_value]

endpoints = endpoints.T
figure, axes = plt.subplots()
Drawing_uncolored_circle = plt.Circle( (0.0, 0.0 ),
                                       259 ,
                                       fill = False )

axes.set_aspect( 1 )
axes.add_artist( Drawing_uncolored_circle )

plt.scatter(endpoints[0], endpoints[1])
plt.show()

