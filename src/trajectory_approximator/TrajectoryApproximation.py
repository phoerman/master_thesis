import math

import numpy
from shapely import wkt
import numpy as np
from scipy.optimize import minimize
import matplotlib.colors as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import csv

# Import os module
import os

# global params for later
progress_param = []
show_intermediate_process_frechet = False
l1 = 0
l2 = 0
fei = 0
m1 = 0
v1 = 0

add_penalty = False


def mae(y_true, predictions):
    y_true, predictions = np.array(y_true), np.array(predictions)
    return np.mean(np.abs(y_true - predictions))


def mse(actual, predicted):
    actual = np.array(actual)
    predicted = np.array(predicted)
    differences = np.subtract(actual, predicted)
    squared_differences = np.square(differences)
    return squared_differences.mean()


def rmse(actual, predicted):
    return math.sqrt(mse(actual, predicted))


def callback(x1):
    progress_param.append(x1)


def frechet_distance(x, y1, y2):
    # from https://pypi.org/project/frechetdist/, inspired by Eiter, T. and Mannila, H., 1994. Computing discrete Fréchet distance. Tech. Report CD-TR 94/64, Information Systems Department, Technical University of Vienna
    # with the help of chat GPT
    """
    Computes the Frechet distance between two curves x and y.
    """

    n = len(y1)
    m = len(y2)
    ca = np.full((n, m), -1)

    p1 = np.vstack((x, y1)).T
    p2 = np.vstack((x, y2)).T

    def c(i_c, j_c):
        if ca[i_c, j_c] > -1:
            return ca[i_c, j_c]
        elif i_c == 0 and j_c == 0:
            ca[i_c, j_c] = math.dist(p1[0], p2[0])
        elif i_c > 0 and j_c == 0:
            ca[i_c, j_c] = max(c(i_c - 1, 0), math.dist(p1[i_c], p2[0]))
        elif i_c == 0 and j_c > 0:
            ca[i_c, j_c] = max(c(0, j_c - 1), math.dist(p1[0], p2[j_c]))
        elif i_c > 0 and j_c > 0:
            ca[i_c, j_c] = max(min(c(i_c - 1, j_c), c(i_c - 1, j_c - 1), c(i_c, j_c - 1)), math.dist(p1[i_c], p2[j_c]))
        else:
            ca[i_c, j_c] = 1e6
        return ca[i_c, j_c]

    return c(n - 1, m - 1)


def optimize_params(x, y):

    def loss(params_loss):

        predicted_curve = func(x, *params_loss)
        curve_list = predicted_curve.tolist()
        y_list = y.tolist()
        return frechet_distance(x, curve_list, y_list)

    initial_params = [l1, l2, fei, m1, v1]
    result = []

    # Minimize the loss function using Scipy's minimize function
    result = minimize(loss, initial_params, method='Powell',
                      callback=callback)  # SLSQP, Nelder-Mead, Powell, L-BFGS-B
    print('minimum frechet:', result.fun)
    print('reason for stopping and number of iterations:', result.message, result.nit)

    return result.x


def func(x, l1_func, l2_func, fei_func, m1_func, v1_func):
    t = (x - l1_func) / (l2_func - l1_func)
    h0 = 1 - 3 * t ** 2 + 2 * t ** 3
    h1 = 3 * t ** 2 - 2 * t ** 3
    h2 = t - 2 * t ** 2 + t ** 3
    h3 = - t ** 2 + t ** 3
    m0 = -(fei_func * (l1_func ** 3) / 3)
    v0 = -math.tan(fei_func * l1_func ** 2 / 2)

    cubichermite = h0 * m0 + h1 * m1_func + h2 * v0 * (l2_func - l1_func) + h3 * v1_func * (l2_func - l1_func)
    beam = -(fei_func * (3 * l1_func * x ** 2 - x ** 3) / 6)
    line = v1_func * x + m1_func - v1_func * l2_func

    if l1_func <= l2_func:

        arr = beam[np.where(x < l1_func)]
        arr = np.append(arr, cubichermite[np.where((x >= l1_func) & (x < l2_func))])
        arr = np.append(arr, line[np.where(x >= l2_func)])
    else:
        arr = beam[np.where(x < l2_func)]
        arr = np.append(arr, cubichermite[np.where((x >= l2_func) & (x < l1_func))])
        arr = np.append(arr, line[np.where(x >= l1_func)])

    penalty = 0

    if l1_func > l2_func:
        penalty = 10000000
        return np.add(arr, penalty)

    penalty = -(l2_func - l1_func) * 1000 / 30 + 1000
    if l2_func > np.amax(x) - 10:
        penalty = penalty + l2_func * 100

    if not add_penalty:
        penalty = 0
    return np.add(arr, penalty)


# Output file from C++ medial axis code
# Initialize the directory name for saving
dirname = "/home/patricia/workspaces/master_thesis/src/trajectory_approximator/outputs/2023_03_09-10_29_59-1fps"
# Check the directory name exist or not
if not os.path.isdir(dirname):
    # Create the directory
    os.mkdir(dirname)
    # Print success message
    print('The directory ' + dirname + 'is created.')
f = open(dirname + '/params.csv', 'w')

for image in range(0, 476):
    progress_param = []

    # Load data
    pts = np.loadtxt(
        "/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/2023_03_09-10_29_59-1fps/image" + str(
            image) + "/voronoi_points.csv",
        skiprows=1, delimiter=',')

    fig4 = wkt.loads(
        open(
            "/home/patricia/workspaces/master_thesis/src/image_processing/test/outputs/2023_03_09-10_29_59-1fps/image" + str(
                image) + "/wkt.txt").read())
    for geom in fig4.geoms:
        xs, ys = geom.exterior.xy

    allid = []
    allpts = []
    ma = np.loadtxt(
        "/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/2023_03_09-10_29_59-1fps/image" + str(
            image) + "/voronoi_edges.csv",
        dtype=int, skiprows=1, delimiter=',')
    for mal in ma:
        if not (mal[0] in allid):
            allid.append(mal[0])
        if not (mal[1] in allid):
            allid.append(mal[1])
    allid.sort()
    for ids in allid:
        allpts.append([pts[ids][0], pts[ids][1]])

    # Prepare Data in order to look in the right direction and to be horizontal in the beginning
    allpts.sort(reverse=True)
    xdata = []
    ydata = []
    for pt in allpts:
        xdata.append(pt[0])
        ydata.append(pt[1])
    xnumpy = np.asarray(xdata)
    ynumpy = np.asarray(ydata)

    offsety = ynumpy[0]
    ynumpy = ynumpy - offsety
    xnumpy = -xnumpy
    offsetx = np.amin(xnumpy)
    xnumpy = xnumpy - offsetx

    theta = math.atan(ynumpy[10] / xnumpy[10])
    xnumpy = xnumpy * math.cos(theta) + ynumpy * math.sin(theta)
    ynumpy = - xnumpy * math.sin(theta) + ynumpy * math.cos(theta)

    # Fit the curve to the data

    # with non lin least square
    if image == 0:
        l2 = np.amax(xnumpy) - 25
        l1 = l2 - 75
        closest = 0
        distance = 100000
        for it in range(len(xnumpy)):
            if abs(xnumpy[it] - l2) < distance:
                closest = it

        m1 = ynumpy[closest]
        v1 = m1 / l2
        fei = 1.72970312e-05
    popt, pcov, infodict, mesg, ier = curve_fit(func, xnumpy, ynumpy, p0=[l1, l2, fei, m1, v1],
                                            full_output=True, maxfev=150000)

    l1 = popt[0]
    l2 = popt[1]
    fei = popt[2]
    m1 = popt[3]
    v1 = popt[4]

    # # with frechet
    # params = optimize_params(xnumpy, ynumpy)
    # print('[image:', image, '] frechet params:', params)

    print('[image:', image, '] non lin least square params:', popt)

    add_penalty = False

    # mae_frechet = mae(ynumpy, func(xnumpy, *params))
    mae_nonlinleast = mae(ynumpy, func(xnumpy, *popt))
    # mse_frechet = mse(ynumpy, func(xnumpy, *params))
    mse_nonlinleast = mse(ynumpy, func(xnumpy, *popt))
    # rmse_frechet = rmse(ynumpy, func(xnumpy, *params))
    rmse_nonlinleast = rmse(ynumpy, func(xnumpy, *popt))

    print('[image:', image, '] non lin least square mae:', mae_nonlinleast)
    print('[image:', image, '] non lin least square mse:', mse_nonlinleast)
    print('[image:', image, '] non lin least square rmse:', rmse_nonlinleast)
    # print('[image:', image, '] frechet mae:', mae_frechet)
    # print('[image:', image, '] frechet mse:', mse_frechet)
    # print('[image:', image, '] frechet rmse:', rmse_frechet)

    # Plot the original data and the fitted curve
    # cmap = plt.cm.get_cmap('viridis')
    #
    # all_distances = []
    # for prog in progress_param:
    #     dist = frechet_distance(xnumpy, ynumpy, func(xnumpy, *prog))
    #     all_distances.append(dist)
    # for i in range(len(all_distances)):
    #     colors = cmap(all_distances[i] / np.max(all_distances))
    #     myplot = plt.plot(xnumpy, func(xnumpy, *progress_param[i]), label='iteration', c=colors)

    # norm = mpl.Normalize(vmin=0, vmax=np.max(all_distances))
    # sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    plt.plot(xnumpy, ynumpy, 'o', label='data', color="black")
    plt.plot(xnumpy, func(xnumpy, *popt), label='non lin least square', color="red")
    plt.gca().set_aspect('equal')
    # plt.plot(xnumpy, func(xnumpy, *params), label='frechet', color="blue")
    plt.legend()

    # also plot the color bar
    # plt.colorbar(sm, label="Frechet Distance")

    writer = csv.writer(f)
    if image == 0:
        header = ['image number', 'non lin least square param l1', 'non lin least square param l2',
                  'non lin least square param fei', 'non lin least square param m1', 'non lin least square param v1',
                  'non lin least square mae', 'non lin least square mse', 'non lin least square rmse', 'x max']
        writer.writerow(header)
    data = [image, popt[0], popt[1], popt[2], popt[3], popt[4], mae_nonlinleast, mse_nonlinleast, rmse_nonlinleast, np.amax(xnumpy)]
    writer.writerow(data)

    image_save_directory = dirname + "/graph" + str(image) + ".png"
    plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
    # plt.show()

    # clear plot
    plt.clf()
f.close()
