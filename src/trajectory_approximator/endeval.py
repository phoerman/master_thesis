import math
import numpy
from shapely import wkt
import numpy as np
from scipy.optimize import minimize
import matplotlib.colors as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import csv
import os
import pandas as pd

import statistics



df=pd.read_csv("/home/patricia/workspaces/master_thesis/src/trajectory_approximator/outputs/2023_03_09-10_29_59-1fps/params.csv")

alll1 = df["non lin least square param l1"].values
alll2 = df["non lin least square param l2"].values
allfei = df["non lin least square param fei"].values
allm1 = df["non lin least square param m1"].values
allv1 = df["non lin least square param v1"].values
allmae = df["non lin least square mae"].values
allmse = df["non lin least square mse"].values
allrmse = df["non lin least square rmse"].values
allxmax =df["x max"].values

q1_l1 = np.quantile(alll1, .25)
q1_l2 = np.quantile(alll2, .25)
q1_fei = np.quantile(allfei, .25)
q1_m1 = np.quantile(allm1, .25)
q1_v1 = np.quantile(allv1, .25)
q2_l1 = np.quantile(alll1, .5)
q2_l2 = np.quantile(alll2, .5)
q2_fei = np.quantile(allfei, .5)
q2_m1 = np.quantile(allm1, .5)
q2_v1 = np.quantile(allv1, .5)
q3_l1 = np.quantile(alll1, .75)
q3_l2 = np.quantile(alll2, .75)
q3_fei = np.quantile(allfei, .75)
q3_m1 = np.quantile(allm1, .75)
q3_v1 = np.quantile(allv1, .75)

f = open('/home/patricia/workspaces/master_thesis/src/trajectory_approximator/outputs/2023_03_09-10_29_59-1fps/eval.csv', 'w')

writer = csv.writer(f)
header = ['min mae', 'min mse', 'min rmse',
          'max mae', 'max mse', 'max rmse',
          'avg mae', 'avg mse', 'avg rmse',
          'var mae', 'var mse', 'var rmse',
          'Q1 mae', 'Q1 mse', 'Q1 rmse',
          'Q2 mae', 'Q2 mse', 'Q2 rmse',
          'Q3 mae', 'Q3 mse', 'Q3 rmse',
          'Q1 l1', 'Q1 l2', 'Q1 fei', 'Q1 m1', 'Q1 v1',
          'Q2 l1', 'Q2 l2', 'Q2 fei', 'Q2 m1', 'Q2 v1',
          'Q3 l1', 'Q3 l2', 'Q3 fei', 'Q3 m1', 'Q3 v1',
          # 'x max'
          ]
writer.writerow(header)
data = [np.amin(allmae), np.amin(allmse), np.amin(allrmse),
        np.amax(allmae), np.amax(allmse), np.amax(allrmse),
        np.average(allmae), np.average(allmse), np.average(allrmse),
        statistics.variance(allmae), statistics.variance(allmse), statistics.variance(allrmse),
        np.quantile(allmae, .25), np.quantile(allmse, .25), np.quantile(allrmse, .25),
        np.quantile(allmae, .50), np.quantile(allmse, .50), np.quantile(allrmse, .50),
        np.quantile(allmae, .75), np.quantile(allmse, .75), np.quantile(allrmse, .75),
        q1_l1, q1_l2, q1_fei, q1_m1, q1_v1,
        q2_l1, q2_l2, q2_fei, q2_m1, q2_v1,
        q3_l1, q3_l2, q3_fei, q3_m1, q3_v1,
        # allxmax
        ]
writer.writerow(data)


dirname = "/home/patricia/workspaces/master_thesis/src/trajectory_approximator/outputs/2023_03_09-10_29_59-1fps"

# alll1 = [i for i in alll1 if i <= 1000]
x0 = numpy.arange(len(alll1))
image_save_directory = dirname + "/paramsOverTimeL1.png"
plt.plot(x0, alll1, 'o', label='l1 without outliers')
plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
plt.legend()
# clear plot
plt.clf()

# alll2 = [i for i in alll2 if i <= 2000]
x1 = numpy.arange(len(alll2))
image_save_directory = dirname + "/paramsOverTimeL2.png"
plt.plot(x1, alll2, 'o', label='l2 without outliers')
plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
plt.legend()
# clear plot
plt.clf()

# allfei = [i for i in allfei if (i <= 0.002 and i >= -0.002)]
x2 = numpy.arange(len(allfei))
image_save_directory = dirname + "/paramsOverTimeFei.png"
plt.plot(x2, allfei, 'o', label='fei without outliers')
plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
plt.legend()
# clear plot
plt.clf()

# allm1 = [i for i in allm1 if (i <= 1000 and i >= -1000)]
x3 = numpy.arange(len(allm1))
image_save_directory = dirname + "/paramsOverTimeM1.png"
plt.plot(x3, allm1, 'o', label='m1 without outliers')
plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
plt.legend()
# clear plot
plt.clf()

# allv1 = [i for i in allv1 if (i <= 25 and i >= -25)]
x4 = numpy.arange(len(allv1))
image_save_directory = dirname + "/paramsOverTimeV1.png"
plt.plot(x4, allv1, 'o', label='v1 without outliers')
plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
plt.legend()
# clear plot
plt.clf()

# allxmax = [i for i in allxmax if (i <= 25 and i >= -25)]
x5 = numpy.arange(len(allxmax))
image_save_directory = dirname + "/paramsOverTimeXMax.png"
plt.plot(x5, allxmax, 'o', label='v1 without outliers')
plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
plt.legend()
# clear plot
plt.clf()
f.close()

