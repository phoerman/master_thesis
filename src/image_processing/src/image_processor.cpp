#include <fstream>

#include "image_processing/image_processor.h"

namespace image_processing
{
    ImageProcessor::ImageProcessor(const rclcpp::NodeOptions& options) : Node("image_processor",  options)
    {
        parse_parameters();
        for (int i = 0; i < (numberOfImages_); i++){
            read_image(i);
            apriltag_detection(i);
            transform_perspective(i);
            robot_recognizer(i);
            find_middle(i);
            if (getWKT_){
                array_to_polygon_wkt(i);
            }
        }
        rclcpp::shutdown();

    }

    ImageProcessor::~ImageProcessor() = default;

    // Parse parameters
    void ImageProcessor::parse_parameters()
    {
        this->declare_parameter("number_of_images", 100);
        this->declare_parameter("camera_was_not_moved", false);
        this->declare_parameter("image_read_in_directory", "default");
        this->declare_parameter("dataset", "default");
        this->declare_parameter("safe_debug_images", false);
        this->declare_parameter("camera_parameters.distortion_coefficients", std::vector<double>({0, 0, 0, 0, 0}));
        this->declare_parameter("camera_parameters.camera_matrix", std::vector<double>({0, 0, 0, 0, 0, 0, 0, 0, 0}));
        this->declare_parameter("apriltag_outside.length", 100);
        this->declare_parameter("apriltag_outside.width", 100);
        this->declare_parameter("thresholding.threshold", 100);
        this->declare_parameter("thresholding.type", 0);
        this->declare_parameter("contouring_type", 1);
        this->declare_parameter("size_apriltag", 23);
        this->declare_parameter("border_apriltag", 100);
        this->declare_parameter("min_area_contour", 3000);
        this->declare_parameter("connect_roboa_parts.do", false);
        this->declare_parameter("connect_roboa_parts.shape", 2);
        this->declare_parameter("connect_roboa_parts.ksize", 5);
        this->declare_parameter("connect_roboa_parts.iterations", 10);
        this->declare_parameter("get_wkt", false);
        this->declare_parameter("wkt_interval", 100);


        numberOfImages_ = this->get_parameter("number_of_images").as_int();
        cameraWasNotMoved_ = this->get_parameter("camera_was_not_moved").as_bool();
        imageReadInDirectory_ = this->get_parameter("image_read_in_directory").as_string();
        dataset_ = this->get_parameter("dataset").as_string();
        safeDebugImages_ = this->get_parameter("safe_debug_images").as_bool();
        distortionCoefficientsVector_ = this->get_parameter("camera_parameters.distortion_coefficients").as_double_array();
        cameraMatrixVector_ = this->get_parameter("camera_parameters.camera_matrix").as_double_array();
        apriltagLengthOutside_ = this->get_parameter("apriltag_outside.length").as_int();
        apriltagWidthOutside_ = this->get_parameter("apriltag_outside.width").as_int();
        thresholdBinary_ = this->get_parameter("thresholding.threshold").as_int();
        thresholdingType_ = this->get_parameter("thresholding.type").as_int();
        contouringType_ = this->get_parameter("contouring_type").as_int();
        sizeApriltag_ = this->get_parameter("size_apriltag").as_int();
        borderApriltag_ = this->get_parameter("border_apriltag").as_int();
        minAreaContour_ = this->get_parameter("min_area_contour").as_int();
        connectRoboaPartsDo_ = this->get_parameter("connect_roboa_parts.do").as_bool();
        connectRoboaPartsShape_ = this->get_parameter("connect_roboa_parts.shape").as_int();
        connectRoboaPartsKsize_ = this->get_parameter("connect_roboa_parts.ksize").as_int();
        connectRoboaPartsIterations_ = this->get_parameter("connect_roboa_parts.iterations").as_int();
        getWKT_ = this->get_parameter("get_wkt").as_bool();
        WKTInterval_ = this->get_parameter("wkt_interval").as_int();

    }
    // Read in image for later use
    void ImageProcessor::read_image(int image)
    {
        std::string imageReadIn = imageReadInDirectory_ + "/" + dataset_ + "/image" + std::to_string(image) + ".jpg";
        image_ = cv::imread(imageReadIn);

        // Error Handling
        if (image_.empty()) {
            RCLCPP_ERROR(this->get_logger(), "[read_image] image %i: Image not found", image);
     }

        std::string::size_type const p(imageReadInDirectory_.find_last_of('/') + 1);
        imageSafeDirectory_ = imageReadInDirectory_.substr(0, p) + "outputs/" + dataset_ + "/image" + std::to_string(image) + "/";

        if (std::filesystem::create_directory(imageReadInDirectory_.substr(0, p) + "outputs")){
            RCLCPP_DEBUG(this->get_logger(), "[read_image] image %i: direcotry %s created", image, imageSafeDirectory_.c_str());
        }
        if (std::filesystem::create_directory(imageReadInDirectory_.substr(0, p) + "outputs/" + dataset_)){
            RCLCPP_DEBUG(this->get_logger(), "[read_image] image %i: direcotry %s created", image, imageSafeDirectory_.c_str());
        }
        if (std::filesystem::create_directory(imageReadInDirectory_.substr(0, p) + "outputs/" + dataset_ + "/image" + std::to_string(image))){
            RCLCPP_DEBUG(this->get_logger(), "[read_image] image %i: direcotry %s created",image,imageSafeDirectory_.c_str());
        }
        RCLCPP_DEBUG(this->get_logger(), "[read_image] image %i: safe direcotry: %s", image,  imageSafeDirectory_.c_str());
    }

    //Detect april tags in image and safe coordinates of edges
    void ImageProcessor::apriltag_detection(int image) {
        if (!cameraWasNotMoved_ || image == 1 || (markerCorners_.size() < 4)){
            // code partially borrowed from https://docs.opencv.org/3.4/df/d4a/tutorial_charuco_detection.html
            cv::Mat imageCopy;
            std::vector<int> markerIds;
            std::vector<cv::Vec3d> rvecs, tvecs;

            float cameraMatrixVectorFloat[9];

            for (uint i = 0; i < cameraMatrixVector_.size(); ++i){
                cameraMatrixVectorFloat[i] = (float) cameraMatrixVector_[i];
            }

            cv::Matx33f cameraMatrix(cameraMatrixVectorFloat);
            cv::Mat distortionCoefficients = (cv::Mat_<double>(distortionCoefficientsVector_)).reshape(5);

            image_.copyTo(imageCopy);
            cv::aruco::detectMarkers(image_, dictionary_, markerCorners_, markerIds);

            // Debug marker information
            for (uint i=0;i < markerIds.size();i++) {
                RCLCPP_DEBUG(this->get_logger(), "[apriltag_detection] image %i: Markercorners %i, id %i", image, i, markerIds[i]);

                for (uint j=0; j < markerCorners_[i].size();j++){
                    RCLCPP_DEBUG(this->get_logger(), "[apriltag_detection]  image %i: %f, %f", image, markerCorners_[i][j].x, markerCorners_[i][j].y);
                }
            }

            // if at least one marker detected
            if (!markerIds.empty()) {
                cv::aruco::drawDetectedMarkers(imageCopy, markerCorners_, markerIds,
                                               cv::Scalar(0, 255, 0));
                cv::aruco::estimatePoseSingleMarkers(markerCorners_, 0.08, cameraMatrix,
                                                     distortionCoefficients, rvecs, tvecs);
                for(uint i=0; i < markerIds.size(); i++)
                    cv::drawFrameAxes(imageCopy, cameraMatrix, distortionCoefficients, rvecs[i],
                                      tvecs[i], 0.1);
                if (markerIds.size() >= 4){
                    RCLCPP_INFO(this->get_logger(), "[apriltag_detection]  image %i: All four tags found", image);
                }
                else{
                    RCLCPP_ERROR(this->get_logger(), "[apriltag_detection]  image %i: Not enough tags, number of tags: %li, please remove image", image, markerIds.size());

                }
            }
            else{
                RCLCPP_ERROR(this->get_logger(), "[apriltag_detection]  image %i: Tags not found, please remove image", image);
            }
            if (safeDebugImages_){
                cv::imwrite(imageSafeDirectory_ + "apriltagImage.png", imageCopy);
            }
        }

    }

    // Take coordinates of Corners of April Tags to get rid of perspective distortion
    void ImageProcessor::transform_perspective(int image) {
        // inspired by https://stackoverflow.com/questions/54015836/get-imagerect-on-a-plane-with-perspective-correction-from-camera-2d-image
        // and https://courses.ece.cornell.edu/ece5990/ECE5725_Fall2020_Projects/Dec_21_Demo/Drawing%20Robot/eam348_mm2994_W/index.html

        std::vector<cv::Point2f> inputVec = std::vector<cv::Point2f>(4);
        std::vector<cv::Point2f> outputVec = std::vector<cv::Point2f>(4);
        cv::Mat transformMatrix;

        double factor = apriltagLengthOutside_ / (double)apriltagWidthOutside_;
        perspectiveCorrectedImageWidth_ = image_.rows * factor;
        perspectiveCorrectedImageHight_ = image_.rows;

        inputVec[0] = markerCorners_[0][1];
        inputVec[1] = markerCorners_[1][0];
        inputVec[2] = markerCorners_[2][2];
        inputVec[3] = markerCorners_[3][3];


        outputVec[1].x = perspectiveCorrectedImageWidth_;
        outputVec[1].y = perspectiveCorrectedImageHight_;
        outputVec[0].x = 0.0;
        outputVec[0].y = perspectiveCorrectedImageHight_;
        outputVec[3].x = perspectiveCorrectedImageWidth_;
        outputVec[3].y = 0.0;
        outputVec[2].x = 0.0;
        outputVec[2].y = 0.0;



        transformMatrix = cv::getPerspectiveTransform(inputVec, outputVec);
        cv::warpPerspective(image_, perspectiveCorrectedImage_, transformMatrix, cv::Size(
                perspectiveCorrectedImageWidth_, perspectiveCorrectedImageHight_));

        RCLCPP_INFO(this->get_logger(), "[transform_perspective]  image %i: Perspective Corrected", image);

        if (safeDebugImages_){
            cv::imwrite(imageSafeDirectory_ + "perspectiveTransform.png", perspectiveCorrectedImage_);
        }
    }

    // Recognize the robot within the undistorted image and create a black and white of it
    void ImageProcessor::robot_recognizer(int image){
        //inspired by https://learnopencv.com/contour-detection-using-opencv-python-c/
        contours_.clear();
        robotContourID_.clear();


        cv::Mat imageGray;

        cv::cvtColor(perspectiveCorrectedImage_, imageGray, cv::COLOR_BGR2GRAY);
        cv::Mat thresh;
        cv::threshold(imageGray, thresh, thresholdBinary_, 255, thresholdingType_);

        bitwise_not(thresh, thresh);

        // detect the contours on the binary image using cv2.CHAIN_APPROX_NONE

        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(thresh, contours_, hierarchy, cv::RETR_EXTERNAL,
                         contouringType_);
        //cv::RETR_TREE also shows inner contours_

        // draw contours_ on the original image
        cv::Mat image_recognized_robot = perspectiveCorrectedImage_.clone();


        cv::Mat imageNew(perspectiveCorrectedImage_.size(), CV_8UC1, cv::Scalar(0));
        imageNew.copyTo(imageRoBoaOnly_);
        for(uint i=0; i < contours_.size(); i++)
        {
            bool goodContour = false;

            // Filter contours_ that are apriltags according to their location
            for(uint j=0; j < contours_[i].size(); j++){
                if (contours_[i][j].x <(perspectiveCorrectedImageWidth_ - sizeApriltag_ / apriltagLengthOutside_ *
                perspectiveCorrectedImageWidth_ - borderApriltag_) && contours_[i][j].x >
                (sizeApriltag_ / apriltagLengthOutside_ * perspectiveCorrectedImageWidth_ + borderApriltag_) &&
                contours_[i][j].y <(perspectiveCorrectedImageHight_ - sizeApriltag_ / apriltagWidthOutside_ *
                perspectiveCorrectedImageHight_ - borderApriltag_) && contours_[i][j].y >
                (sizeApriltag_ / apriltagWidthOutside_ * perspectiveCorrectedImageHight_ + borderApriltag_)){
                    goodContour = true;
                }
            }
            double areaContour=cv::contourArea(contours_[i]);

            RCLCPP_DEBUG(this->get_logger(), "[robot_recognizer]  image %i: areasize of %i: %f", image, i, areaContour);

            if(areaContour>minAreaContour_ && goodContour == true) {
                robotContourID_.push_back(i);
            }

        }
        RCLCPP_DEBUG(this->get_logger(), "[robot_recognizer]  image %i: good contours size: %li",  image, robotContourID_.size());

        if (robotContourID_.empty()){
            RCLCPP_ERROR(this->get_logger(), "[robot_recognizer]  image %i: Robot not found, increase threshold", image);
        }

        for (uint i = 0; i < robotContourID_.size(); ++i){
                drawContours(imageRoBoaOnly_, contours_, robotContourID_[i], cv::Scalar(255, 255, 255),
                             -1);
                drawContours(image_recognized_robot, contours_, robotContourID_[i], cv::Scalar(0, 255, 0),
                             7);
                RCLCPP_DEBUG(this->get_logger(), "[robot_recognizer]  image %i: id of contour: %i", image, robotContourID_[i]);

        }

        if (robotContourID_.size() == 2){
            RCLCPP_INFO(this->get_logger(), "[robot_recognizer]  image %i: Robot is split into two parts, setting connect_roboa_parts.do to true is recommended", image);
        }

        // If RoBoa is split in two half

        if (connectRoboaPartsDo_){
            RCLCPP_INFO(this->get_logger(), "[robot_recognizer]  image %i: Connecting robot segments", image);
            cv::Mat image_connected = perspectiveCorrectedImage_.clone();
            cv::dilate(imageRoBoaOnly_, imageRoBoaOnly_, cv::getStructuringElement(
                    connectRoboaPartsShape_, cv::Size(connectRoboaPartsKsize_,connectRoboaPartsKsize_)), cv::Point(-1, -1),
                       connectRoboaPartsIterations_);
            cv::erode(imageRoBoaOnly_, imageRoBoaOnly_, cv::getStructuringElement(
                              connectRoboaPartsShape_, cv::Size(connectRoboaPartsKsize_,connectRoboaPartsKsize_)), cv::Point(-1, -1),
                      connectRoboaPartsIterations_);

            cv::findContours(imageRoBoaOnly_, contours_, hierarchy, cv::RETR_EXTERNAL,
                             contouringType_);

            drawContours(image_connected, contours_, -1, cv::Scalar(0, 255, 0),
                         7);
            if (safeDebugImages_){
                cv::imwrite(imageSafeDirectory_ + "robotRecognizedWhole.png", image_connected);
            }
        }

        if (safeDebugImages_){
            cv::imwrite(imageSafeDirectory_ + "robotRecognized.png", image_recognized_robot);
            cv::imwrite(imageSafeDirectory_ + "robotRecognizedB&W.png", imageRoBoaOnly_);
        }


        if (robotContourID_.size() > 2){
            RCLCPP_ERROR(this->get_logger(), "[robot_recognizer]  image %i: Too many contours, decrease threshold", image);
        }

        else{
            RCLCPP_INFO(this->get_logger(), "[robot_recognizer]  image %i: Robot detected", image);
        }

    }

    // Find middle line of robot by eroding the robot
    void ImageProcessor::find_middle(int image){
        // from http://felix.abecassis.me/2011/09/opencv-morphological-skeleton/

        cv::Mat skel(imageRoBoaOnly_.size(), CV_8UC1, cv::Scalar(0));
        cv::Mat roboaWithSkel;
        cv::Mat roboaWhite;
        cv::Mat locations;
        cv::Mat middleLine(imageRoBoaOnly_.size(), CV_8UC1, cv::Scalar(0));
        imageRoBoaOnly_.copyTo(roboaWhite);
//        skel.copyTo(roboaWithSkel);
        cv::Mat temp(imageRoBoaOnly_.size(), CV_8UC1);
        cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3,3));

        bool done;
        do
        {
            cv::morphologyEx(imageRoBoaOnly_, temp, cv::MORPH_OPEN, element);
            cv::bitwise_not(temp, temp);
            cv::bitwise_and(imageRoBoaOnly_, temp, temp);
            cv::bitwise_or(skel, temp, skel);
            cv::erode(imageRoBoaOnly_, imageRoBoaOnly_, element);

            double max;
            cv::minMaxLoc(imageRoBoaOnly_, 0, &max);
            done = (max == 0);
        } while (!done);

        cv::findNonZero(skel, locations);
        cv::drawContours(middleLine, locations, -1, cv::Scalar(255,255,255), 5);
        cv::bitwise_xor(roboaWhite, middleLine, roboaWithSkel);

        RCLCPP_INFO(this->get_logger(), "[find_middle]  image %i: Robot Skeletonized", image);

        if (safeDebugImages_){
            cv::imwrite(imageSafeDirectory_ + "robotWithSkeleton.png", roboaWithSkel);
            cv::imwrite(imageSafeDirectory_ + "robotSkeleton.png", skel);
        }
    }

    // Export WKT with outline of the robot
    void ImageProcessor::array_to_polygon_wkt(int image){
        std::string str = "MULTIPOLYGON(((";
        for(uint i=0;i<contours_[0].size();i+=WKTInterval_){
            if(i>0){
                str.push_back(',');
            }
            str = str + std::to_string(contours_[0][contours_[0].size()-i-1].x) + ' '
                    + std::to_string(contours_[0][contours_[0].size()-i-1].y);
        }
        str = str + "," + std::to_string(contours_[0][contours_[0].size()-1].x) + ' '
                + std::to_string(contours_[0][contours_[0].size()-1].y);
        str = str + ")))";
        std::ofstream out(imageSafeDirectory_ + "wkt.txt");
        out << str;
        out.close();

        RCLCPP_INFO(this->get_logger(), "[array_to_polygon_wkt]  image %i: WKT created", image);
    }

}  // namespace image_processing

#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(image_processing::ImageProcessor)
