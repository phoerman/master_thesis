#pragma once

#include <climits>
#include <string>
#include <filesystem>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/u_int32.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <apriltag_msgs/msg/april_tag_detection.hpp>
#include <apriltag_msgs/msg/april_tag_detection_array.hpp>


namespace image_processing
{

    class ImageProcessor : public rclcpp::Node
    {
    public:
        // Functions
        explicit ImageProcessor(const rclcpp::NodeOptions& options);
        ~ImageProcessor();

        //getter and setter

    private:
        // Functions
        void parse_parameters();
        void read_image(int i);
        void apriltag_detection(int i);
        void transform_perspective(int i);
        void robot_recognizer(int i);
        void find_middle(int i);
        void array_to_polygon_wkt(int i);

        // Parameters
        int numberOfImages_;
        bool cameraWasNotMoved_;
        std::string imageReadInDirectory_;
        std::string dataset_;
        std::string imageSafeDirectory_;
        cv::Mat image_;
        bool safeDebugImages_ = false;

        std::vector<double> cameraMatrixVector_;
        std::vector<double> distortionCoefficientsVector_;

        std::vector<std::vector<cv::Point2f>> markerCorners_;

        int apriltagLengthOutside_;
        int apriltagWidthOutside_;

        cv::Ptr<cv::aruco::Dictionary> dictionary_ = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_APRILTAG_36h11);

        cv::Mat perspectiveCorrectedImage_;

        double perspectiveCorrectedImageWidth_;
        double perspectiveCorrectedImageHight_;

        std::vector<std::vector<cv::Point>> contours_;
        std::vector<int> robotContourID_;
        int thresholdBinary_;
        int thresholdingType_;
        int contouringType_;
        int borderApriltag_;
        int sizeApriltag_;
        int minAreaContour_;

        cv::Mat imageRoBoaOnly_;

        bool connectRoboaPartsDo_;
        int connectRoboaPartsShape_;
        int connectRoboaPartsKsize_;
        int connectRoboaPartsIterations_;

        bool getWKT_;
        int WKTInterval_;

    };

}  // namespace image_processing
