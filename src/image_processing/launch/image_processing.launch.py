import os
from ament_index_python.packages import get_package_share_directory
import launch
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():

    config = os.path.join(
        get_package_share_directory('image_processing'),
        'config',
        'params.yaml'
    )

    container = ComposableNodeContainer(
        name="image_processing_container",
        namespace="",
        package="rclcpp_components",
        executable="component_container",
        emulate_tty=True,
        arguments=[
            "--ros-args",
            "--log-level",
            "image_processor:=info", # info or debug
        ],
        composable_node_descriptions=[
            ComposableNode(
                package="image_processing",
                plugin="image_processing::ImageProcessor",
                name="image_processor",
                namespace="image_processor",
                parameters=[config]
            ),
        ],
        output="screen",
    )

    return launch.LaunchDescription([container])
