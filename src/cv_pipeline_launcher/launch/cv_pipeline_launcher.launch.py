from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration as LaunchConfig
from launch.actions import DeclareLaunchArgument as LaunchArg
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    """Launch blackfly_s camera node."""
    flir_dir = get_package_share_directory('flir_spinnaker_custom')
    config_dir = flir_dir + '/config/'
    name_arg = 'blackfly_0'
    serial_arg = '21278227'
    print([LaunchConfig('serial'), '_'])
    config = config_dir + '/blackfly_s_gige_custom_params.yaml'
    node = Node(package='flir_spinnaker_ros2',
                executable='camera_driver_node',
                output='screen',
                name=name_arg,
                parameters=[
                    {config},
                    {'parameter_file': config_dir + 'blackfly_s_gige_custom.cfg',
                     'serial_number': serial_arg, }],
                remappings=[('~/control', '/exposure_control/control')])
    return LaunchDescription([node])