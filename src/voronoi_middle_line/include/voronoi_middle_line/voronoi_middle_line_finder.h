#pragma once
#include <filesystem>

#include "rclcpp/rclcpp.hpp"

#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/IO/WKT.h>
#include <CGAL/Segment_Delaunay_graph_2.h>
#include <CGAL/Segment_Delaunay_graph_adaptation_policies_2.h>
#include <CGAL/Segment_Delaunay_graph_adaptation_traits_2.h>
#include <CGAL/Segment_Delaunay_graph_traits_2.h>
#include <CGAL/Voronoi_diagram_2.h>
#include <CGAL/Triangulation_2.h>


typedef CGAL::Exact_predicates_exact_constructions_kernel              K;
typedef CGAL::Segment_Delaunay_graph_traits_2<K>                       Gt;
typedef CGAL::Segment_Delaunay_graph_2<Gt>                             SDG2;
typedef CGAL::Segment_Delaunay_graph_adaptation_traits_2<SDG2>         AT;
typedef CGAL::Segment_Delaunay_graph_degeneracy_removal_policy_2<SDG2> AP;
typedef CGAL::Voronoi_diagram_2<SDG2, AT, AP>      VoronoiDiagram;
typedef AT::Site_2                                 Site_2;
typedef AT::Point_2                                Point_2;
typedef VoronoiDiagram::Vertex_handle              Vertex_handle;
typedef VoronoiDiagram::Halfedge                   Halfedge;
typedef CGAL::Polygon_with_holes_2<K> Polygon;
typedef std::deque<Polygon> MultiPolygon;
typedef std::set<Point_2> Point2_Set;
typedef std::map<Vertex_handle, int> VH_Int_Map;



namespace voronoi_middle_line{

/// Holds a more accessible description of the Voronoi diagram
    struct MedialData {
        /// Map of vertices comprising the Voronoi diagram
        VH_Int_Map vertex_handles;
        /// List of edges in the diagram (pairs of the vertices above)
        std::vector<std::pair<int, int>> edges;
    };

/// create a line out of medial data
    struct Line {
        // defines the ids of the specific line
        std::vector<int> ids;
        // defines array of points in line
        std::vector<int> points;
    };

/// Holds a description for arm filtering with medial data
    struct Fork {
        // defines the id of the vectors
        std::vector<int> ids;
        // defines the start point of the fork
        int point;
        // defines the arms
        std::vector<Line> arms;
        // stores the angle between the arms
        std::vector<float> angles;


    };


    class VoronoiMiddleLineFinder : public rclcpp::Node{

            public:
            // Functions
            explicit VoronoiMiddleLineFinder(const rclcpp::NodeOptions& options);
            ~VoronoiMiddleLineFinder();

            //getter and setter

            private:

            //finds angle between two vectors given four points that define the two vectors
            void angle_between_arms(Fork &fork, double x1v1, double y1v1, double x2v1, double y2v1, double x1v2, double y1v2, double x2v2, double y2v2);
            //builds lines outgoing of a specific point. Define point and medial data. returns a line
            void make_arm(Fork &fork, MedialData md, int idBegin, std::vector<int> forkPoints);

            void parse_parameters();
            /// Read in WKT from file
            MultiPolygon get_wkt_from_file(int i);

            /// Converts a MultiPolygon into its corresponding Voronoi diagram
            VoronoiDiagram convert_mp_to_voronoi_diagram(const MultiPolygon &mp);

            /// Find @p item in collection @p c or add it if not present.
            /// Returns the index of `item`'s location
            int find_or_add(VH_Int_Map &c, const Vertex_handle &item);


            /// Convert a map of <T, int> pairs to a vector of `T` ordered by increasing int
            std::vector<Vertex_handle> map_to_ordered_vector(const VH_Int_Map &m);


            /// Find vertex handles which are in the interior of the MultiPolygon
            std::set<Vertex_handle> identify_vertex_handles_inside_mp(const VoronoiDiagram &vd,const MultiPolygon &mp
            );


            /// The medial axis is formed by building a Voronoi diagram and then removing
            /// the edges of the diagram which connect to the concave points of the
            /// MultiPolygon. Here, we identify those concave points
            Point2_Set identify_concave_points_of_mp(const MultiPolygon &mp);


            /// Print the points which collectively comprise the medial axis
            void print_medial_axis_points(const MedialData &md, const std::string &filename);


            /// Prints the edges of the medial diagram
            void print_medial_axis_edges(const MedialData &md, const std::string &filename);


            MedialData filter_voronoi_diagram_to_medial_axis(const VoronoiDiagram &vd,const MultiPolygon &mp);

            MedialData filter_arms(MedialData &md, bool cutEnds);

            void find_medial_axis(int i);






        int numberOfImages_;
        std::string wktReadInDirectory_;
        std::string dataset_;
        std::string safeDirectory_;
        int cutOffLength_;
        double minAngle_;
        double maxAngle_;
        bool checkForAngle_;
    };
}




//
// Created by patricia on 15.03.23.
//


