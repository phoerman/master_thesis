//
// Created by patricia on 16.03.23.
//
#include <fstream>
#include "voronoi_middle_line/voronoi_middle_line_finder.h"
// idea from https://stackoverflow.com/questions/69237154/how-do-you-get-the-medial-axis-of-a-multipolygon-using-cgal, own arm cutting algorithm, line 317

namespace voronoi_middle_line {
    VoronoiMiddleLineFinder::VoronoiMiddleLineFinder(const rclcpp::NodeOptions &options) : Node(
            "voronoi_middle_line_finder", options) {
        parse_parameters();
        for (int i = 0; i < numberOfImages_; i++){
            RCLCPP_INFO(this->get_logger(), "image: %i", i);

            find_medial_axis(i);

        }
        rclcpp::shutdown();

    }

    VoronoiMiddleLineFinder::~VoronoiMiddleLineFinder() = default;

    void VoronoiMiddleLineFinder::parse_parameters()
    {
        this->declare_parameter("number_of_images", 100);
        this->declare_parameter("wkt_read_in_directory", "default");
        this->declare_parameter("dataset", "default");
        this->declare_parameter("cut_arms.cut_off_length", 10);
        this->declare_parameter("cut_arms.min_angle", 0.3);
        this->declare_parameter("cut_arms.max_angle", 2.8);
        this->declare_parameter("cut_arms.check_for_angle_first", true);

        numberOfImages_ = this->get_parameter("number_of_images").as_int();
        wktReadInDirectory_ = this->get_parameter("wkt_read_in_directory").as_string();
        dataset_ = this->get_parameter("dataset").as_string();
        cutOffLength_ = this->get_parameter("cut_arms.cut_off_length").as_int();
        minAngle_ = this->get_parameter("cut_arms.min_angle").as_double();
        maxAngle_ = this->get_parameter("cut_arms.max_angle").as_double();
        checkForAngle_ = this->get_parameter("cut_arms.check_for_angle_first").as_bool();
    }

/// Read in WKT file
    MultiPolygon VoronoiMiddleLineFinder::get_wkt_from_file(int i) {
        std::string readIn = wktReadInDirectory_ + "/" + dataset_ + "/image" + std::to_string(i) + "/wkt.txt";
        std::ifstream fin(readIn);
        MultiPolygon mp;
        CGAL::read_multi_polygon_WKT(fin, mp);
        if (mp.empty()) {
            RCLCPP_ERROR(this->get_logger(), "get_wkt_from_file: WKT file wkt.txt was empty");
            rclcpp::shutdown();
        }
        for (const auto &poly: mp) {
            if (poly.outer_boundary().size() == 0) {
                RCLCPP_ERROR(this->get_logger(), "get_wkt_from_file: WKT file wkt.txt contained a polygon without an outer boundary");
                rclcpp::shutdown();
            }
        }

        std::string::size_type const p(readIn.find_last_of('/'));
        std::string p1 = readIn.substr(0, p);
        safeDirectory_ = "/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/" + dataset_ + "/" + p1.substr(p1.find_last_of("/") + 1) + "/";
        RCLCPP_DEBUG(this->get_logger(), "get_wkt_from_file: safe direcotry: %s", safeDirectory_.c_str());

        if (std::filesystem::create_directory("/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/" + dataset_ + "/")){
            RCLCPP_INFO(this->get_logger(), "get_wkt_from_file: direcotry %s created", safeDirectory_.c_str());
        }
        if (std::filesystem::create_directory(safeDirectory_)){
            RCLCPP_INFO(this->get_logger(), "get_wkt_from_file: direcotry %s created", safeDirectory_.c_str());
        }

        return mp;
    }

/// Converts a MultiPolygon into its corresponding Voronoi diagram
    VoronoiDiagram VoronoiMiddleLineFinder::convert_mp_to_voronoi_diagram(const MultiPolygon &mp) {
        VoronoiDiagram vd;

        const auto add_segments_to_vd = [&](const auto &poly) {
            RCLCPP_DEBUG(this->get_logger(), "convert_mp_to_voronoi_diagram: Size of polygon: %lu", poly.size());
            for (std::size_t i = 0; i < poly.size(); i++) {
                // Modulus to close the loop
                vd.insert(
                        Site_2::construct_site_2(poly[i], poly[(i + 1) % poly.size()])
                );
            }
        };
        for (const auto &poly: mp) {                    // For each polygon in MultiPolygon
            add_segments_to_vd(poly.outer_boundary());  // Add the outer boundary
            for (const auto &hole: poly.holes()) {       // And any holes
                add_segments_to_vd(hole);
            }
        }

        if (!vd.is_valid()) {
            RCLCPP_ERROR(this->get_logger(), "convert_mp_to_voronoi_diagram: Voronoi Diagram was not valid");
            rclcpp::shutdown();
        }

        RCLCPP_INFO(this->get_logger(), "convert_mp_to_voronoi_diagram: Voronoi diagram created");

        return vd;
    }

// Find @p item in collection @p c or add it if not present.
/// Returns the index of `item`'s location
    int VoronoiMiddleLineFinder::find_or_add(VH_Int_Map &c, const Vertex_handle &item) {
        // Map means we can do this in log(N) time
        if (c.count(item) == 0) {
            c.emplace(item, c.size());
            return c.size() - 1;
        }

        return c.at(item);
    }


/// Convert a map of <T, int> pairs to a vector of `T` ordered by increasing int
    std::vector<Vertex_handle> VoronoiMiddleLineFinder::map_to_ordered_vector(const VH_Int_Map &m) {
        std::vector <std::pair<Vertex_handle, int>> to_sort(m.begin(), m.end());
        to_sort.reserve(m.size());
        std::sort(to_sort.begin(), to_sort.end(), [](const auto &a, const auto &b) {
            return a.second < b.second;
        });

        std::vector <Vertex_handle> ret;
        ret.reserve(to_sort.size());
        std::transform(begin(to_sort), end(to_sort), std::back_inserter(ret),
                       [](auto const &pair) { return pair.first; }
        );

        return ret;
    }


/// Find vertex handles which are in the interior of the MultiPolygon
    std::set <Vertex_handle> VoronoiMiddleLineFinder::identify_vertex_handles_inside_mp(
            const VoronoiDiagram &vd,
            const MultiPolygon &mp
    ) {
        // Used to accelerate interior lookups by avoiding Point-in-Polygon checks for
        // vertices we've already considered
        std::set <Vertex_handle> considered;
        // The set of interior vertices we are building
        std::set <Vertex_handle> interior;

        for (
                auto edge_iter = vd.bounded_halfedges_begin();
                edge_iter != vd.bounded_halfedges_end();
                edge_iter++
                ) {
            // Determine if an orientation implies an interior vertex
            const auto inside = [](const auto &orientation) {
                return orientation == CGAL::POSITIVE;
            };

            // Determine if a vertex is in the interior of the multipolygon and, if so,
            // add it to `interior`
            const auto vertex_in_mp_interior = [&](const Vertex_handle &vh) {
                // Skip vertices which have already been considered, since a vertex may
                // be connected to multiple halfedges
                if (considered.count(vh) != 0) {
                    return;
                }
                // Ensure we don't look at a vertex twice
                considered.insert(vh);
                // Determine if the vertex is inside any polygon of the MultiPolygon
                const auto inside_of_a_poly = std::any_of(
                        mp.begin(), mp.end(), [&](const auto &poly) {
                            return inside(CGAL::oriented_side(vh->point(), poly));
                        }
                );
                // If the vertex was inside the MultiPolygon make a note of it
                if (inside_of_a_poly) {
                    interior.insert(vh);
                }
            };

            // Check both vertices of the current halfedge of the Voronoi diagram
            vertex_in_mp_interior(edge_iter->source());
            vertex_in_mp_interior(edge_iter->target());
        }

        return interior;
    }


/// The medial axis is formed by building a Voronoi diagram and then removing
/// the edges of the diagram which connect to the concave points of the
/// MultiPolygon. Here, we identify those concave points
    Point2_Set VoronoiMiddleLineFinder::identify_concave_points_of_mp(const MultiPolygon &mp) {
        Point2_Set concave_points;

        // Determine cross-product, given three points. The sign of the cross-product
        // determines whether the point is concave or convex.
        const auto z_cross_product = [](const Point_2 &pt1, const Point_2 &pt2, const Point_2 &pt3) {
            const auto dx1 = pt2.x() - pt1.x();
            const auto dy1 = pt2.y() - pt1.y();
            const auto dx2 = pt3.x() - pt2.x();
            const auto dy2 = pt3.y() - pt2.y();
            return dx1 * dy2 - dy1 * dx2;
        };

        // Loop through all the points in a polygon, get their cross products, and
        // add any concave points to the set we're building.
        // `sense` should be `1` for outer boundaries and `-1` for holes, since holes
        // will have points facing outward.
        const auto consider_polygon = [&](const auto &poly, const double sense) {
            for (size_t i = 0; i < poly.size() + 1; i++) {
                const auto zcp = z_cross_product(
                        poly[(i + 0) % poly.size()],
                        poly[(i + 1) % poly.size()],
                        poly[(i + 2) % poly.size()]
                );
                if (sense * zcp < 0) {
                    concave_points.insert(poly[(i + 1) % poly.size()]);
                }
            }
        };

        // Loop over the polygons of the MultiPolygon, as well as their holes
        for (const auto &poly: mp) {
            // Outer boundary has positive sense
            consider_polygon(poly.outer_boundary(), 1);
            for (const auto &hole: poly.holes()) {
                // Inner boundaries (holes) have negative (opposite) sense
                consider_polygon(hole, -1);
            }
        }

        return concave_points;
    }


/// Print the points which collectively comprise the medial axis
    void VoronoiMiddleLineFinder::print_medial_axis_points(const MedialData &md, const std::string &filename) {
        std::ofstream fout(filename);
        fout << "x,y" << std::endl;
        for (const auto &vh: map_to_ordered_vector(md.vertex_handles)) {
            fout << vh->point().x() << "," << vh->point().y() << std::endl;
        }
    }


/// Prints the edges of the medial diagram
    void VoronoiMiddleLineFinder::print_medial_axis_edges(const MedialData &md, const std::string &filename) {
        std::ofstream fout(filename);
        fout << "SourceIdx,TargetIdx,UpGovernorIsPoint,DownGovernorIsPoint" << std::endl;
        for (std::size_t i = 0; i < md.edges.size(); i++) {
            fout << md.edges[i].first << ","
                 << md.edges[i].second
                 << std::endl;
        }
    }


    MedialData VoronoiMiddleLineFinder::filter_voronoi_diagram_to_medial_axis(
            const VoronoiDiagram &vd,
            const MultiPolygon &mp
    ) {
        MedialData ret;

        const auto interior = identify_vertex_handles_inside_mp(vd, mp);
        const auto concave_points = identify_concave_points_of_mp(mp);

        // Returns true if a point is a concave point of the MultiPolygon
        const auto pconcave = [&](const Point_2 &pt) {
            return concave_points.count(pt) != 0;
        };

        // The Voronoi diagram comprises a number of vertices connected by lines
        // Here, we go through each edge of the Voronoi diagram and determine which
        // vertices it's incident on. We add these vertices to `ret.vertex_handles`
        // so that they will have unique ids.

        // The `up` and `down` refer to the medial axis governors - that which
        // constrains each edge of the Voronoi diagram
        for (
                auto edge_iter = vd.bounded_halfedges_begin();
                edge_iter != vd.bounded_halfedges_end();
                edge_iter++
                ) {
            const Halfedge &halfedge = *edge_iter;
            const Vertex_handle &v1p = halfedge.source();
            const Vertex_handle &v2p = halfedge.target();

            // Filter Voronoi diagram to only the part in the interior of the
            // MultiPolygon
            if (interior.count(v1p) == 0 || interior.count(v2p) == 0) {
                continue;
            }

            // Drop those edges of the diagram which are not part of the medial axis
            if (pconcave(v1p->point()) || pconcave(v2p->point())) {
                continue;
            }

            // Get unique ids for edge vertex handle that's part of the medial axis
            const auto id1 = find_or_add(ret.vertex_handles, v1p);
            const auto id2 = find_or_add(ret.vertex_handles, v2p);
            ret.edges.emplace_back(id1, id2);


        }
        RCLCPP_DEBUG(this->get_logger(), "filter_voronoi_diagram_to_medial_axis: before removal");
        // remove doubled edges --> Every second edge is duplicated after algorithm above
        MedialData newMd = ret;

        for (uint i = 0; i < (ret.edges.size()/2); i += 1){
            newMd.edges.erase(newMd.edges.begin() + i);
        }
        RCLCPP_DEBUG(this->get_logger(), "filter_voronoi_diagram_to_medial_axis: medialaxis created");

        return newMd;
    }

// code by phoerman@student.ethz.ch
    MedialData VoronoiMiddleLineFinder::filter_arms(MedialData &md, bool cutEnds) {

        uint numberOfPoints = md.vertex_handles.size();
        std::vector<Vertex_handle> vh = map_to_ordered_vector(md.vertex_handles);
        std::vector<Fork> forks;
        std::vector<int> forkPoints;
        // loop through all points, thus vertex_handles size, if more than two occurances, it is a fork point
        for (uint i = 0; i < numberOfPoints; ++i){
            int count = 0;
            for (uint j = 0; j < md.edges.size(); ++j){
                if ((unsigned int)md.edges[j].first == i || (unsigned int)md.edges[j].second == i){
                    count += 1;
                }
            }
            if (count > 2){
                Fork f;
                f.point = i;
                forks.push_back(f);
                forkPoints.push_back(i);
                RCLCPP_DEBUG(this->get_logger(), "filter_arms: added: %i", i);
            }
        }

        // loop though all potential forks and find points which need to be cut away
        for (auto & fork : forks){

            // find specific rows in "edges"
            for (uint i = 0; i < md.edges.size(); ++i){
                if (md.edges[i].first == fork.point || md.edges[i].second == fork.point){
                    fork.ids.push_back(i);
                }
            }

            // determine angle between vectors for first evaluation
            for (uint i = 0; i < fork.ids.size(); i++){
                const double x_one_arm_one = CGAL::to_double(vh[md.edges[fork.ids[i]].first]->point().x());
                const double y_one_arm_one = CGAL::to_double(vh[md.edges[fork.ids[i]].first]->point().y());
                const double x_two_arm_one = CGAL::to_double(vh[md.edges[fork.ids[i]].second]->point().x());
                const double y_two_arm_one = CGAL::to_double(vh[md.edges[fork.ids[i]].second]->point().y());
                const double x_one_arm_two = CGAL::to_double(vh[md.edges[fork.ids[(i + 1) % fork.ids.size()]].first]->point().x());
                const double y_one_arm_two = CGAL::to_double(vh[md.edges[fork.ids[(i + 1) % fork.ids.size()]].first]->point().y());
                const double x_two_arm_two = CGAL::to_double(vh[md.edges[fork.ids[(i + 1) % fork.ids.size()]].second]->point().x());
                const double y_two_arm_two = CGAL::to_double(vh[md.edges[fork.ids[(i + 1) % fork.ids.size()]].second]->point().y());
                angle_between_arms(fork, x_one_arm_one, y_one_arm_one,
                                        x_two_arm_one, y_two_arm_one, x_one_arm_two,
                                        y_one_arm_two, x_two_arm_two, y_two_arm_two);
            }
            // mark which arm needs to be deleted

            bool checked_angle = false;
            for (uint i = 0; i < fork.angles.size(); i++){
                if (checkForAngle_ && (fork.angles[i] < minAngle_ || fork.angles[i] > maxAngle_)){
                    RCLCPP_DEBUG(this->get_logger(), "forkpoint: %i, angles being checked", fork.point);
                    // is a normal arm, only one strand needs to be deleted
                    int idArmBegin = (i + 2) % 3;
                    make_arm(fork, md, idArmBegin, forkPoints);
                    checked_angle = true;
                }

            }
            if (checked_angle == false && cutEnds == true){
                for (uint i = 0; i < fork.ids.size(); ++i){
                    RCLCPP_DEBUG(this->get_logger(), "forkpoint: %i, arm is being made", fork.point);
                    make_arm(fork, md, i, forkPoints);
                }
            }


            // delete arms

            for (auto & arm : fork.arms){
                for (auto & id : arm.ids){
                    md.edges[id].second = numberOfPoints + 1;
                }

            }


        }
        // delete edges and vertexes
        bool point_found = 1;
        while(point_found){
            point_found = 0;
            for (uint i = 0; i < md.edges.size(); i++){
                if ((unsigned int)md.edges[i].second == numberOfPoints + 1){
                    md.edges.erase(md.edges.begin() + i);
                    point_found = 1;
                }
            }
        }


        return md;

    }

    void VoronoiMiddleLineFinder::make_arm(Fork &fork, MedialData md, int idBegin, std::vector<int> forkPoints){
        Line arm;
        int idMd = fork.ids[idBegin];
        arm.ids.push_back(idMd);

        if (md.edges[idMd].first == fork.point){
            arm.points.push_back(md.edges[idMd].second);
        }
        else {
            arm.points.push_back(md.edges[idMd].first);
        }
        int previousPoint = fork.point;
        int nextPoint = arm.points[0];
        bool endOfArm = false;
        bool isArm = true;
        while (!endOfArm && isArm){
            endOfArm = true;
            for (uint i = 0; i < md.edges.size(); ++i){

                if ((md.edges[i].first == nextPoint && md.edges[i].second != previousPoint) ||
                    (md.edges[i].second == nextPoint && md.edges[i].first != previousPoint)){
                    endOfArm = false;
                    arm.ids.push_back(i);
                    previousPoint = nextPoint;
                    if(md.edges[i].first == nextPoint){
                        arm.points.push_back(md.edges[i].second);
                        nextPoint = md.edges[i].second;
                    }
                    else {
                        arm.points.push_back(md.edges[i].first);
                        nextPoint = md.edges[i].first;
                    }
                    break;

                }
            }
            for (auto & point : forkPoints){

                if (nextPoint == point){
                    RCLCPP_DEBUG(this->get_logger(), "forkpoint: %i, next point is fork point", fork.point);
                    isArm = false;
                    break;
                }
            }
            RCLCPP_DEBUG(this->get_logger(), "forkpoint: %i, arm length: %li", fork.point, arm.points.size());
            if (arm.points.size() > (unsigned int)cutOffLength_){
                RCLCPP_DEBUG(this->get_logger(), "forkpoint: %i, arm added", fork.point);
                isArm = false;
            }
        }
        if (isArm){
            RCLCPP_DEBUG(this->get_logger(), "forkpoint: %i, arm added", fork.point);
            fork.arms.push_back(arm);
        }

    }

    void VoronoiMiddleLineFinder::angle_between_arms(Fork &fork, double x1v1, double y1v1, double x2v1, double y2v1, double x1v2, double y1v2, double x2v2, double y2v2){
        float xv1 = x2v1 - x1v1;
        float yv1 = y2v1 - y1v1;
        float xv2 = x2v2 - x1v2;
        float yv2 = y2v2 - y1v2;
        float dot_product = xv1*xv2 + yv1*yv2;
        float magnitude_v1 = sqrt(std::pow(xv1, 2) + std::pow(yv1, 2));
        float magnitude_v2 = sqrt(std::pow(xv2, 2) + std::pow(yv2, 2));
        float angle = acos(dot_product/magnitude_v1/magnitude_v2);


        fork.angles.push_back(angle);
    }

    void VoronoiMiddleLineFinder::find_medial_axis(int i) {

        CGAL::set_pretty_mode(std::cout);

        const auto mp = get_wkt_from_file(i);
        const auto voronoi = convert_mp_to_voronoi_diagram(mp);
        auto ma_data_with_arms = filter_voronoi_diagram_to_medial_axis(voronoi, mp);
// First cut off arms because of angle, then cut off ends in order to not collide with the arms
        auto ma_data_1 = filter_arms(ma_data_with_arms, false);
        const auto ma_data = filter_arms(ma_data_1, true);
        RCLCPP_INFO(this->get_logger(), "find_medial_axis: Calculations done");
        print_medial_axis_points(ma_data, safeDirectory_ + "voronoi_points.csv");
        print_medial_axis_edges(ma_data, safeDirectory_ + "voronoi_edges.csv");
        RCLCPP_INFO(this->get_logger(), "find_medial_axis: Files saved");
    }
}

#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(voronoi_middle_line::VoronoiMiddleLineFinder);