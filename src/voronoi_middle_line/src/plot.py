#!/usr/bin/env python3
# from https://stackoverflow.com/questions/69237154/how-do-you-get-the-medial-axis-of-a-multipolygon-using-cgal

import matplotlib.pyplot as plt
import numpy as np
from shapely import wkt
import os

for image in range(0, 476):
    fig, ax = plt.subplots()

    # Output file from C++ medial axis code

    pts = np.loadtxt("/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/2023_03_09-10_29_59-1fps/image" + str(image) + "/voronoi_points.csv", skiprows=1, delimiter=',')

    fig4 = wkt.loads(open("/home/patricia/workspaces/master_thesis/src/image_processing/test/outputs/2023_03_09-10_29_59-1fps/image" + str(image) + "/wkt.txt").read())
    for geom in fig4.geoms:
        xs, ys = geom.exterior.xy

        ax.plot(xs, ys, '-ok', lw=4)

    ma = np.loadtxt("/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/2023_03_09-10_29_59-1fps/image" + str(image) + "/voronoi_edges.csv", dtype=int, skiprows=1, delimiter=',')
    for mal in ma:
        ax.plot((pts[mal[0]][0], pts[mal[1]][0]), (pts[mal[0]][1], pts[mal[1]][1]), '-o')

    axe = plt.gca()
    ax.set_aspect('equal', adjustable='box')
    plt.gca().invert_yaxis()
    # plt.show()

    # Initialize the directory name
    dirname = "/home/patricia/workspaces/master_thesis/src/trajectory_approximator/outputs/2023_03_09-10_52_21-1fps"
    # Check the directory name exist or not
    if os.path.isdir(dirname) == False:
        # Create the directory
        os.mkdir(dirname)
        # Print success message
        print("The directory " + dirname + "is created.")

    # plt.show()
    image_save_directory = "/home/patricia/workspaces/master_thesis/src/voronoi_middle_line/test/2023_03_09-10_29_59-1fps/image" + str(image) + "/plot.png"
    plt.savefig(image_save_directory, bbox_inches='tight', dpi=300)
