import os
from ament_index_python.packages import get_package_share_directory
import launch
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():

    config = os.path.join(
        get_package_share_directory('voronoi_middle_line'),
        'config',
        'params.yaml'
    )

    container = ComposableNodeContainer(
        name="voronoi_middle_line_container",
        namespace="",
        package="rclcpp_components",
        executable="component_container",
        emulate_tty=True,
        arguments=[
            "--ros-args",
            "--log-level",
            "voronoi_middle_line_finder:=info", # info or debug
        ],
        composable_node_descriptions=[
            ComposableNode(
                package="voronoi_middle_line",
                plugin="voronoi_middle_line::VoronoiMiddleLineFinder",
                name="voronoi_middle_line_finder",
                namespace="voronoi_middle_line_finder",
                parameters=[config]
            ),
        ],
        output="screen",
    )

    return launch.LaunchDescription([container])
