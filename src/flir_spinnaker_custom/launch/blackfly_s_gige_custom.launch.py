# -----------------------------------------------------------------------------
# Copyright 2022 Kevin Janesch
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration as LaunchConfig
from launch.actions import DeclareLaunchArgument as LaunchArg
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    """Launch blackfly_s camera node."""
    flir_dir = get_package_share_directory('flir_spinnaker_custom')
    config_dir = flir_dir + '/config/'
    name_arg = 'blackfly_0'
    serial_arg = '21278227'
    print([LaunchConfig('serial'), '_'])
    config = config_dir + '/blackfly_s_gige_custom_params.yaml'
    node = Node(package='flir_spinnaker_ros2',
                executable='camera_driver_node',
                output='screen',
                name=name_arg,
                parameters=[
                    {config},
                    {'parameter_file': config_dir + 'blackfly_s_gige_custom.cfg',
                     'serial_number': serial_arg, }],
                remappings=[('~/control', '/exposure_control/control')])
    return LaunchDescription([node])
