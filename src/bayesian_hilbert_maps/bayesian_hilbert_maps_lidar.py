import numpy as np
import matplotlib.pyplot as pl
# %matplotlib inline
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.datasets import make_classification

def sigmoid(x):
    return 1. / (1 + np.exp(-x))
    
def calcPosterior(Phi, y, xi, mu0, sig0):
    logit_inv = sigmoid(xi)
    lam = 0.5 / xi * (logit_inv - 0.5)

    sig = 1. /(1./sig0 + 2*np.sum( (Phi.T**2)*lam, axis=1)) # note the numerical trick for the dot product

    mu = sig*(mu0/sig0 + np.dot(Phi.T, y - 0.5).ravel())

    return mu, sig

#Let's define a function for this
def getTrainingData(data, robot_pos, max_laser_distance, unoccupied_points_per_meter=1, margin=0.01):
    distances = np.sqrt(np.sum((data-robot_pos)**2, axis=1))

    # parametric filling
    for n in range(len(distances)):
        dist = distances[n]
        laser_endpoint = data[n,:3]
        para = np.sort(np.random.random(np.int16(dist * unoccupied_points_per_meter)) * (1 - 2 * margin) + margin)[:,np.newaxis]  # TODO: Uniform[0.05, 0.95]
        points_scan_i = robot_pos + para * (laser_endpoint - robot_pos)  # y =  + para ; para \in [0, 1]
        #print('points_scan_i', points_scan_i)

        if n == 0:  # first data point
            if dist >= max_laser_distance:  # there's no laser reflection
                points = points_scan_i
                labels = np.zeros((points_scan_i.shape[0], 1))
            else:  # append the arrays with laser end-point
                points = np.vstack((points_scan_i, laser_endpoint))
                labels = np.vstack((np.zeros((points_scan_i.shape[0], 1)), np.array([1])[:, np.newaxis]))
        else:
            if dist >= max_laser_distance:  # there's no laser reflection
                points = np.vstack((points, points_scan_i))
                labels = np.vstack((labels, np.zeros((points_scan_i.shape[0], 1))))
            else:  # append the arrays with laser end-point
                points = np.vstack((points, np.vstack((points_scan_i, laser_endpoint))))
                labels = np.vstack((labels, np.vstack((np.zeros((points_scan_i.shape[0], 1)), np.array([1])[:, np.newaxis]))))

    return np.hstack((points, labels))

# Let's generate a toy dataset wit 30 LIDAR beams (note: this disregards circular geometry for simplicity)
nBeams = 30
robotPos = np.array([[5,1]])
laserHitpoints = np.hstack((np.linspace(-40,40, nBeams).reshape(-1,1), 50*np.ones((nBeams,1)) ) )
laserHitpoints[:8,1] = 40

# Get the training set
trainingData = getTrainingData(laserHitpoints, robotPos, 100, 0.3, 0.03)

# Step 0 - data
X3, y3 = trainingData[:,:2], trainingData[:,2]

# Step 1 - define hinde points
xx, yy = np.meshgrid(np.linspace(-60, 60, 60), np.linspace(0, 60, 30))
grid = np.hstack((xx.ravel().reshape(-1,1), yy.ravel().reshape(-1,1)))

# Step 2 - compute features
gamma = 0.7
Phi = rbf_kernel(X3, grid, gamma=gamma)
print("feature map size: ", Phi.shape)

# Step 3 - estimate the parameters
# Let's define the prior
N, D = Phi.shape[0], Phi.shape[1]
epsilon = np.ones(N)
mu = np.zeros(D)
sig = 10000*np.ones(D)

for i in range(3):
    # E-step
    mu, sig = calcPosterior(Phi, y3, epsilon, mu, sig)

    # M-step
    epsilon = np.sqrt(np.sum((Phi**2)*sig, axis=1) + (Phi.dot(mu.reshape(-1, 1))**2).ravel())

# Step 4 - predict
qxx, qyy = np.meshgrid(np.linspace(-60, 60, 120), np.linspace(0, 60, 60))
qX = np.hstack((qxx.ravel().reshape(-1,1), qyy.ravel().reshape(-1,1)))
qPhi = rbf_kernel(qX, grid, gamma=gamma)
qw = np.random.multivariate_normal(mu, np.diag(sig), 1000)
occ = sigmoid(qw.dot(qPhi.T))
occMean = np.mean(occ, axis=0)
occStdev = np.std(occ, axis=0)

# Plot
pl.figure(figsize=(15,2))
pl.subplot(131)
#pl.scatter(grid[:,0], grid[:,1], c='k', marker='o')
pl.scatter(X3[:,0], X3[:,1], c=y3, marker='x', cmap='jet')
pl.colorbar()
pl.title('Hinge points and dataset')
pl.xlim([-60,60]); pl.ylim([0,60]);
pl.subplot(132)
pl.scatter(qX[:,0], qX[:,1], c=occMean, s=4, cmap='jet', vmin=0, vmax=1)
pl.colorbar()
pl.title('Occupancy probability - mean')
pl.xlim([-60,60]); pl.ylim([0,60]);
pl.subplot(133)
pl.scatter(qX[:,0], qX[:,1], c=occStdev,  s=4, cmap='jet', vmin=0)
pl.colorbar()
pl.title('Occupancy probability - stdev')
pl.xlim([-60,60]); pl.ylim([0,60]);

pl.show()