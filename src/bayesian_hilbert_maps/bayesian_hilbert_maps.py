import numpy as np
import matplotlib.pyplot as pl
# %matplotlib inline
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.datasets import make_classification

def sigmoid(x):
    return 1. / (1 + np.exp(-x))
    
def calcPosterior(Phi, y, xi, mu0, sig0):
    logit_inv = sigmoid(xi)
    lam = 0.5 / xi * (logit_inv - 0.5)

    sig = 1. / (1./sig0 + 2*np.sum( (Phi.T**2)*lam, axis=1)) # note the numerical trick for the dot product

    mu = sig*(mu0/sig0 + np.dot(Phi.T, y - 0.5).ravel())

    return mu, sig

# Place nine hinge points on a regular grid
xx, yy = np.meshgrid(np.linspace(-3, 3, 3), np.linspace(-3, 3, 3))
grid = np.hstack((xx.ravel().reshape(-1,1), yy.ravel().reshape(-1,1)))

# Define the datapoint
X = np.array([[-1.6,2]]) 
y = np.array([0]) #means that the field is free, y = 1 would be occupied

#Compute the kernel distance between X1 and all hinge points
#phi = [k(x, x1), k(x, x2), ..., k(x, xM), ], x1 ... xM are coordinates of grid points
#K(x, y) = exp(-gamma ||x-y||^2)
Phi = rbf_kernel(X, grid, gamma=0.5) 
print('Phi(x)=', np.round(Phi, 3))
print('Shape of Phi(x) is', Phi.shape)

# Get the dimensionality
N, D = Phi.shape[0], Phi.shape[1] #N = 1, D = 9

# Let's define the prior distributions
mu = np.zeros(D)
sig = 10000*np.ones(D)

# Define xi vector
xi = np.ones(N)

# Iteratively learn mu, sig, and xi
for i in range(5):
    # E-step
    mu, sig = calcPosterior(Phi, y, xi, mu, sig)

    # M-step
    xi = np.sqrt(np.sum((Phi**2)*sig, axis=1) + (Phi.dot(mu.reshape(-1, 1))**2).ravel()) #simplify the brackets

# Let's query 10000 points between (-4,4) in each direction
qxx, qyy = np.meshgrid(np.linspace(-4, 4, 100), np.linspace(-4, 4, 100))
qX = np.hstack((qxx.ravel().reshape(-1,1), qyy.ravel().reshape(-1,1)))
qPhi = rbf_kernel(qX, grid, gamma=0.5)

# Draw samples and estimate the 
qw = np.random.multivariate_normal(mu, np.diag(sig), 1000)
occ = sigmoid(qw.dot(qPhi.T))
occMean = np.mean(occ, axis=0)
occStdev = np.std(occ, axis=0)


#Plot
pl.figure(figsize=(15,8))
pl.subplot(331)
pl.scatter(grid[:,0], grid[:,1], c='k', marker='o')
pl.scatter(X[:,0], X[:,1], c='b', marker='x')
pl.text(X[0,0]+0.1, X[0,1]+0.1, r'$\bf{x}$', color='b', fontsize=15)
color = iter(pl.cm.rainbow(np.linspace(0,1,Phi.shape[1])))
for i in range(grid.shape[0]):
    c = next(color)
    pl.text(grid[i,0]+0.15, grid[i,1]+0.15, r'$\tilde{\bf{x}}$'+'$_{}$'.format(i+1), fontsize=15)
    pl.arrow(X[0,0], X[0,1], grid[i,0]-X[0,0], grid[i,1]-X[0,1], color=c)
    pl.text((grid[i,0]+X[0,0])/2-0.5, (grid[i,1]+X[0,1])/2, r'$k({\bf x},\tilde{\bf x}$'+'$_{}$)'.format(i+1), color=c, fontsize=12) 
pl.title('Hinge points')
pl.xlim([-4,4])
pl.ylim([-4,4]);

pl.subplot(332)
pl.scatter(grid[:,0], grid[:,1], c=mu, cmap='jet', vmin=-np.max(np.abs(mu)), vmax=np.max(np.abs(mu))); pl.colorbar()
for i in range(grid.shape[0]):
    pl.text(grid[i,0]+0.15, grid[i,1]+0.15, r'$\hat{\mu}$'+'$_{}$'.format(i+1), fontsize=15)
pl.title('Weight mean (updated)')
pl.xlim([-4,4]); pl.ylim([-4,4]);

pl.subplot(333)
pl.scatter(grid[:,0], grid[:,1], c=sig, cmap='jet'); pl.colorbar()
for i in range(grid.shape[0]):
    pl.text(grid[i,0]+0.15, grid[i,1]+0.15, r'$\hat{\sigma}$'+'$_{}$'.format(i+1), fontsize=15)
pl.title('Weight stdev (updated)')
pl.xlim([-4,4]); pl.ylim([-4,4]);

pl.subplot(334)
pl.scatter(qX[:,0], qX[:,1], c=occMean, cmap='jet', vmin=0, vmax=1)
pl.colorbar()
pl.title('Occupancy probability - empirical mean')
pl.xlim([-4,4]); pl.ylim([-4,4])

pl.subplot(335)
pl.scatter(qX[:,0], qX[:,1], c=occStdev, cmap='jet', vmin=0)
pl.colorbar()
pl.xlim([-4,4]); pl.ylim([-4,4])
pl.title('Occupancy probability - empirical stdev/confidence');

pl.show()