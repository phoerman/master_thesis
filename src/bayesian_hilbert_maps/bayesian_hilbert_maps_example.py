import numpy as np
import matplotlib.pyplot as pl
# %matplotlib inline
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.datasets import make_classification

def sigmoid(x):
    return 1. / (1 + np.exp(-x))
    
def calcPosterior(Phi, y, xi, mu0, sig0):
    logit_inv = sigmoid(xi)
    lam = 0.5 / xi * (logit_inv - 0.5)

    sig = 1. /(1./sig0 + 2*np.sum( (Phi.T**2)*lam, axis=1)) # note the numerical trick for the dot product

    mu = sig*(mu0/sig0 + np.dot(Phi.T, y - 0.5).ravel())

    return mu, sig

# Let's generate a dataset
X2, y2 = make_classification(n_samples=100, n_features=2, n_informative=2, n_redundant=0, n_repeated=0, n_classes=2, n_clusters_per_class=2)
y2 = y2.ravel()

# Step 1 - define hinde points
hingePoints = 6
xx, yy = np.meshgrid(np.linspace(-3, 3, hingePoints), np.linspace(-3, 3, hingePoints))
grid = np.hstack((xx.ravel().reshape(-1,1), yy.ravel().reshape(-1,1)))

# Step 2 - compute features
gamma = 2.5
Phi = rbf_kernel(X2, grid, gamma=gamma)
print("feature map size: ", Phi.shape)

# Step 3 - estimate the parameters
# Let's define the prior
N, D = Phi.shape[0], Phi.shape[1]
xi = np.ones(N)
mu = np.zeros(D)
sig = 10000*np.ones(D)

for i in range(3):
    # E-step
    mu, sig = calcPosterior(Phi, y2, xi, mu, sig)

    # M-step
    xi = np.sqrt(np.sum((Phi**2)*sig, axis=1) + (Phi.dot(mu.reshape(-1, 1))**2).ravel())

# Step 4 - predict
qxx, qyy = np.meshgrid(np.linspace(-4, 4, 100), np.linspace(-4, 4, 100))
qX = np.hstack((qxx.ravel().reshape(-1,1), qyy.ravel().reshape(-1,1)))
qPhi = rbf_kernel(qX, grid, gamma=gamma)
qw = np.random.multivariate_normal(mu, np.diag(sig), 1000)
occ = sigmoid(qw.dot(qPhi.T))
occMean = np.mean(occ, axis=0)
occStdev = np.std(occ, axis=0)

# Plot
pl.figure(figsize=(15,4))
pl.subplot(131)
pl.scatter(grid[:,0], grid[:,1], c='k', marker='o')
pl.scatter(X2[:,0], X2[:,1], c=y2, marker='x', cmap='jet')
pl.colorbar()
pl.title('Hinge points and dataset')
pl.xlim([-4,4]); pl.ylim([-4,4])
pl.subplot(132)
pl.scatter(qX[:,0], qX[:,1], c=occMean, cmap='jet', vmin=0, vmax=1)
pl.colorbar()
pl.title('Occupancy probability - mean')
pl.xlim([-4,4]); pl.ylim([-4,4])
pl.subplot(133)
pl.scatter(qX[:,0], qX[:,1], c=occStdev, cmap='jet', vmin=0)
pl.colorbar()
pl.title('Occupancy probability - stdev')
pl.xlim([-4,4]); pl.ylim([-4,4]);

pl.show()